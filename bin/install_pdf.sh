#!/bin/bash
php ./magento module:enable Vnecoms_Core
php ./magento module:enable Vnecoms_PageBuilder
php ./magento module:enable Vnecoms_PdfPro
php ./magento module:enable Vnecoms_PdfProCustomVariables
php ./magento module:enable Vnecoms_PdfProItem

php ./magento setup:upgrade
chmod -R 777 ../var/cache
chmod -R 777 ../var/log
chmod -R 777 ../var/generation