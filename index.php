<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);
//if ($_SERVER['REMOTE_ADDR'] == '116.206.152.52' || $_SERVER['REMOTE_ADDR'] == '37.107.119.204') {
/**
 * Application entry point
 *
 * Example - run a particular store or website:
 * --------------------------------------------
 * require __DIR__ . '/app/bootstrap.php';
 * $params = $_SERVER;
 * $params[\Magento\Store\Model\StoreManager::PARAM_RUN_CODE] = 'website2';
 * $params[\Magento\Store\Model\StoreManager::PARAM_RUN_TYPE] = 'website';
 * $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $params);
 * \/** @var \Magento\Framework\App\Http $app *\/
 * $app = $bootstrap->createApplication('Magento\Framework\App\Http');
 * $bootstrap->run($app);
 * --------------------------------------------
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*if($_SERVER['HTTP_X_ORIGINAL_REQUEST'] == "/oudboxn.html"){
	header('location:https://www.almajed4oud.com/oudboxn-1.html/');
}*/
try {
    require __DIR__ . '/app/bootstrap.php';
} catch (\Exception $e) {
    echo <<<HTML
<div style="font:12px/1.35em arial, helvetica, sans-serif;">
    <div style="margin:0 0 25px 0; border-bottom:1px solid #ccc;">
        <h3 style="margin:0;font-size:1.7em;font-weight:normal;text-transform:none;text-align:left;color:#2f2f2f;">
        Autoload error</h3>
    </div>
    <p>{$e->getMessage()}</p>
</div>
HTML;
    exit(1);
}

$bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
/** @var \Magento\Framework\App\Http $app */
$app = $bootstrap->createApplication('Magento\Framework\App\Http');
$bootstrap->run($app);
/*} else { ?>
<!DOCTYPE html>
<html>
<head>
<title>Website is under maintenance</title>
</head>
<body>

<img src="http://www.almajed4oud.com/maintenance.jpg" width="100%" height="100%">

</body>
</html>
<?php }*/ ?> 
