/**
 * Copyright © 2017 Vnecoms, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/*jshint browser:true, jquery:true*/
define([
	'jquery',
  'jquery/ui',
  'jquery/intltellinput',
  'Vnecoms_Sms/js/utils'
], function($){
    "use strict";

    $.widget('vnecoms.smsLogin', {
        options: {
            mobileInput: '#mobile-number-input',
            mobileField: '#mobile-number',
            loginTypeField: '#login_type',
            initialCountry: '',
            onlyCountries: [],
            preferredCountries: [],
            fieldCtrSelector: '.sms-login-field-ctrl',
            fieldSelector: '.sms-login-field'
        },

        _create: function() {
            this._initMobileInput();
            this._bindEvents();
        },
        
        /**
         * Bind Events
         */
        _bindEvents: function(){
        	var self = this;
        	$(this.options.fieldCtrSelector).click(function(event){
            	event.preventDefault();
            	$(self.options.fieldCtrSelector+'.selected').removeClass('selected');
                $(self.options.fieldSelector).hide();
                $($(this).attr('href')).show();
                $(this).addClass('selected');
                $(self.options.loginTypeField).val($(this).attr('href').replace('#',''));
            });
        },
        
        /**
         * Init mobile input
         */
        _initMobileInput: function(){
        	var self = this;
    	    var data= {
    	    	initialCountry: this.options.initialCountry,
	      		onlyCountries: this.options.onlyCountries,
	    		preferredCountries:this.options.preferredCountries
            }
    	    if(this.options.initialCountry == 'auto'){
    	    	data['geoIpLookup'] = function(callback) {
            	    $.get("https://ipinfo.io?token=d9a48054457087", function() {}, "jsonp").always(function(resp) {
          	    	   var countryCode = (resp && resp.country) ? resp.country : "";
          	    	   callback(countryCode);
         		    });
           		};
    	    }
    	    
    	    $(this.options.mobileInput).intlTelInput(data).done(function() {
            	self._updateMobileNumber();
         	   $(self.options.mobileInput).on('keyup', function() {
         		  self._updateMobileNumber();
           	   }).on("countrychange", function(e, countryData) {
           		   self._updateMobileNumber();
          	   });
          	});
    	},
    	
    	/**
    	 * Update mobile number
    	 */
    	_updateMobileNumber: function(){
    		$(this.options.mobileField).val($(this.options.mobileInput).intlTelInput("getNumber"));
    	}
    });

    return $.vnecoms.smsLogin;
});
