var config = {
	"map": {
        '*': {
            "smsLogin": 'Vnecoms_Sms/js/login',
        }
    },
    "shim": {
        "jquery/intltellinput": ["jquery"],
        "Vnecoms_Sms/js/utils": ["jquery", "jquery/intltellinput"]
    },
    "paths": {
        "jquery/intltellinput": "Vnecoms_Sms/js/intlTelInput"
    }
};
