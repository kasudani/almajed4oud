<?php
namespace Vnecoms\SmsKsaSms\Model;

class Api {
    /**
     * @var string
     */
    protected $user;
    
    /**
     * @var string
     */
    protected $pass;
    
    /**
     * @var string
     */
    protected $appType;
    
    /**
     * @param string $user
     * @param string $pass
     * @param number $appType
     */
    public function __construct($user, $pass, $appType=24){
        $this->user = $user;
        $this->pass = $pass;
        $this->appType = $appType;
    }
    
    //Send SMS API using CURL method
    function sendSMS($numbers, $sender, $msg, $unicode='UTF8')
    {
        global $arraySendMsg;
        $url = "http://api.unifonic.com/wrapper/sendSMS.php?";
        $stringToPost = "userid=".$this->user."&password=".$this->pass."&to=".$numbers."&sender=".$sender."&msg=".$msg."&encoding=".$unicode;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToPost);
        $result = curl_exec($ch);
        return $result;
    }
}