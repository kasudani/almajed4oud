<?php

namespace Vnecoms\PdfPro\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Key.
 */
class Key extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('ves_pdfpro_key', 'entity_id');
    }
}
