<?php

namespace Vnecoms\PdfPro\Model\Processors;

use Vnecoms\PdfPro\Model\KeyFactory;
use Magento\Framework\App\State as AppState;
use Magento\Framework\DataObject;
use Magento\Framework\App\Area;

/**
 * Class Mpdf.
 */
class Mpdf extends \Magento\Framework\DataObject
{
    /**
     * @var KeyFactory
     */
    protected $_keyFactory;

    /**
     * @var \Vnecoms\PdfPro\Helper\Data
     */
    protected $_helper;

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;
    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $_layout;

    /**
     * @var \Vnecoms\PdfPro\Helper\Mconfig
     */
    protected $_pdfConfig;

    /**
     * Mpdf constructor.
     *
     * @param KeyFactory                              $keyFactory
     * @param \Vnecoms\PdfPro\Helper\Data             $helper
     * @param AppState                                $appState
     * @param \Psr\Log\LoggerInterface                $logger
     * @param \Magento\Framework\View\LayoutInterface $layout
     * @param \Vnecoms\PdfPro\Helper\Mconfig          $config
     * @param array                                   $data
     */
    public function __construct(
        KeyFactory $keyFactory,
        \Vnecoms\PdfPro\Helper\Data $helper,
        AppState $appState,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\View\LayoutInterface $layout,
        \Vnecoms\PdfPro\Helper\Mconfig $config,
        $data = []
    ) {
        $this->_keyFactory = $keyFactory;
        $this->_helper = $helper;
        $this->appState = $appState;
        $this->_logger = $logger;
        $this->_layout = $layout;
        $this->_pdfConfig = $config;

        parent::__construct($data);
    }

    /**
     * @param $apiKey
     *
     * @return array
     */
    public function getInfo($apiKey)
    {
        return $this->_keyFactory->create()->load($apiKey, 'api_key')->getData();
    }

    /**
     * @param string $apiKey
     * @param $datas
     * @param $type
     *
     * @return array
     *
     * @throws \Exception
     */
    public function process($apiKey, $datas, $type)
    {
        //get config tax
        $config = $this->_helper->getTaxDisplayConfig();

        /*Get API Key information*/
        $apiKeyInfo = $this->getInfo($apiKey);        //get info of api (css, template, sku)


        if ($type == 'all') {
            return $this->processAllPdf($datas, $apiKey);
        }    //check type of invoice(order,invoice....)
        $sources = array();
        $apiKeys = array();    /*store all api key*/

        foreach ($datas as $data) {
            $tmpData = ($data);

            /*Get API Key information*/
            $pdfInfo = $this->getInfo($tmpData['key']);// var_dump($pdfInfo);die();

            if (!is_array($pdfInfo) || !isset($pdfInfo[$type.'_template'])) {
                $errMsg = __('Your API key is not valid.');
                if ($this->appState->getAreaCode() == Area::AREA_ADMINHTML) {
                    throw new \Exception($errMsg);
                } else {
                    throw new \Exception(__('Can not generate PDF file. Please contact administrator about this error.'));
                }
            }

            if (!isset($apiKeys[$tmpData['key']])) {
                $apiKeys[$tmpData['key']] = new DataObject($pdfInfo);
            }
            $sources[] = $tmpData;
        }

        $this->_pdfConfig->loadPdfConfig();//var_dump(_MPDF_TTFONTDATAPATH);

       // require_once $this->_helper->getPdfLibDir().'/vendor/autoload.php';

        // require_once($this->_helper->getPdfLibDir().'/mpdf.php');//var_dump(_MPDF_TTFONTDATAPATH);
        $pageSize = $this->_helper->getConfig('pdfpro/advanced/page_size');
        $orientation = $this->_helper->getConfig('pdfpro/advanced/orientation');
        $autolang = $this->_helper->getConfig('pdfpro/advanced/autolang');

        /*if($autolang) {
            $mpdf = new \mPDF('utf-8', $pageSize . '-' . $orientation);
        }
        else {
            $mpdf  = new \mPDF('c',$pageSize . '-' . $orientation);
        }*/

        //mode
        //format
        //default font size
        //default font
        //margin-left
        //margin rght
        //margin top
        //margin bottom
        //margin header, footer, orientation
        if ($autolang == \Vnecoms\PdfPro\Model\Source\Lang::CORE) {
            $mpdf = new \mPDF('c', $pageSize.'-'.$orientation);
        } elseif ($autolang == \Vnecoms\PdfPro\Model\Source\Lang::ALL) {
            /*auto langs*/
            $mpdf = new \mPDF('', $pageSize.'-'.$orientation);
        }

        $mpdf->SetTitle($this->_helper->getConfig('pdfpro/general/pdf_title'));
        $mpdf->SetAuthor($this->_helper->getConfig('pdfpro/general/pdf_author'));

        /*auto langs*/
        if ($autolang == \Vnecoms\PdfPro\Model\Source\Lang::ALL) {
            $mpdf->autoScriptToLang = true;
            $mpdf->baseScript = 1;
            $mpdf->autoVietnamese = true;
            $mpdf->autoArabic = true;
            $mpdf->autoLangToFont = true;
        }
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoVietnamese = true;
        $mpdf->autoArabic = true;
        $mpdf->autoLangToFont = true;

//        $mpdf->SetWatermarkText("Paid");
//        $mpdf->showWatermarkText = true;
//        $mpdf->watermarkTextAlpha = 0.1;

        $mpdf->simpleTables = true; //reduce memory
        $mpdf->packTableData = true;//reduce memory
        $mpdf->useSubstitutions = false;
        $mpdf->SetProtection(array('print'));
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->mirrorMargins = true;

        $fontData = [];
        if (isset($apiKeyInfo['font_data'])) {
            $fontData = unserialize($apiKeyInfo['font_data']);
            $fontDataMerge = [];
            foreach ($fontData as $font) {
                $r = [
                    'R' => $font['regular_font'],
                    'B' => $font['regular_font'],
                    'I' => $font['i_font'],
                    'BI' => $font['regular_font'],
                ];
                if ($font['otl']) {
                    $r['useOTL'] = 0xFF;
                }
                $fontDataMerge[$font['title']] = $r;
            }
        }

        if (is_array( $fontDataMerge))
            $mpdf->fontdata = array_merge($mpdf->fontdata, $fontDataMerge);

        $mpdf->available_unifonts = array();
        foreach ($mpdf->fontdata AS $f => $fs) {
            if (isset($fs['R']) && $fs['R']) {
                $mpdf->available_unifonts[] = $f;
            }
            if (isset($fs['B']) && $fs['B']) {
                $mpdf->available_unifonts[] = $f . 'B';
            }
            if (isset($fs['I']) && $fs['I']) {
                $mpdf->available_unifonts[] = $f . 'I';
            }
            if (isset($fs['BI']) && $fs['BI']) {
                $mpdf->available_unifonts[] = $f . 'BI';
            }
        }

        $mpdf->default_available_fonts = $mpdf->available_unifonts;

        $block = \Magento\Framework\App\ObjectManager::getInstance()->create('Vnecoms\PdfPro\Block\PdfBuilder');
        $block->setData(array('config' => $config, 'source' => $sources, 'type' => $type, 'api_keys' => $apiKeys));
        $block->setArea('adminhtml')->setIsSecureMode(true);
        $block->setTemplate('Vnecoms_PdfPro::template-builder.phtml');

        $block->setPdf($mpdf);

        $html = preg_replace('/>\s+</', '><', $block->toHtml());

        $html = htmlspecialchars_decode($html);
        $mpdf->WriteHTML($html);
        $content = $mpdf->Output('', 'S');
        return array('success' => true, 'content' => $content);
    }

    /**
     * @param $datas
     * @param $apiKey
     */
    public function processAllPdf($datas, $apiKey)
    {
        return;
    }
}
