<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Vnecoms\PdfPro\Controller\Guest;

/**
 * Class PrintAction.
 *
 * @author Vnecoms team <vnecoms.com>
 */
class PrintAction extends \Vnecoms\PdfPro\Controller\AbstractController\PrintAction
{
}
