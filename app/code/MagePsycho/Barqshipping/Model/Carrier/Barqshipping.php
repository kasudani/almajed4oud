<?php

namespace MagePsycho\Barqshipping\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Config;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Psr\Log\LoggerInterface;

/**
 * @category   MagePsycho
 * @package    MagePsycho_Barqshipping
 * @author     magepsycho@gmail.com
 * @website    http://www.magepsycho.com
 */
class Barqshipping extends AbstractCarrier implements CarrierInterface
{
    /**
     * Carrier's code
     *
     * @var string
     */
    protected $_code = 'mpbarqshipping';

    /**
     * Whether this carrier has fixed rates calculation
     *
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * Generates list of allowed carrier`s shipping methods
     * Displays on cart price rules page
     *
     * @return array
     * @api
     */
    public function getAllowedMethods()
    {
        return [$this->getCarrierCode() => __($this->getConfigData('name'))];
    }

    /**
     * Collect and get rates for storefront
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @param RateRequest $request
     * @return DataObject|bool|null
     * @api
     */
    public function collectRates(RateRequest $request)
    {
        /**
         * Make sure that Shipping method is enabled
         */
        if (!$this->isActive()) {
            return false;
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        $shippingPrice = $this->getConfigData('price');

        $method = $this->_rateMethodFactory->create();

        /**
         * Set carrier's method data
         */
        $method->setCarrier($this->getCarrierCode());
        $method->setCarrierTitle($this->getConfigData('title'));

        /**
         * Displayed as shipping method under Carrier
         */
        $method->setMethod($this->getCarrierCode());
        $method->setMethodTitle($this->getConfigData('name'));

        $method->setPrice($shippingPrice);
        $method->setCost($shippingPrice);

        $result->append($method);

        return $result;
    }

    /*public function checkAvailableShipCountries(\Magento\Framework\DataObject $request)
    {
        
        $allowedCity = explode(',', $this->getConfigData('barqspecificregion'));
        if(in_array($request->getDestRegionId(), $allowedCity)) {
            return $this;
        } elseif (!$allowedCity || $allowedCity && !in_array(
                $request->getDestRegionId(),
                $allowedCity
            )
            ) {
                
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $errorMsg = $this->getConfigData('specificerrmsg');
                $error->setErrorMessage(
                    $errorMsg ? $errorMsg : __(
                        'Sorry, but we can\'t deliver to the destination city with this shipping module.'
                    )
                );
                //return $error;
                return false;
            } else {
            return false;
        }
        return $this;
    }*/


    public function checkAvailableShipCountries(\Magento\Framework\DataObject $request)
    {
        $allowedCity = explode(',', $this->getConfigData('barqspecificregion'));
        if(in_array($request->getDestRegionId(), $allowedCity)) {
            
            $res = $this->checkAvailableShipCountriesCopy($request);
            if($res) {
               return false;     
            } else {
               return $this;
            }
        } elseif (!$allowedCity || $allowedCity && !in_array(
                $request->getDestRegionId(),
                $allowedCity
            )
            ) {
                /** @var Error $error */
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $errorMsg = $this->getConfigData('specificerrmsg');
                $error->setErrorMessage(
                    $errorMsg ? $errorMsg : __(
                        'Sorry, but we can\'t deliver to the destination city with this shipping module.'
                    )
                );
                //return $error;
                return false;
            } else {
            return false;
        }
        return $this;
    }

    public function checkAvailableShipCountriesCopy(\Magento\Framework\DataObject $request)
    {
        $allowedCity = explode(',', $this->_scopeConfig->getValue('carriers/mpjakplusshipping/specificregion',\Magento\Store\Model\ScopeInterface::SCOPE_STORE));

        $city_polygons =  $this->_scopeConfig->getValue('carriers/mpjakplusshipping/specificpolygonsdata',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $city_polygons = json_decode($city_polygons, TRUE);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');
        
        $latlong = "";
        $postcode = "";
        $customerRepository = $objectManager->get('Magento\Customer\Api\CustomerRepositoryInterface');
        $addressRepository = $objectManager->get('Magento\Customer\Api\AddressRepositoryInterface'); 
        if(!isset($_SESSION['address_id']) || $_SESSION['address_id']=="" || $_SESSION['address_id'] == "new-customer-address"){
            if(isset($_SESSION['latlong']) && $_SESSION['latlong']!=""){
                $latlong = $_SESSION['latlong'];
            } else if(isset($_SESSION['postcode']) && $_SESSION['postcode']!=""){
                $postcode = $_SESSION['postcode'];
            } else {

                if(!isset($_SESSION['customer_base']['customer_id'])){
                    return false;
                }
                $customerId = $_SESSION['customer_base']['customer_id'];
                $customer = $customerRepository->getById($customerId);
                $shippingAddressId = $customer->getDefaultShipping();

                 if($shippingAddressId){
                $shippingAddress = $addressRepository->getById($shippingAddressId);
                $streetaddress = $shippingAddress->getStreet();
                $city = $shippingAddress->getCity();
                $countryId = $shippingAddress->getCountryId();
                //$country = $shippingAddress->getCountryCode();
                $region = $shippingAddress->getRegion();
                $regionId = $shippingAddress->getRegionId();
                $regionName = $request->getDestRegionName();      
                }      
            }
        } else {

            $shippingAddress = $addressRepository->getById($_SESSION['address_id']);
            $streetaddress = $shippingAddress->getStreet();
            $city = $shippingAddress->getCity();
            $countryId = $shippingAddress->getCountryId();
            //$country = $shippingAddress->getCountryCode();
            $region = $shippingAddress->getRegion();
            $regionId = $shippingAddress->getRegionId();
            $regionName = $request->getDestRegionName(); 

            
            /* check the address has the lat Long*/
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            $sql = "Select * FROM  customer_address_entity_varchar where attribute_id='183' AND entity_id = '".$_SESSION['address_id']."'";// limit 0,4
            $result = $connection->fetchAll($sql);
            if(isset($result[0])){
                $result = $result[0];
            }
            if(isset($result['value'])){
                $latlong = $result['value'];
            }

        }

        //$country = $objectManager->create('Magento\Directory\Api\CountryInformationAcquirerInterface')
         //               ->getCountryInfo($countryId);
        $countryName = 'Saudi Arabia';
        if( !isset($regionName) || $regionName==""){
            $region = $objectManager->create('Magento\Directory\Model\Region')
                        ->load($request->getDestRegionId());
            $regionName = $region->getName();
        }

        if(in_array($request->getDestRegionId(), $allowedCity)) {

            if(isset($city_polygons[$regionName])){
                $points_polygon = $city_polygons[$regionName];
                $vertices_x = [];
                $vertices_y = [];
                if($latlong==""){ 
                    //$addrs = implode(' ', $shippingAddress->getStreet());
                    //$addrs .= ' '.$city." ".$regionName." ".$countryName;
                    if($postcode == "")
                        $addrs = $shippingAddress->getPostcode();
                      else
                        $addrs = $postcode;
                    $addrs .= " ".$countryName;
                    $lacg = $this->getLatLong($addrs);
                    //$longitude_x = 26.481673557960757;
                    $longitude_x = $lacg['lat']; 
                    $latitude_y = $lacg['long'];
                } else {
                    $latlong = explode(',', $latlong);
                    $longitude_x = $latlong[0];
                    $latitude_y = $latlong[1];
                }

                foreach ($points_polygon as $key => $value) {
                    $vertices_x[] = $value[0];
                    $vertices_y[] = $value[1];
                }
                $points_polygon = count($vertices_x) - 1;

                if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
                    return $this;
                } else {
                    return false;
                }
            }
            
        } elseif (!$allowedCity || $allowedCity && !in_array(
                $request->getDestRegionId(),
                $allowedCity
            )
            ) {
                /** @var Error $error */
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $errorMsg = $this->getConfigData('specificerrmsg');
                $error->setErrorMessage(
                    $errorMsg ? $errorMsg : __(
                        'Sorry, but we can\'t deliver to the destination city with this shipping module.'
                    )
                );
                return false;
            } else {
            return false;
        }
        return $this;
    }

    function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y) {
      $i = $j = $c = 0;
      for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
        if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
         ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
           $c = !$c;
      }
      return $c;
    }

    function getLatLong($address){
        // Get lat and long by address         
        $prepAddr = str_replace(' ','+',$address);
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyAy2GGPoYwUcEmwD7wPCeCY9-ieFeLmq8M');
        $output = json_decode($geocode);
        if(isset($output->results[0])){
            $latitude = $output->results[0]->geometry->location->lat;
            $longitude = $output->results[0]->geometry->location->lng;
            return ['lat'=>$latitude, 'long'=> $longitude ];    
        } else {
            return ['lat'=>0.0, 'long'=> 0.0 ];    
        }
    }
}