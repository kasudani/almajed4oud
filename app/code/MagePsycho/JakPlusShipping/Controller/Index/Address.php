<?php
namespace MagePsycho\JakPlusShipping\Controller\Index;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;

 
class Address extends Action
{
   /**
    * @var \Magento\Framework\View\Result\PageFactory
    */
   protected $_pageFactory;
 
   /**
    * @param Context $context
    * @param PageFactory $pageFactory
    */
   public function __construct(
      Context $context,
      PageFactory $pageFactory
   ) {
      parent::__construct($context);
      $this->_pageFactory = $pageFactory;
   } 

   public function execute()
    {
        $request = json_decode ($this->getRequest()->getContent());
        
        if(isset($request->address_id)){
          $address_id = $request->address_id;
          $_SESSION['address_id'] = $address_id;
        }
        if(isset($request->latlong)){
          $latlong = $request->latlong; 
          $_SESSION['latlong'] = $latlong;
        }
        if(isset($request->postcode)){
          $postcode = $request->postcode; 
          $_SESSION['postcode'] = $postcode;
        }
        
        echo 'done';
    }
 
}