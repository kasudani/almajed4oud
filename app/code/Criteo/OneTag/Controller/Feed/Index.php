<?php

namespace Criteo\OneTag\Controller\Feed;

use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Request\Http;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Framework\Api\SearchCriteriaInterface;
use \Magento\Framework\Api\Search\FilterGroup;
use \Magento\Framework\Api\FilterBuilder;
use \Magento\Catalog\Model\Product\Attribute\Source\Status;
use \Magento\Catalog\Model\Product\Visibility;
use \Magento\CatalogInventory\Model\Stock\StockItemRepository;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Framework\App\ResourceConnection;
use \Magento\Catalog\Helper\Category;
use \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;

class Index extends \Magento\Framework\App\Action\Action
{
    const SETTINGS_PRODUCT_TYPES = 'cto_onetag_section/general/cto_product_types';
    const SETTINGS_USE_SKU = 'cto_onetag_section/general/cto_use_sku';

    protected $_request;
    protected $_productRepository;
    protected $_searchCriteria;
    protected $_filterGroup;
    protected $_filterBuilder;
    protected $_productStatus;
    protected $_productVisibility;
    protected $_objectManager;
    protected $_stockRepository;
    protected $_scopeConfig;
    protected $_dbConnection;
    protected $_productTypeConfigurable;

    public function __construct(
        Context $context,
        Http $request,
        ProductRepository $productRepository,
        SearchCriteriaInterface $criteria,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisbility,
        StockItemRepository $stockRepository,
        ScopeConfigInterface $scopeConfig,
        ResourceConnection $connection,
        Category $categoryHelper,
        Configurable $productTypeConfigurable
    ) {
        $this->_request = $request;
        $this->_productRepository = $productRepository;
        $this->_searchCriteria = $criteria;
        $this->_filterBuilder = $filterBuilder;
        $this->_productStatus = $productStatus;
        $this->_productVisibility = $productVisbility;
        $this->_stockRepository = $stockRepository;
        $this->_scopeConfig = $scopeConfig;
        $this->_dbConnection = $connection;
        $this->_categoryHelper = $categoryHelper;
        $this->_productTypeConfigurable = $productTypeConfigurable;

        $this->_objectManager = $context->getObjectManager();

        parent::__construct($context);
    }

    public function execute()
    {

        //get parameters
        $page = (int) $this->_request->getParam('page');
        $limit = (int) $this->_request->getParam('limit');

        if (!$page) {
            $page = 1;
        }

        if (!$limit) {
            $limit = 250;
        }

        //get product types
        $productTypes =  $this->getProductTypesSQL();

        //get product ids with given criteria
        $productIds = $this->getProductIds($page, $limit, $productTypes);

        //set filters
        $filterIds = $this->_filterBuilder->setField('entity_id')->setConditionType('in')->setValue($productIds)->create();
        $filterGroupIds = new FilterGroup();
        $filterGroupIds->setFilters([$filterIds]);

        $filterStatus = $this->_filterBuilder->setField('status')->setConditionType('in')->setValue($this->_productStatus->getVisibleStatusIds())->create();
        $filterGroupStatus = new FilterGroup();
        $filterGroupStatus->setFilters([$filterStatus]);

        $filterVisibility = $this->_filterBuilder->setField('visibility')->setConditionType('in')->setValue($this->_productVisibility->getVisibleInSiteIds())->create();
        $filterGroupVisibility = new FilterGroup();
        $filterGroupVisibility->setFilters([$filterVisibility]);

        $this->_searchCriteria->setFilterGroups([$filterGroupIds, $filterGroupStatus, $filterGroupVisibility]);

        $products = $this->_productRepository->getList($this->_searchCriteria);
        $productItems = $products->getItems();

        $feed_data = array();

        //do caching (if any)
        $store = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
        $baseUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product';
        $categoryNames = $this->getCategoryNames();

        foreach ($productItems as $item) {
            $product = new \stdClass();

            try {
                $product->id = $item->getId();
                $product->sku = $item->getSku();
                $product->item_group_id = '';
                $parent = $this->_productTypeConfigurable->getParentIdsByChild($product->id);
                if (isset($parent[0])) {
                    $product->item_group_id = $parent[0];
                }
                $product->title = $item->getName();
                $product->description = $item->getShortDescription();
                $product->link = $item->getProductUrl();
                $product->image_link = $baseUrl . $item->getImage();
                $product->price = $item->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\RegularPrice::PRICE_CODE)->getValue();
                $product->sale_price = $item->getPriceInfo()->getPrice(\Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE)->getValue();
                $categoryIds = $item->getCategoryIds();
                $tmpCategoryNames = array();
                foreach ($categoryIds as $id) {
                    array_push($tmpCategoryNames, $categoryNames[$id]);
                }
                $product->product_type = implode(' > ', $tmpCategoryNames);

                try {
                    $stock = $this->_stockRepository->get($product->id);
                    if (isset($stock)) {
                        $product->availability = $stock->getIsInStock() == true ? 'in stock' : 'out of stock';
                    }
                } catch (\Magento\Framework\Exception\NoSuchEntityException $exc) {
                    $product->availability = 'in stock';
                }
            } catch (\Exception $exc) {
                //any thing we can do?
                continue;
            }
            array_push($feed_data, $product);
        }

        $xml_feed = self::generate_xml_feed($feed_data);

        //output XML to browser
        header('Content-type: text/xml; charset=utf-8');
        echo($xml_feed);
    }

    private function generate_xml_feed($feed_data)
    {
        $xmlWriter = new \XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->setIndent(true);
        $xmlWriter->setIndentString("    ");
        $xmlWriter->startDocument('1.0', 'UTF-8');
        $xmlWriter->startElement('rss');
        $xmlWriter->writeAttribute('version', '2.0');
        $xmlWriter->writeAttribute('xmlns:g', 'http://base.google.com/ns/1.0');
        $xmlWriter->startElement('channel');
        foreach ($feed_data as $row) {
            $xmlWriter->startElement('item');
            foreach ($row as $key => $value) {
                $xmlWriter->startElement('g:'.$key);
                in_array($key, ['title', 'description']) ? $xmlWriter->writeCData($value) : $xmlWriter->text($value);
                $xmlWriter->endElement(); //g:KEY
            }
            $xmlWriter->endElement(); //item
        }
        $xmlWriter->endElement(); //channel
        $xmlWriter->endElement(); //rss
        return $xmlWriter->flush(true);
    }

    /*
     * Return product types in SQL IN statement
     */
    private function getProductTypesSQL()
    {
        $productTypes = $this->_scopeConfig->getValue(self::SETTINGS_PRODUCT_TYPES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (empty($productTypes)) {
            $productTypes = 'simple,configurable';
        }
        $productTypes = explode(',', $productTypes);
        $out = '("' . implode('","', $productTypes) . '")';
        return $out;
    }

    /*
     * Return array of product ids
     */
    private function getProductIds($page, $limit, $types)
    {
        $start = ($page - 1) * $limit;
        $connection = $this->_dbConnection->getConnection();
        $table = $connection->getTableName('catalog_product_entity');
        $query = <<<EOT
			SELECT entity_id FROM $table WHERE type_id IN $types LIMIT $start, $limit
EOT;
        $result = $connection->fetchAll($query);
        $ids = array();

        foreach ($result as $row) {
            array_push($ids, $row['entity_id']);
        }
        return $ids;
    }

    /*
     * Get set of categories name
     */
    private function getCategoryNames()
    {
        $sorted = false;
        $asCollection = true;
        $toLoad = true;
        $names = array();
        $categories = $this->_categoryHelper->getStoreCategories($sorted, $asCollection, $toLoad);

        foreach ($categories as $category) {
            $names[$category->getId()] = $category->getName();
        }
        return $names;
    }
}
