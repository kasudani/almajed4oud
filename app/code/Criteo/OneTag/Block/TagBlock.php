<?php

namespace Criteo\OneTag\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Criteo\OneTag\Helper\TagGenerator;
use Magento\Framework\ObjectManagerInterface;

/**
 * Description of TagBlock
 *
 * @author Criteo
 */
class TagBlock extends \Magento\Framework\View\Element\Template
{
    const SETTINGS_PARTNER_ID = 'cto_onetag_section/general/cto_partner';
    const SETTINGS_ENABLE_HOME = 'cto_onetag_section/general/cto_enable_home';
    const SETTINGS_ENABLE_LISTING = 'cto_onetag_section/general/cto_enable_listing';
    const SETTINGS_ENABLE_PRODUCT = 'cto_onetag_section/general/cto_enable_product';
    const SETTINGS_ENABLE_BASKET = 'cto_onetag_section/general/cto_enable_basket';
    const SETTINGS_ENABLE_SALE = 'cto_onetag_section/general/cto_enable_sale';
    const SETTINGS_USE_SKU = 'cto_onetag_section/general/cto_use_sku';

    protected $_request;
    protected $_registry;
    protected $_scopeConfig;
    protected $_customer_session;
    protected $_cart;
    protected $_order;
    protected $_tag_generator;
    protected $_use_sku;
    protected $_objectManager;
    protected $_storeManager;

    public function __construct(
        Context $context,
        Http $request,
        Registry $registry,
        \Magento\Customer\Model\Session $customer_session,
        \Magento\Checkout\Model\Session $cart,
        Order $order,
        TagGenerator $tag_generator,
        ObjectManagerInterface $objectManager,
        array $data = []
    ) {
        $this->_request = $request;
        $this->_registry = $registry;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_customer_session = $customer_session;
        $this->_cart = $cart;
        $this->_order = $order;
        $this->_tag_generator = $tag_generator;
        $this->_use_sku = $this->_scopeConfig->getValue(self::SETTINGS_USE_SKU, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->_objectManager = $objectManager;
        $this->_storeManager = $context->getStoreManager();

        parent::__construct($context, $data);
    }

    public function cto_generate_tag()
    {
        $output = '';

        //get configuration info
        $partner_id = $this->_scopeConfig->getValue(self::SETTINGS_PARTNER_ID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $enable_home = $this->_scopeConfig->getValue(self::SETTINGS_ENABLE_HOME, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $enable_listing = $this->_scopeConfig->getValue(self::SETTINGS_ENABLE_LISTING, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $enable_product = $this->_scopeConfig->getValue(self::SETTINGS_ENABLE_PRODUCT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $enable_basket = $this->_scopeConfig->getValue(self::SETTINGS_ENABLE_BASKET, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $enable_sale = $this->_scopeConfig->getValue(self::SETTINGS_ENABLE_SALE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        //get user email if available
        $email = '';
        if ($this->_customer_session->isLoggedIn()) {
            $email = $this->_customer_session->getCustomer()->getEmail();
        }

        if (!empty($email)) {
            $email = md5(strtolower(trim($email)));
        }

        //set partner id
        $this->_tag_generator->setAccount(array('account' => $partner_id));

        //set user email
        $this->_tag_generator->setEmail(array('email' => $email));

        if ($this->is_homepage()) {
            $this->_tag_generator->viewHome();

            if ($enable_home) {
                $output = $this->_tag_generator->cto_get_code();
            }
        } elseif ($this->is_listing()) {
            $product_list = $this->get_product_list();
            $this->_tag_generator->viewList($product_list);

            if ($enable_listing) {
                $output = $this->_tag_generator->cto_get_code();
            }
        } elseif ($this->is_product()) {
            $product_id = $this->get_product_id();
            $this->_tag_generator->viewItem($product_id);

            if ($enable_product) {
                $output = $this->_tag_generator->cto_get_code();
            }
        } elseif ($this->is_cart()) {
            $items = $this->get_cart();
            $this->_tag_generator->viewBasket($items);

            if ($enable_basket) {
                $output = $this->_tag_generator->cto_get_code();
            }
        } elseif ($this->is_sale()) {
            $this->_tag_generator->trackTransaction($this->get_sale_data());

            if ($enable_sale) {
                $output = $this->_tag_generator->cto_get_code();
            }
        }

        //show data layer regardless of enable options
        $output .= $this->_tag_generator->cto_set_dataLayer();

        return $output;
    }

    private function is_homepage()
    {
        return $this->_request->getFullActionName() == 'cms_index_index';
    }

    private function is_listing()
    {
        return $this->_request->getFullActionName() == 'catalog_category_view';
    }

    private function is_product()
    {
        return $this->_request->getFullActionName() == 'catalog_product_view';
    }

    private function is_cart()
    {
        $action_name = $this->_request->getFullActionName();
        return $action_name == 'checkout_cart_index' || $action_name == 'checkout_index_index';
    }

    private function is_sale()
    {
        return $this->_request->getFullActionName() == 'checkout_onepage_success';
    }

    private function get_product_id()
    {
        $product_id = '';

        try {
            $current_product = $this->_registry->registry('current_product');
            $product_id = $current_product->getId();

            if ($this->_use_sku) {
                $product_id = $current_product->getSku();
            }
        } catch (\Exception $exc) {
            //do nothing
        }
        return array('item' => $product_id);
    }

    private function get_product_list()
    {
        $product_list = array();
        try {
            $products = $this->_registry->registry('current_category')->getProductCollection();
            foreach ($products as $product) {
                if ($this->_use_sku) {
                    $product_list[] = $product->getSku();
                } else {
                    $product_list[] = $product->getId();
                }

                if (sizeof($product_list) == 3) {
                    break;
                }
            }
        } catch (\Exception $exc) {
            //do nothing
        }

        return array('item' => $product_list);
    }

    private function get_cart()
    {
        $cart_content = array();
        $currency_code = '';
        try {
            $products = $this->_cart->getQuote()->getAllVisibleItems();
            foreach ($products as $product) {
                $product_id = $this->_use_sku ?  $product->getSku() : $product->getProductId();
                $cart_content[] = array(
                    'id' => $product_id,
                    'price' => (float) $product->getPrice(),
                    'quantity' => (int) $product->getQty()
                );
            }

            $currency_code = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        } catch (\Exception $exc) {
            // do nothing
        }

        return array('item' => $cart_content, 'currency' => $currency_code);
    }

    private function get_sale_data()
    {
        $transaction_id = '';
        $purchased_products = array();
        $currency_code = '';

        try {
            $order_id = $this->_cart->getLastOrderId();
            $order = $this->_order->load($order_id);

            //get transaction id (known to customer)
            $transaction_id = $order->getIncrementId();

            //get purchased products
            $products = $order->getAllVisibleItems();
            $pro_count = count($products);
            $discount = ($order->getDiscountAmount())/$pro_count;
            $shipping = ((float) $order->getShippingAmount())/$pro_count;
            foreach ($products as $product) {
                $product_id = $this->_use_sku ? $product->getSku() : $product->getProductId();
                $price = (float) $product->getPrice();
                $tax = (float) $product->getTaxAmount();
                $price = $price + $tax + $shipping + $discount;
                $purchased_products[] = array(
                    'id' => $product_id,
                    'price' => (float) $price,
                    'quantity' => (int) $product->getQtyOrdered()
                );
            }

            $currency_code = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        } catch (\Exception $exc) {
            //do nothing
        }

        return array('id' => $transaction_id, 'item' => $purchased_products, 'currency' => $currency_code);
    }
}
