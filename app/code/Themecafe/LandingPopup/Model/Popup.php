<?php
namespace Themecafe\LandingPopup\Model;

use Magento\Framework\Exception\LocalizedException as CoreException;

class Popup extends \Magento\Framework\Model\AbstractModel
{
	
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Themecafe\LandingPopup\Model\ResourceModel\Popup');
    }
}