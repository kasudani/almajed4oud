<?php
namespace Themecafe\LandingPopup\Model\System\Config;

class Status implements \Magento\Framework\Option\ArrayInterface
{
    const STATUS_ENABLED = 1;

    /**
     * @var int
     */
    const STATUS_DISABLED = 0;
    /**
     * @return array
     */
    public function toOptionArray()
    {
 
        $options = [
            ['value' => self::STATUS_DISABLED, 'label' => __('Disabled')],
            ['value' => self::STATUS_ENABLED, 'label' => __('Enabled')]
        ];
        return $options;
    }
}