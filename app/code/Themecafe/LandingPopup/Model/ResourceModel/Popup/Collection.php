<?php
namespace Themecafe\LandingPopup\Model\ResourceModel\Popup;
 
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
 
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     * 
     * @var string
     */
    protected $_eventPrefix = 'themecafe_landingpopup_popup_collection';

    /**
     * Event object
     * 
     * @var string
     */
    protected $_eventObject = 'popup_collection';
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Themecafe\LandingPopup\Model\Popup', 'Themecafe\LandingPopup\Model\ResourceModel\Popup');
    }
    
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(\Zend_Db_Select::GROUP);
        return $countSelect;
    }
    protected function _toOptionArray($valueField = 'id', $labelField = 'title', $additional = [])
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }
}