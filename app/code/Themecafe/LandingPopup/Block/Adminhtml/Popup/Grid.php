<?php
namespace Themecafe\LandingPopup\Block\Adminhtml\Popup;
 
class Grid extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'Themecafe_LandingPopup';
        $this->_controller = 'adminhtml_popup';
        $this->_headerText = __('Popup');
        $this->_addButtonLabel = __('Add New Popup');
        parent::_construct();
    }
}