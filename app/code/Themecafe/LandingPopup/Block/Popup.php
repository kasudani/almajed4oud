<?php
namespace Themecafe\LandingPopup\Block;

class Popup extends \Magento\Framework\View\Element\Template {
    
    const COOKIE_NAME = "Themecafe-langingpopup";
    const REGISTER_DURATION = 'Themecafe-landingpopup-duration';
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Themecafe\LandingPopup\Model\ResourceModel\Popup\CollectionFactory $collectionFactory,
        \Magento\Cms\Model\Template\FilterProvider $filterProvider,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory,
        \Themecafe\LandingPopup\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        array $data = array()
    ) {
        $this->scopeConfig = $context->getScopeConfig();
        $this->collectionFactory = $collectionFactory;
        $this->_filterProvider = $filterProvider;
        $this->session = $context->getSession();
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->helper = $helper;
        $this->registry = $registry;
        $this->store = $context->getStoreManager();
        parent::__construct($context, $data);
    }
    public function getCollection() {
        $data = $this->collectionFactory->create()->addFieldToFilter('is_active',1)->getLastItem();
        $html = $this->_filterProvider->getPageFilter()->filter($data->getContent());
        return $html;
    }
    public function setCustomCookie(){
        $duration = $this->getDuration();
        $metadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()->setDuration($duration * 3600);
        $this->_cookieManager->setPublicCookie(self::COOKIE_NAME,1,$metadata);
        $this->session->setCookieDuration($duration);
    }
    public function getCustomCookie() {
        $duration = $this->getDuration();
        if($this->session->getCookieDuration() == $duration){
            return $this->_cookieManager->getCookie(self::COOKIE_NAME);
        }
        else{
            $this->session->setCookieDuration($duration);
        }
    }
    public function getDuration(){
        $duration = $this->helper->getCookieDuration($this->helper->getStoreId());
        if(!$duration || $duration == 0){
            $duration = $this->helper->getDefaultCookieDuration($this->helper->getStoreId());
            
        }
        return $duration;
    }
}