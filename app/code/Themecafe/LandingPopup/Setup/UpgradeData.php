<?php

namespace Themecafe\LandingPopup\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class UpgradeData implements UpgradeDataInterface {

    protected $departmentFactory;

    public function __construct(
    \Themecafe\LandingPopup\Model\PopupFactory $popupFactory
    ) {
        $this->popupFactory = $popupFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context) {
         $modeldata = [[
            'title' => 'Default',
            'content' => '<div style="padding: 30px; background: #e54628; background: -webkit-linear-gradient(left top, #e54628, #212121); background: -o-linear-gradient(bottom right, #e54628, #212121); background: -moz-linear-gradient(bottom right, #e54628, #212121); background: linear-gradient(to bottom right, #e54628, #212121);color:#FFFFFF;">
                            <div>
                            <h2>Default Promotional Popup Title</h2>
                            </div>
                            <div style="width: 79%; display: inline-block;">
                            <p>&nbsp;</p>
                            <h3><strong>Get 10% off on all Gear products</strong></h3>
                            <p><i>Some text here</i></p>
                            </div>
                            <div style="width: 20%; display: inline-block;"><img src="{{media url="wysiwyg/ThemecafeLandingPopup/PNGPIX-COM-Travel-Bag-PNG-Transparent-Image-1.png"}}" alt="" width="300" height="350" /></div>
                            </div>',
             'is_active'=>1
        ],
        [
            'title' => 'Newsletter',
            'content' => '<div class="popup-subscribe" style="padding: 25px; width: 615px; background-image: url({{media url="wysiwyg/ThemecafeLandingPopup/mens-pants.jpg"}}); background-repeat: no-repeat; background-size: cover;">
                            <div class="em-wrapper-newsletter">
                            <div class="em-block-title">
                            <h2>Newsletter</h2>
                            </div>
                            <p style="width: 400px;">Sign up to our email newsletter to be the first to hear about great offers and more</p>
                            <div class="em-newsletter-style05">
                            <div class="block block-subscribe">{{block class="Magento\Newsletter\Block\Subscribe" template="subscribe.phtml"}}</div>
                            </div>
                            </div>
                            </div>',
            'is_active'=>1
        ]];
       
        foreach ($modeldata as $data) {
            $this->popupFactory->create()->setData($data)->save();
        }
    }

}
