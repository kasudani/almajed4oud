<?php

namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;

use Themecafe\LandingPopup\Controller\Adminhtml\Popup;

class Save extends Popup {

    /**
     * @return void
     */
    public function execute() {
        $isPost = $this->getRequest()->getPost();
        if ($isPost) {
            $popupModel = $this->_popupFactory->create();
            $popupId = $this->getRequest()->getParam('id');

            if ($popupId) {
                $popupModel->load($popupId);
                $popupModel->set();
            }
            $formData = $this->getRequest()->getParam('popup');
            
            $popupModel->setData($formData);
            try {
                // Save popup
                $popupModel->save();

                // Display success message
                $this->messageManager->addSuccess(__('The popup has been saved.'));

                // Check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $popupModel->getId(), '_current' => true]);
                    return;
                }

                // Go to grid page
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }

            $this->_getSession()->setFormData($formData);
            $this->_redirect('*/*/edit', ['id' => $popupId]);
        }
    }

}
