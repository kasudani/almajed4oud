<?php

namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;

use Themecafe\LandingPopup\Controller\Adminhtml\Popup;

class Edit extends Popup {
    /**
     * @return void
     */
    public function execute() {
        $popupId = $this->getRequest()->getParam('id');
        /** @var \Tutorial\SimpleNews\Model\News $model */
        $model = $this->_popupFactory->create();

        if ($popupId) {
            $model->load($popupId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This popup no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        // Restore previously entered form data from session
        $data = $this->_session->getPopupData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        
        $this->_coreRegistry->register('themecafe_popup', $model);
       
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        
        $resultPage->setActiveMenu('Themecafe_LandingPopup::addlandingpopup');
        $resultPage->getConfig()->getTitle()->prepend(__('New Popup'));
        
        return $resultPage;
    }

}
