<?php
namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Themecafe\LandingPopup\Controller\Adminhtml\Popup
{
    public function execute() {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Themecafe Landing Popup'));
        return $resultPage;
    }

}