<?php
namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;
 
use Themecafe\LandingPopup\Controller\Adminhtml\Popup;
 
class NewAction extends Popup
{
   /**
     * Create new popup action
     *
     * @return void
     */
   public function execute()
   {
        $this->_forward('edit');
   }
}
 