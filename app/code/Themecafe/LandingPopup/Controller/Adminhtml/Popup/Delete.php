<?php
 
namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;
 
use Themecafe\LandingPopup\Controller\Adminhtml\Popup;
 
class Delete extends Popup
{
   /**
    * @return void
    */
   public function execute()
   {
      $popupId = (int) $this->getRequest()->getParam('id');
 
      if ($popupId) {
         /** @var $popupModel \Mageworld\SimpleNews\Model\News */
         $popupModel = $this->_popupFactory->create();
         $popupModel->load($popupId);
 
         // Check this popup exists or not
         if (!$popupModel->getId()) {
            $this->messageManager->addError(__('This popup no longer exists.'));
         } else {
               try {
                  // Delete popup
                  $popupModel->delete();
                  $this->messageManager->addSuccess(__('The popup has been deleted.'));
 
                  // Redirect to grid page
                  $this->_redirect('*/*/');
                  return;
               } catch (\Exception $e) {
                   $this->messageManager->addError($e->getMessage());
                   $this->_redirect('*/*/edit', ['id' => $popupModel->getId()]);
               }
            }
      }
   }
}