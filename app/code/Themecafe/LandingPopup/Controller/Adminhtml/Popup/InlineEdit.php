<?php
namespace Themecafe\LandingPopup\Controller\Adminhtml\Popup;

class InlineEdit extends \Magento\Backend\App\Action
{
    /**
     * JSON Factory
     * 
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_jsonFactory;

    /**
     * Post Factory
     * 
     * @var \Themecafe\LandingPopu\Model\PopupFactory
     */
    protected $_popupFactory;

    /**
     * constructor
     * 
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     * @param \Themecafe\StoreLocator\Model\PostFactory $popupFactory
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Themecafe\LandingPopup\Model\PopupFactory $popupFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_jsonFactory = $jsonFactory;
        $this->_popupFactory = $popupFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultJson = $this->_jsonFactory->create();
        $error = false;
        $messages = [];
        $popupItems = $this->getRequest()->getParam('items', []);
        if (!($this->getRequest()->getParam('isAjax') && count($popupItems))) {
            return $resultJson->setData([
                'messages' => [__('Please correct the data sent.')],
                'error' => true,
            ]);
        }
        foreach (array_keys($popupItems) as $popupId) {
            /** @var \Themecafe\StoreLocator\Model\Post $popup */
            $popup = $this->_popupFactory->create()->load($popupId);
            try {
                $popupData = $popupItems[$popupId];//todo: handle dates
                $popup->addData($popupData);
                $popup->save();
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $messages[] = $this->getErrorWithPopupId($popup, $e->getMessage());
                $error = true;
            } catch (\RuntimeException $e) {
                $messages[] = $this->getErrorWithPopupId($popup, $e->getMessage());
                $error = true;
            } catch (\Exception $e) {
                $messages[] = $this->getErrorWithPopupId(
                    $popup,
                    __('Something went wrong while saving the Store.')
                );
                $error = true;
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add Post id to error message
     *
     * @param \Themecafe\StoreLocator\Model\Post $popup
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithPopupId(\Themecafe\LandingPopup\Model\Popup $popup, $errorText)
    {
        return '[Popup ID: ' . $popup->getId() . '] ' . $errorText;
    }
}
