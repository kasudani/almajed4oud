<?php
namespace Themecafe\LandingPopup\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $storeManager;
    protected $objectManager;

    const XML_PATH_COOKIEDURATION = 'themecafe_langingpopup/general/cookie_duration';
    const XML_PATH_DEFAULT_COOKIEDURATION = 'themecafe_langingpopup/general/default_cookie_duration';

    public function __construct(Context $context,
        ObjectManagerInterface $objectManager,
        StoreManagerInterface $storeManager
    ) {
        $this->objectManager = $objectManager;
        $this->storeManager  = $storeManager;
        parent::__construct($context);
    }
    public function getStoreId(){
        return $this->storeManager->getStore()->getId();
    }
    
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getCookieDuration($storeId = null)
    {
        return trim($this->getConfigValue(self::XML_PATH_COOKIEDURATION, $storeId));
    }
    public function getDefaultCookieDuration($storeId = null)
    {
        return trim($this->getConfigValue(self::XML_PATH_DEFAULT_COOKIEDURATION, $storeId));
    }

}