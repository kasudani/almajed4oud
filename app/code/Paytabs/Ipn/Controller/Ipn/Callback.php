<?php
namespace Paytabs\Ipn\Controller\Callback;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

if(!empty($_POST)){

	/*check paytab response code*/
	if(!empty($_POST['response_code']) && $_POST['response_code'] == 100){
		/*create magebto ObjectManager*/
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$customerSession = $objectManager->get('Magento\Customer\Model\Session');
		$connection = $resource->getConnection();

		/*save post data in log file*/
		$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/paytabs_ipn.log');
		$logger = new \Zend\Log\Logger();
		$logger->addWriter($writer);
		$logger->info(json_encode($_POST));

		/*get order_id form post data*/
		$order_number = $_POST['order_id'];

		/*get data from sales order according to order number*/
		$sales_order_sql = "Select * FROM sales_order Where increment_id = ".$order_number." AND state = 'pending_payment' AND status = 'pending_payment'";
		$sales_order_data = $connection->fetchAll($sales_order_sql);

		$order_id = '';
		$state = '';
		$status = '';
		if(!empty($sales_order_data)) {
			/*get order id*/
			$order_id = isset($sales_order_data[0]['entity_id'])?$sales_order_data[0]['entity_id']:'';
			
			/*get data from sales order grid according to order id */
			$sales_order_grid_select_sql = "Select shipping_name, billing_name, billing_address, shipping_address, shipping_information, shipping_and_handling, customer_email, customer_name, payment_method FROM sales_order_grid Where entity_id = ".$order_id."";
			$sales_order_grid_data = $connection->fetchAll($sales_order_grid_select_sql);

			$state = 'processing';
			$status = 'processing';
			/*update state & status on sales_order table*/
			$sales_order_sql = "UPDATE sales_order Set state = '".$state."', status = '".$status."' WHERE entity_id = ".$order_id."";
			$connection->exec($sales_order_sql);

			/*update status on sales_order_grid table*/
			$sales_order_grid_sql = "UPDATE sales_order_grid Set status = '".$status."' WHERE entity_id = ".$order_id."";
			$connection->exec($sales_order_grid_sql);

			/*Check invoice exist or not*/
			$sales_invoice_sql = "Select * FROM sales_invoice Where order_id = ".$order_id."";
			$sales_invoice_data = $connection->fetchAll($sales_invoice_sql);

			if(empty($sales_invoice_data)){
				/*Insert query for sales_invoice*/
				$store_id = $sales_order_data[0]['store_id'];
				$base_grand_total = $sales_order_data[0]['base_grand_total'];
				$shipping_tax_amount = $sales_order_data[0]['shipping_tax_amount'];
				$tax_amount = $sales_order_data[0]['tax_amount'];
				$base_tax_amount = $sales_order_data[0]['tax_amount'];
				$store_to_order_rate = $sales_order_data[0]['store_to_order_rate'];
				$base_shipping_tax_amount = $sales_order_data[0]['base_shipping_tax_amount'];
				$base_discount_amount = $sales_order_data[0]['base_discount_amount'];
				$base_to_order_rate = $sales_order_data[0]['base_to_order_rate'];
				$grand_total = $sales_order_data[0]['grand_total'];
				$shipping_amount = $sales_order_data[0]['shipping_amount'];
				$subtotal_incl_tax = $sales_order_data[0]['subtotal_incl_tax'];
				$base_subtotal_incl_tax = $sales_order_data[0]['base_subtotal_incl_tax'];
				$store_to_base_rate = $sales_order_data[0]['store_to_base_rate'];
				$base_shipping_amount = $sales_order_data[0]['base_shipping_amount'];
				$total_qty_ordered = $sales_order_data[0]['total_qty_ordered'];
				$base_to_global_rate = $sales_order_data[0]['base_to_global_rate'];
				$subtotal = $sales_order_data[0]['subtotal'];
				$base_subtotal = $sales_order_data[0]['base_subtotal'];
				$discount_amount = $sales_order_data[0]['discount_amount'];
				$billing_address_id = $sales_order_data[0]['billing_address_id'];
				$is_used_for_refund = '';
				$email_sent = $sales_order_data[0]['email_sent'];
				$send_email = $sales_order_data[0]['send_email'];
				$can_void_flag = 0;
				$state = 2;
				$shipping_address_id = $sales_order_data[0]['shipping_address_id'];
				$store_currency_code = $sales_order_data[0]['store_currency_code'];
				$transaction_id = $_POST['transaction_id'];
				$order_currency_code = $sales_order_data[0]['order_currency_code'];
				$base_currency_code = $sales_order_data[0]['base_currency_code'];
				$global_currency_code = $sales_order_data[0]['global_currency_code'];
				$increment_id = $sales_order_data[0]['increment_id'];
				$created_at = $sales_order_data[0]['created_at'];
				$updated_at = $sales_order_data[0]['updated_at'];
				$discount_tax_compensation_amount = $sales_order_data[0]['discount_tax_compensation_amount'];
				$base_discount_tax_compensation_amount = $sales_order_data[0]['base_discount_tax_compensation_amount'];
				$shipping_discount_tax_compensation_amount = $sales_order_data[0]['shipping_discount_tax_compensation_amount'];
				$base_shipping_discount_tax_compensation_amnt = $sales_order_data[0]['base_shipping_discount_tax_compensation_amnt'];
				$shipping_incl_tax = $sales_order_data[0]['shipping_incl_tax'];
				$base_shipping_incl_tax = $sales_order_data[0]['base_shipping_incl_tax'];
				$base_total_refunded = $sales_order_data[0]['base_total_refunded'];
				$discount_description = $sales_order_data[0]['discount_description'];
				$customer_note = $sales_order_data[0]['customer_note'];
				$customer_note_notify = $sales_order_data[0]['customer_note_notify'];
				$store_name = $sales_order_data[0]['store_name'];
				$customer_name = $sales_order_grid_data[0]['customer_name'];
				$customer_email = $sales_order_grid_data[0]['customer_email'];
				$customer_group_id = $sales_order_data[0]['customer_group_id'];
				$payment_method = $sales_order_grid_data[0]['payment_method'];
				$billing_name = $sales_order_grid_data[0]['billing_name'];
				$billing_address = $sales_order_grid_data[0]['billing_address'];
				$shipping_address = $sales_order_grid_data[0]['shipping_address'];
				$shipping_information = $sales_order_grid_data[0]['shipping_information'];
				$shipping_and_handling = $sales_order_grid_data[0]['shipping_and_handling'];

				$sales_invoice_ins_sql = "INSERT INTO sales_invoice (store_id, base_grand_total, shipping_tax_amount, tax_amount, base_tax_amount, store_to_order_rate , base_shipping_tax_amount, base_discount_amount, base_to_order_rate, grand_total, shipping_amount, subtotal_incl_tax, base_subtotal_incl_tax, store_to_base_rate, base_shipping_amount, total_qty, base_to_global_rate, subtotal, base_subtotal, discount_amount, billing_address_id, is_used_for_refund, order_id, email_sent, send_email, can_void_flag, state, shipping_address_id, store_currency_code, transaction_id, order_currency_code, base_currency_code, global_currency_code, increment_id, created_at, updated_at, discount_tax_compensation_amount, base_discount_tax_compensation_amount, shipping_discount_tax_compensation_amount, base_shipping_discount_tax_compensation_amnt, shipping_incl_tax, base_shipping_incl_tax, base_total_refunded, discount_description, customer_note, customer_note_notify)
				VALUES ('$store_id', '$base_grand_total','$shipping_tax_amount','$tax_amount','$base_tax_amount','$store_to_order_rate','$base_shipping_tax_amount','$base_discount_amount','$base_to_order_rate','$grand_total','$shipping_amount','$subtotal_incl_tax','$base_subtotal_incl_tax','store_to_base_rate','$base_shipping_amount','$total_qty_ordered','$base_to_global_rate','$subtotal','$base_subtotal','$discount_amount','$billing_address_id','$is_used_for_refund','$order_id','$email_sent','$send_email','$can_void_flag','$state','$shipping_address_id','$store_currency_code','$transaction_id','$order_currency_code','$base_currency_code','$global_currency_code','$increment_id','$created_at','$updated_at','$discount_tax_compensation_amount','$base_discount_tax_compensation_amount','$shipping_discount_tax_compensation_amount','$base_shipping_discount_tax_compensation_amnt','$shipping_incl_tax','$base_shipping_incl_tax','$base_total_refunded','$discount_description','$customer_note','customer_note_notify')";

				$connection->exec($sales_invoice_ins_sql);

				$lastInsertId = $connection->lastInsertId();

				/*get sales invoice grid increment id*/
				$get_sales_invoice_last_increment_id_sql = "SELECT increment_id FROM sales_invoice_grid ORDER BY entity_id DESC LIMIT 1;";
				$sales_invoice_grid_data = $connection->fetchAll($get_sales_invoice_last_increment_id_sql);
				$sales_invoice_grid_increment_id = ($sales_invoice_grid_data['0']['increment_id']+1);

				/*Insert query for sales_invoice_grid*/
				$sales_invoice_grid_ins_sql = "INSERT INTO sales_invoice_grid (entity_id,increment_id, state, store_id, store_name, order_id, order_increment_id, order_created_at, customer_name, customer_email, customer_group_id , payment_method , store_currency_code, order_currency_code, base_currency_code, global_currency_code, billing_name, billing_address, shipping_address, shipping_information, subtotal, shipping_and_handling, base_grand_total, grand_total, created_at, updated_at)
				VALUES ('$lastInsertId','$sales_invoice_grid_increment_id','$state', '$store_id','$store_name','$order_id','$increment_id','created_at','$customer_name','$customer_email','$customer_group_id','$payment_method','$store_currency_code','$order_currency_code','$base_currency_code','$global_currency_code','$billing_name','$billing_address','$shipping_address','$shipping_information', '$subtotal', '$shipping_and_handling', '$base_grand_total', '$grand_total', '$created_at', '$updated_at')";

				$connection->exec($sales_invoice_grid_ins_sql);

				/*get last insert entity_id form sales_invoice*/
				$sales_invoice_grid_sel_sql = "Select entity_id FROM sales_invoice Where order_id = ".$order_id."";
				$sales_invoice_entity_id = $connection->fetchAll($sales_invoice_grid_sel_sql);
				
				/*get all pruceage product from sales_order_item*/
				$sales_order_item_sql = "Select * FROM sales_order_item Where order_id = ".$order_id."";
				$sales_order_item_data = $connection->fetchAll($sales_order_item_sql);

				if(!empty($sales_order_item_data)){
					foreach ($sales_order_item_data as $key => $value) {
						$sales_parent_id = $sales_invoice_entity_id[0]['entity_id'];
						$sales_base_price = $value['base_price'];
						$sales_tax_amount = $value['tax_amount'];
						$sales_base_row_total = $value['base_row_total'];
						$sales_discount_amount = $value['discount_amount'];
						$sales_row_total = $value['row_total'];
						$sales_base_discount_amount = $value['base_discount_amount'];
						$sales_price_incl_tax = $value['price_incl_tax'];
						$sales_base_tax_amount = $value['base_tax_amount'];
						$sales_base_price_incl_tax = $value['base_price_incl_tax'];
						$sales_qty_ordered = $value['qty_ordered'];
						$sales_base_cost = $value['base_cost'];
						$sales_price = $value['price'];
						$sales_base_row_total_incl_tax = $value['base_row_total_incl_tax'];
						$sales_row_total_incl_tax = $value['row_total_incl_tax'];
						$sales_product_id = $value['product_id'];
						$sales_order_item_id = $value['item_id'];
						$sales_additional_data = $value['additional_data'];
						$sales_description = $value['description'];
						$sales_sku = $value['description'];
						$sales_name = $value['name'];
						$sales_discount_tax_compensation_amount = $value['discount_tax_compensation_amount'];
						$sales_base_discount_tax_compensation_amount = $value['base_discount_tax_compensation_amount'];
						$sales_tax_ratio = '';
						$sales_weee_tax_applied = $value['weee_tax_applied'];
						$sales_weee_tax_applied_amount = $value['weee_tax_applied_amount'];
						$sales_weee_tax_applied_row_amount = $value['weee_tax_applied_row_amount'];
						$sales_weee_tax_disposition = $value['weee_tax_disposition'];
						$sales_weee_tax_row_disposition = $value['weee_tax_row_disposition'];
						$sales_base_weee_tax_applied_amount = $value['base_weee_tax_applied_amount'];
						$sales_base_weee_tax_applied_row_amnt = $value['base_weee_tax_applied_row_amnt'];
						$sales_base_weee_tax_disposition = $value['base_weee_tax_disposition'];
						$sales_base_weee_tax_row_disposition = $value['base_weee_tax_row_disposition'];
						
						$sales_order_item_ins_sql = "INSERT INTO sales_invoice_item (parent_id, base_price, tax_amount, base_row_total, discount_amount, row_total, base_discount_amount, price_incl_tax, base_tax_amount, base_price_incl_tax, qty, base_cost, price, base_row_total_incl_tax, row_total_incl_tax, product_id, order_item_id, additional_data,description, sku, name, discount_tax_compensation_amount, base_discount_tax_compensation_amount, tax_ratio, weee_tax_applied, weee_tax_applied_amount, weee_tax_applied_row_amount, weee_tax_disposition, weee_tax_row_disposition, base_weee_tax_applied_amount, base_weee_tax_applied_row_amnt, base_weee_tax_disposition, base_weee_tax_row_disposition)
						VALUES ('$sales_parent_id', '$sales_base_price', '$sales_tax_amount', '$sales_base_row_total', '$sales_discount_amount', '$sales_row_total', '$sales_base_discount_amount', '$sales_price_incl_tax', '$sales_base_tax_amount', '$sales_base_price_incl_tax', '$sales_qty_ordered', '$sales_base_cost', '$sales_price', '$sales_base_row_total_incl_tax', '$sales_row_total_incl_tax', '$sales_product_id', '$sales_order_item_id', '$sales_additional_data', '$sales_description', '$sales_sku', '$sales_name', '$sales_discount_tax_compensation_amount', '$sales_base_discount_tax_compensation_amount', '$sales_tax_ratio', '$sales_weee_tax_applied', '$sales_weee_tax_applied_amount', '$sales_weee_tax_applied_row_amount', '$sales_weee_tax_disposition', '$sales_weee_tax_row_disposition', '$sales_base_weee_tax_applied_amount', '$sales_base_weee_tax_applied_row_amnt', '$sales_base_weee_tax_disposition', '$sales_base_weee_tax_row_disposition')";

						$connection->exec($sales_order_item_ins_sql);
					}
				}

				/*send eamil to customer*/
				$order = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
				$emailSender = $objectManager->create('\Magento\Sales\Model\Order\Email\Sender\OrderSender');
				$emailSender->send($order);
			}
		}
	}
}
die;
