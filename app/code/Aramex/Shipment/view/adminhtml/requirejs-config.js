var config = {
    map: {
        '*': {
            'aramexmass'    : 'Aramex_Shipment/js/aramexmass',
            'aramexcommon'    : 'Aramex_Shipment/js/common',
            'chained'    : 'Aramex_Shipment/js/jquery.chained'
        }
    }
};