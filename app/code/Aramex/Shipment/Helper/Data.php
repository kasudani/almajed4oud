<?php

namespace Aramex\Shipment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    protected $_wsdlBasePath;
    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $om
     * 
     */
    public function __construct(
    \Magento\Framework\Module\Dir\Reader $reader, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_wsdlBasePath = $reader->getModuleDir('etc', 'Aramex_Shipment') . '/wsdl/Aramex/';
        $this->_scopeConfig = $scopeConfig;
    }

    public function getClientInfo() {
        $account = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $username = $this->_scopeConfig->getValue('aramex/settings/user_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $password = $this->_scopeConfig->getValue('aramex/settings/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $pin = $this->_scopeConfig->getValue('aramex/settings/account_pin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $entity = $this->_scopeConfig->getValue('aramex/settings/account_entity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_code = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return array(
            'AccountCountryCode' => $country_code,
            'AccountEntity' => $entity,
            'AccountNumber' => $account,
            'AccountPin' => $pin,
            'UserName' => $username,
            'Password' => $password,
            'Version' => 'v1.0',
            'Source' => 31
        );
    }

    public function getClientInfoCOD() {
        $account = $this->_scopeConfig->getValue('aramex/settings/cod_account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $username = $this->_scopeConfig->getValue('aramex/settings/user_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $password = $this->_scopeConfig->getValue('aramex/settings/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $pin = $this->_scopeConfig->getValue('aramex/settings/cod_account_pin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $entity = $this->_scopeConfig->getValue('aramex/settings/cod_account_entity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_code = $this->_scopeConfig->getValue('aramex/settings/cod_account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return array(
            'AccountCountryCode' => $country_code,
            'AccountEntity' => $entity,
            'AccountNumber' => $account,
            'AccountPin' => $pin,
            'UserName' => $username,
            'Password' => $password,
            'Version' => 'v1.0',
            'Source' => 31
        );
    }
    public function getWsdlPath() {
        if ($this->_scopeConfig->getValue('aramex/config/sandbox_flag', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1) {
           $path = $this->_wsdlBasePath . 'TestMode/';
        }else{
			$path = $this->_wsdlBasePath;
		}
        return $path;
    }
    
    /* default user account to working with location api so we  used static account */
    public function getStaticClientInfo() {
        return array(
            'AccountCountryCode' => 'JO',
            'AccountEntity' => 'AMM',
            'AccountNumber' => '20016',
            'AccountPin' => '331421',
            'UserName' => 'testingapi@aramex.com',
            'Password' => 'R123456789$r',
            'Version' => 'v1.0',
            'Source' => NULL
        );
    }

}
