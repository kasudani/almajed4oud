<?php

namespace Aramex\Shipment\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class Shipment extends \Magento\Backend\App\Action {

    const XML_PATH_TRANS_IDENTITY_EMAIL = 'trans_email/ident_general/email';
    const XML_PATH_TRANS_IDENTITY_NAME = 'trans_email/ident_general/name';
    const XML_PATH_SHIPMENT_EMAIL_TEMPLATE = 'aramex/template/shipment_template';
    const XML_PATH_SHIPMENT_EMAIL_COPY_TO = 'aramex/template/copy_to';
    const XML_PATH_SHIPMENT_EMAIL_COPY_METHOD = 'aramex/template/copy_method';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $request;
    protected $_scopeConfig;
    protected $shipmentLoader;
    protected $_transportBuilder;
    protected $_storeManager;
    protected $_request;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\App\Request\Http $request, \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->shipmentLoader = $shipmentLoader;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_request = $context->getRequest();

        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute() {
        $post = $this->getRequest()->getPost();
        $order_id= $this->getRequest()->getParam('order_id');
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $helperCore = $objectManager->create('\Aramex\Core\Helper\Data');
        $baseUrl = $helper->getWsdlPath();
        //SOAP object
        $soapClient = new \Zend\Soap\Client($baseUrl . 'shipping.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);


        $aramex_errors = false;


        $flag = true;
        $error = "";
        try {
            if (empty($post)) {
                echo($this->__('Invalid form data.'));
                die();
            }
            /* here's your form processing */
           // $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($post['aramex_shipment_original_reference']);
            $order = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);
            $payment = $order->getPayment();
            $totalWeight = 0;
            $totalItems = 0;
            $items = $order->getAllItems();
            $descriptionOfGoods = '';
            foreach ($order->getAllVisibleItems() as $itemname) {
                $descriptionOfGoods .= $itemname->getId() . ' - ' . trim($itemname->getName());
            }
            $descriptionOfGoods = substr($descriptionOfGoods, 0, 65);
            $aramex_items_counter = 0;
            $totalItems = (trim($post['number_pieces']) == '') ? 1 : (int) $post['number_pieces'];
            $aramex_atachments = array();
            //attachment
            for ($i = 1; $i <= 3; $i++) {
                $fileName = @$_FILES['file' . $i]['name'];
                if (isset($fileName) != '') {
                    $fileName = explode('.', $fileName);
                    $fileName = $fileName[0]; //filename without extension
                    $fileData = '';
                    if ($_FILES['file' . $i]['tmp_name'] != '')
                        $fileData = file_get_contents($_FILES['file' . $i]['tmp_name']);
                    //$fileData = base64_encode($fileData); //base64binary encode
                    $ext = pathinfo($_FILES['file' . $i]['name'], PATHINFO_EXTENSION); //file extension
                    if ($fileName && $ext && $fileData)
                        $aramex_atachments[] = array(
                            'FileName' => $fileName,
                            'FileExtension' => $ext,
                            'FileContents' => $fileData
                        );
                }
            }

            $totalWeight = $post['order_weight'];
            $params = array();

            //shipper parameters
            $params['Shipper'] = array(
                'Reference1' => $post['aramex_shipment_shipper_reference'], //'ref11111',
                'Reference2' => '',
                'AccountNumber' => ($post['aramex_shipment_info_billing_account'] == 1) ? $post['aramex_shipment_shipper_account'] : $post['aramex_shipment_shipper_account'], //'43871',
                //Party Address
                'PartyAddress' => array(
                    'Line1' => addslashes($post['aramex_shipment_shipper_street']), //'13 Mecca St',
                    'Line2' => '',
                    'Line3' => '',
                    'City' => $post['aramex_shipment_shipper_city'], //'Dubai',
                    'StateOrProvinceCode' => $post['aramex_shipment_shipper_state'], //'',
                    'PostCode' => $post['aramex_shipment_shipper_postal'],
                    'CountryCode' => $post['aramex_shipment_shipper_country'], //'AE'
                ),
                //Contact Info
                'Contact' => array(
                    'Department' => '',
                    'PersonName' => $post['aramex_shipment_shipper_name'], //'Suheir',
                    'Title' => '',
                    'CompanyName' => $post['aramex_shipment_shipper_company'], //'Aramex',
                    'PhoneNumber1' => $post['aramex_shipment_shipper_phone'], //'55555555',
                    'PhoneNumber1Ext' => '',
                    'PhoneNumber2' => '',
                    'PhoneNumber2Ext' => '',
                    'FaxNumber' => '',
                    'CellPhone' => $post['aramex_shipment_shipper_phone'],
                    'EmailAddress' => $post['aramex_shipment_shipper_email'], //'',
                    'Type' => ''
                ),
            );


            //consinee parameters
            $params['Consignee'] = array(
                'Reference1' => $post['aramex_shipment_receiver_reference'], //'',
                'Reference2' => '',
                'AccountNumber' => ($post['aramex_shipment_info_billing_account'] == 2) ? $post['aramex_shipment_shipper_account'] : '',
                //Party Address
                'PartyAddress' => array(
                    'Line1' => $post['aramex_shipment_receiver_street'], //'15 ABC St',
                    'Line2' => '',
                    'Line3' => '',
                    'City' => $post['aramex_shipment_receiver_city'], //'Amman',
                    'StateOrProvinceCode' => '',
                    'PostCode' => $post['aramex_shipment_receiver_postal'],
                    'CountryCode' => $post['aramex_shipment_receiver_country'], //'JO'
                ),
                //Contact Info
                'Contact' => array(
                    'Department' => '',
                    'PersonName' => $post['aramex_shipment_receiver_name'], //'Mazen',
                    'Title' => '',
                    'CompanyName' => $post['aramex_shipment_receiver_company'], //'Aramex',
                    'PhoneNumber1' => $post['aramex_shipment_receiver_phone'], //'6666666',
                    'PhoneNumber1Ext' => '',
                    'PhoneNumber2' => '',
                    'PhoneNumber2Ext' => '',
                    'FaxNumber' => '',
                    'CellPhone' => $post['aramex_shipment_receiver_phone'],
                    'EmailAddress' => $post['aramex_shipment_receiver_email'], //'mazen@aramex.com',
                    'Type' => ''
                )
            );
            //new

            if ($post['aramex_shipment_info_billing_account'] == 3) {
                $params['ThirdParty'] = array(
                    'Reference1' => $post['aramex_shipment_shipper_reference'], //'ref11111',
                    'Reference2' => '',
                    'AccountNumber' => $post['aramex_shipment_shipper_account'], //'43871',
                    //Party Address
                    'PartyAddress' => array(
                        'Line1' => addslashes(Mage::getStoreConfig('aramexsettings/shipperdetail/address')), //'13 Mecca St',
                        'Line2' => '',
                        'Line3' => '',
                        'City' => Mage::getStoreConfig('aramexsettings/shipperdetail/city'), //'Dubai',
                        'StateOrProvinceCode' => Mage::getStoreConfig('aramexsettings/shipperdetail/state'), //'',
                        'PostCode' => Mage::getStoreConfig('aramexsettings/shipperdetail/postalcode'),
                        'CountryCode' => Mage::getStoreConfig('aramexsettings/shipperdetail/country'), //'AE'
                    ),
                    //Contact Info
                    'Contact' => array(
                        'Department' => '',
                        'PersonName' => Mage::getStoreConfig('aramexsettings/shipperdetail/name'), //'Suheir',
                        'Title' => '',
                        'CompanyName' => Mage::getStoreConfig('aramexsettings/shipperdetail/company'), //'Aramex',
                        'PhoneNumber1' => Mage::getStoreConfig('aramexsettings/shipperdetail/phone'), //'55555555',
                        'PhoneNumber1Ext' => '',
                        'PhoneNumber2' => '',
                        'PhoneNumber2Ext' => '',
                        'FaxNumber' => '',
                        'CellPhone' => Mage::getStoreConfig('aramexsettings/shipperdetail/phone'),
                        'EmailAddress' => Mage::getStoreConfig('aramexsettings/shipperdetail/email'), //'',
                        'Type' => ''
                    ),
                );
            }

            ////// add COD
            $services = array();
            if ($post['aramex_shipment_info_product_type'] == "CDA") {
                if ($post['aramex_shipment_info_service_type'] == null) {
                    array_push($services, "CODS");
                } elseif (!in_array("CODS", $post['aramex_shipment_info_service_type'])) {
                    $services = array_merge($services, $post['aramex_shipment_info_service_type']);
                    array_push($services, "CODS");
                } else {
                    $services = array_merge($services, $post['aramex_shipment_info_service_type']);
                }
            } else {
                if ($post['aramex_shipment_info_service_type'] == null) {
                    $post['aramex_shipment_info_service_type'] = array();
                }

                $services = array_merge($services, $post['aramex_shipment_info_service_type']);
            }

            $services = implode(',', $services);
            ///// add COD and
            // Other Main Shipment Parameters
            $params['Reference1'] = $post['aramex_shipment_info_reference']; //'Shpt0001';
            $params['Reference2'] = '';
            $params['Reference3'] = '';
            $params['ForeignHAWB'] = $post['aramex_shipment_info_foreignhawb'];

            $params['TransportType'] = 0;
            $params['ShippingDateTime'] = time(); //date('m/d/Y g:i:sA');
            $params['DueDate'] = time() + (7 * 24 * 60 * 60); //date('m/d/Y g:i:sA');
            $params['PickupLocation'] = 'Reception';
            $params['PickupGUID'] = '';
            $params['Comments'] = $post['aramex_shipment_info_comment'];
            $params['AccountingInstrcutions'] = '';
            $params['OperationsInstructions'] = '';
            $params['Details'] = array(
                'Dimensions' => array(
                    'Length' => '0',
                    'Width' => '0',
                    'Height' => '0',
                    'Unit' => 'cm'
                ),
                'ActualWeight' => array(
                    'Value' => $totalWeight,
                    'Unit' => $post['weight_unit']
                ),
                'ProductGroup' => $post['aramex_shipment_info_product_group'], //'EXP',
                'ProductType' => $post['aramex_shipment_info_product_type'], //,'PDX'
                'PaymentType' => $post['aramex_shipment_info_payment_type'],
                'PaymentOptions' => $post['aramex_shipment_info_payment_option'], //$post['aramex_shipment_info_payment_option']
                'Services' => $services,
                'NumberOfPieces' => $totalItems,
                'DescriptionOfGoods' => (trim($post['aramex_shipment_description']) == '') ? $descriptionOfGoods : $post['aramex_shipment_description'],
                'GoodsOriginCountry' => $post['aramex_shipment_shipper_country'], //'JO',
                'Items' => $totalItems,
            );
            if (count($aramex_atachments)) {
                $params['Attachments'] = $aramex_atachments;
            }

            $params['Details']['CashOnDeliveryAmount'] = array(
                'Value' => $post['aramex_shipment_info_cod_amount'],
                'CurrencyCode' => $post['aramex_shipment_currency_code']
            );

            $params['Details']['CustomsValueAmount'] = array(
                'Value' => $post['aramex_shipment_info_custom_amount'],
                'CurrencyCode' => $post['aramex_shipment_currency_code']
            );

            $major_par['Shipments'][] = $params;

            if ($post['aramex_shipment_shipper_account_show'] == 1) {
                $clientInfo = $helper->getClientInfo();
            } else {
                $clientInfo = $helper->getClientInfoCOD();
            }

            $major_par['ClientInfo'] = $clientInfo;
            $report_id = (int) $this->_scopeConfig->getValue('aramex/config/report_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if (!$report_id) {
                $report_id = 9729;
            }
            $major_par['LabelInfo'] = array(
                'ReportID' => $report_id, //'9201',
                'ReportType' => 'URL'
            );

            $formSession = $objectManager->create('\Magento\Backend\Model\Session');
            $formSession->setData("form_data", $post);


            try {

                //create shipment call
                $auth_call = $soapClient->CreateShipments($major_par);
                if ($auth_call->HasErrors) {
                    if (empty($auth_call->Shipments)) {
                        if (count($auth_call->Notifications->Notification) > 1) {
                            foreach ($auth_call->Notifications->Notification as $notify_error) {
                                $this->messageManager->addError('Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message);
                            }
                        } else {
                            $this->messageManager->addError('Aramex: ' . $auth_call->Notifications->Notification->Code . ' - ' . $auth_call->Notifications->Notification->Message);
                        }
                    } elseif (isset($auth_call->Notifications->Notification)) {
                        $this->messageManager->addError('Aramex: ' . $auth_call->Notifications->Notification->Code . ' - ' . $auth_call->Notifications->Notification->Message);
                    } else {
                        if (count($auth_call->Shipments->ProcessedShipment->Notifications->Notification) > 1) {
                            $notification_string = '';
                            foreach ($auth_call->Shipments->ProcessedShipment->Notifications->Notification as $notification_error) {
                                $notification_string .= $notification_error->Code . ' - ' . $notification_error->Message . ' <br />';
                            }
                            $this->messageManager->addError($notification_string);
                        } else {
                            $this->messageManager->addError('Aramex: ' . $auth_call->Shipments->ProcessedShipment->Notifications->Notification->Code . ' - ' . $auth_call->Shipments->ProcessedShipment->Notifications->Notification->Message);
                        }
                    }

                    $resultRedirect->setUrl($post['aramex_shipment_referer'] . 'aramexpopup/show', ['_current' => true]);
                    return $resultRedirect;
                } else {

                    $data = [
                        'items' => $post['aramex_items'],
                        'comment_text' => "AWB No. " . $auth_call->Shipments->ProcessedShipment->ID . " - Order No. " . $auth_call->Shipments->ProcessedShipment->Reference1 . "- <a style='cursor:pointer' onclick='myObj.printLabel(" . $auth_call->Shipments->ProcessedShipment->ID . ");'>Print Label</a>",
                        'comment_customer_notify' => true,
                        'is_visible_on_front' => true
                    ];
                    if ($order->canShip() && $post['aramex_return_shipment_creation_date'] == "create") {
                        //$this->shipmentLoader->setOrderId($post['aramex_shipment_original_reference']); 
                        $this->shipmentLoader->setOrderId($order_id);
                        $this->shipmentLoader->setShipmentId(null);
                        $this->shipmentLoader->setShipment($data);
                        $this->shipmentLoader->setTracking(null);
                        $shipment = $this->shipmentLoader->load();
                        if (!$shipment) {
                            $this->_forward('noroute');
                            return;
                        }
                        if (!empty($data['comment_text'])) {
                            $shipment->addComment(
                                    $data['comment_text'], isset($data['comment_customer_notify']), isset($data['is_visible_on_front'])
                            );

                            $shipment->setCustomerNote($data['comment_text']);
                            $shipment->setCustomerNoteNotify(isset($data['comment_customer_notify']));
                        }

///////////// block shipment
                        $shipment->register();
                        $this->_saveShipment($shipment);
                        $ship = true;
                        /* sending mail */
                        if ($ship) {
                            if ($post['aramex_email_customer'] == 'yes') {

                                /* send shipment mail */
                                $storeId = $order->getStore()->getId();
                                $copyTo = $helperCore->getEmails(self:: XML_PATH_SHIPMENT_EMAIL_COPY_TO, $storeId);
                                $copyMethod = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_COPY_METHOD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                                $templateId = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

                                if ($order->getCustomerIsGuest()) {
                                    $customerName = $order->getBillingAddress()->getName();
                                } else {
                                    $customerName = $order->getCustomerName();
                                }

                                $shipments_id = $auth_call->Shipments->ProcessedShipment->ID;
                                $templateParams = array(
                                    'order' => $order,
                                    'customerName' => $customerName,
                                    'shipments_id' => $shipments_id
                                );
                                $senderName = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_NAME);
                                $senderEmail = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_EMAIL);
								
                                if ($copyTo == "") {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(),  $customerName)
                                            ->getTransport();

                                }
								
                                if ($copyTo !== "" && $copyMethod == 'bcc') {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(), $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
                                }
                                if ($copyTo !== "" && $copyMethod == 'copy') {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(), $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
                                }

                                try {
                                    $transport->sendMessage();
                                } catch (Exception $ex) {
                                    $this->messageManager->addError($ex->getMessage());
                                }
                            }

                            $this->messageManager->addSuccess(
                                    'Aramex Shipment Number: ' . $auth_call->Shipments->ProcessedShipment->ID . ' has been created.'
                            );
                        }
                    } elseif ($post['aramex_return_shipment_creation_date'] == "return") {
					
						$shipments_id = $auth_call->Shipments->ProcessedShipment->ID;
								/* send shipment mail */
                                $storeId = $order->getStore()->getId();
                                $copyTo = $helperCore->getEmails(self:: XML_PATH_SHIPMENT_EMAIL_COPY_TO, $storeId);
                                $copyMethod = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_COPY_METHOD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                                $templateId = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

                                if ($order->getCustomerIsGuest()) {
                                    $customerName = $order->getBillingAddress()->getName();
                                } else {
                                    $customerName = $order->getCustomerName();
                                }

						if ($post['aramex_email_customer'] == 'yes') {
                                $templateParams = array(
                                    'order' => $order,
                                    'customerName' => $customerName,
                                    'shipments_id' => $shipments_id
                                );
                                $senderName = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_NAME);
                                $senderEmail = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_EMAIL);
								
								
                                if ($copyTo == "") {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(),  $customerName)
                                            ->getTransport();

                                }

                                if ($copyTo !== "" && $copyMethod == 'bcc') {
				
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(),  $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
										
											
                                }
                                if ($copyTo !== "" && $copyMethod == 'copy') {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(), $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
                                }

                                try {
                                    $transport->sendMessage();
                                } catch (Exception $ex) {
                                    $this->messageManager->addError($ex->getMessage());
                                }
						}
						
                        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
                        //. " - <a href='javascript:void(0);' onclick='myObj.printLabel();'>Print Label</a>"
                        $message = "Aramex Shipment Return Order AWB No. " . $auth_call->Shipments->ProcessedShipment->ID . " - Order No. " . $auth_call->Shipments->ProcessedShipment->Reference1 . " - <a style='cursor:pointer' onclick='myObj.printLabel(" . $auth_call->Shipments->ProcessedShipment->ID . ");'>Print Label</a>";
                        $this->messageManager->addSuccess('Aramex Shipment Return Order Number: ' . $auth_call->Shipments->ProcessedShipment->ID . ' has been created.');
                        $order->addStatusToHistory($order->getStatus(), $message, false);
                        $order->save();
                    } else {
                        $this->messageManager->addError('Cannot do shipment for the order.');
                    }
                }
            } catch (Exception $e) {
                $aramex_errors = true;
                $this->messageManager->addError('adminhtml/session')->addError($e->getMessage());
            }

            if ($aramex_errors) {
                $strip = strstr($post['aramex_shipment_referer'], "aramexpopup", true);
                $url = $strip;
                if (empty($strip)) {
                    $url = $post['aramex_shipment_referer'];
                }
                $resultRedirect->setUrl($url . 'aramexpopup/show');
                return $resultRedirect;
            } else {
                $resultRedirect->setUrl($post['aramex_shipment_referer']);
                return $resultRedirect;
            }
        } catch (Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }

    protected function _saveShipment($shipment) {
        $shipment->getOrder()->setIsInProcess(true);
        $transaction = $this->_objectManager->create(
                'Magento\Framework\DB\Transaction'
        );
        $transaction->addObject(
                $shipment
        )->addObject(
                $shipment->getOrder()
        )->save();

        return $this;
    }

    private function mass($post_out) {

        $post = array();
        $params = array();
        parse_str($post_out['str'], $params);
        $orders = array();
        $post['aramex_shipment_shipper_country'] = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        //check "pending" status
        if (count($post_out["selectedOrders"])) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            foreach ($post_out["selectedOrders"] as $key => $order_id) {
                $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId((int) $order_id);
                if ($order->getStatus() == "pending") {
                    $shipping = $order->getShippingAddress();
                    $shippingCountry = ($shipping) ? $shipping->getData('country_id') : '';
                    if ($shippingCountry == $post_out['aramex_shipment_shipper_country']) {
                        $orders[$key]['method'] = "DOM";
                    } else {
                        $orders[$key]['method'] = "EXP";
                    }
                    $orders[$key]['order_id'] = $order_id;
                }
            }

            //domestic metods must be first 
            $dom = array();
            $exp = array();
            foreach ($orders as $key => $order_item) {
                if ($order_item['method'] == 'DOM') {
                    $dom[$key]['method'] = "DOM";
                    $dom[$key]['order_id'] = $order_item['order_id'];
                } else {
                    $exp[$key]['method'] = "EXP";
                    $exp[$key]['order_id'] = $order_item['order_id'];
                }
            }
            $orders = array();
            $total = count($dom) + count($exp);
            for ($i = 0; $i < $total; $i++) {
                foreach ($dom as $key => $item) {
                    $orders[$key]['method'] = "DOM";
                    $orders[$key]['order_id'] = $item['order_id'];
                }
                foreach ($exp as $key => $item) {
                    $orders[$key]['method'] = "EXP";
                    $orders[$key]['order_id'] = $item['order_id'];
                }
            }
        }

        if (count($orders)) {
            foreach ($orders as $key => $orderItem) {
                $post['aramex_shipment_original_reference'] = (int) $orderItem['order_id'];
                $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId((int) $orderItem['order_id']);
                $isShipped = false;
                $itemsv = $order->getAllVisibleItems();
                $totalWeight = 0;
                foreach ($itemsv as $itemvv) {
                    if ($itemvv->getWeight() != 0) {
                        $weight = $itemvv->getWeight() * $itemvv->getQtyOrdered();
                    } else {
                        $weight = 0.5 * $itemvv->getQtyOrdered();
                    }
                    $totalWeight += $weight;
                    if ($itemvv->getQtyOrdered() == $itemvv->getQtyShipped()) {
                        $isShipped = true;
                    }
                    //quontity  
                    $_qty = abs($itemvv->getQtyOrdered() - $itemvv->getQtyShipped());
                    if ($_qty == 0 and $isShipped) {
                        $_qty = intval($itemvv->getQtyShipped());
                    }

                    $post[$itemvv->getId()] = (string) $_qty;
                }

                foreach ($itemsv as $item) {

                    if ($item->getQtyOrdered() > $item->getQtyShipped() or $isShipped) {
                        $_qty = abs($item->getQtyOrdered() - $item->getQtyShipped());
                        if ($_qty == 0 && $isShipped) {
                            $_qty = intval($item->getQtyShipped());
                        }
                        $post['aramex_items'][$item->getId()] = $_qty;
                    }
                }

                $post['order_weight'] = (string) $totalWeight;
                $post['aramex_shipment_shipper_reference'] = $order->getIncrementId();
                $post['aramex_shipment_info_billing_account'] = 1;
                $post['aramex_shipment_shipper_account'] = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_street'] = $this->_scopeConfig->getValue('aramex/shipperdetail/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_city'] = $this->_scopeConfig->getValue('aramex/shipperdetail/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_state'] = $this->_scopeConfig->getValue('aramex/shipperdetail/state', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_postal'] = $this->_scopeConfig->getValue('aramex/shipperdetail/postalcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_name'] = $this->_scopeConfig->getValue('aramex/shipperdetail/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_company'] = $this->_scopeConfig->getValue('aramex/shipperdetail/company', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_phone'] = $this->_scopeConfig->getValue('aramex/shipperdetail/phone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_email'] = $this->_scopeConfig->getValue('aramex/shipperdetail/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

                //shipper parameters
                $post['aramex_shipment_receiver_reference'] = $order->getIncrementId();
                $shipping = $order->getShippingAddress();
                $post['aramex_shipment_receiver_street'] = ($shipping) ? $shipping->getData('street') : '';
                $post['aramex_shipment_receiver_city'] = ($shipping) ? $shipping->getData('city') : '';
                $post['aramex_shipment_receiver_postal'] = ($shipping) ? $shipping->getData('postcode') : '';
                $post['aramex_shipment_receiver_country'] = ($shipping) ? $shipping->getCountry() : '';
                $post['aramex_shipment_receiver_name'] = ($shipping) ? $shipping->getName() : '';

                //Contact Info
                $post['aramex_shipment_receiver_name'] = ($shipping) ? $shipping->getName() : '';
                $company_name = isset($billing) ? $billing->getData('company') : '';
                $company_name = ($company_name) ? $company_name : '';
                $company_name = (empty($company_name) and $shipping) ? $shipping->getName() : $company_name;
                $company_name = ($shipping) ? $shipping->getData('company') : '';
                if (Mage::getStoreConfig('aramexsettings/config/sandbox_flag') == 1) {
                    $company_name = (empty($company_name) and $shipping) ? $shipping->getName() : $company_name;
                }
                if (empty($company_name)) {
                    $company_name = $post['aramex_shipment_receiver_name'];
                }
                $post['aramex_shipment_receiver_company'] = ($company_name) ? $company_name : '';
                $post['aramex_shipment_receiver_phone'] = ($shipping) ? $shipping->getData('telephone') : '';
                $post['aramex_shipment_receiver_email'] = ($customer->getEmail()) ? $customer->getEmail() : $order->getData('customer_email');
                // Other Main Shipment Parameters
                $post['aramex_shipment_info_reference'] = $order->getIncrementId();
                $post['aramex_shipment_info_foreignhawb'] = '';
                $post['aramex_shipment_info_comment'] = '';
                $post['weight_unit'] = 'KG';

                if ($orderItem['method'] == 'DOM') {
                    $post['aramex_shipment_info_product_group'] = $orderItem['method'];
                    $post['aramex_shipment_info_product_type'] = ($params['aramex_shipment_info_product_type_dom']) ? $params['aramex_shipment_info_product_type_dom'] : "";
                    $post['aramex_shipment_info_payment_type'] = ($params['aramex_shipment_info_payment_type_dom']) ? $params['aramex_shipment_info_payment_type_dom'] : "";
                    $post['aramex_shipment_info_payment_option'] = ($params['aramex_shipment_info_payment_option_dom']) ? $params['aramex_shipment_info_payment_option_dom'] : "";
                    $post['aramex_shipment_info_service_type'] = ($params['aramex_shipment_info_service_type_dom']) ? $params['aramex_shipment_info_service_type_dom'] : "";
                    $post['aramex_shipment_currency_code'] = ($params['aramex_shipment_currency_code_dom']) ? $params['aramex_shipment_currency_code_dom'] : "";
                    $post['aramex_shipment_info_custom_amount'] = ($params['aramex_shipment_info_custom_amount_dom']) ? $params['aramex_shipment_info_custom_amount_dom'] : "";
                } else {
                    $post['aramex_shipment_info_product_group'] = $orderItem['method'];
                    $post['aramex_shipment_info_product_type'] = ($params['aramex_shipment_info_product_type']) ? $params['aramex_shipment_info_product_type'] : "";
                    $post['aramex_shipment_info_payment_type'] = ($params['aramex_shipment_info_payment_type']) ? $params['aramex_shipment_info_payment_type'] : "";
                    $post['aramex_shipment_info_payment_option'] = ($params['aramex_shipment_info_payment_option']) ? $params['aramex_shipment_info_payment_option'] : "";
                    $post['aramex_shipment_info_service_type'] = ($params['aramex_shipment_info_service_type']) ? $params['aramex_shipment_info_service_type'] : "";
                    $post['aramex_shipment_currency_code'] = ($params['aramex_shipment_currency_code']) ? $params['aramex_shipment_currency_code'] : "";
                    $post['aramex_shipment_info_custom_amount'] = ($params['aramex_shipment_info_custom_amount']) ? $params['aramex_shipment_info_custom_amount'] : "";
                }

                $itemsnamecounter = 1;
                $item_supplier_id = '';
                $aramex_shipment_description = '';
                foreach ($order->getAllVisibleItems() as $itemname) {
                    if ($itemname->getQtyOrdered() > $itemname->getQtyShipped()) {
                        $aramex_shipment_description = $aramex_shipment_description . $itemname->getId() . ' - ' . trim($itemname->getName());
                    }
                    $itemsnamecounter++;
                    $supplier_different = false;
                    $atleast_item_shipped = false;
                    if (!$item_supplier_id) {
                        $item_supplier_id = $itemname->getUdropshipVendor();
                    } else {
                        if ($item_supplier_id != $itemname->getUdropshipVendor()) {
                            if (!$supplier_different) {
                                $supplier_different = true;
                            }
                        }
                    }
                    if ($itemname->getQtyOrdered() == $itemname->getQtyShipped()) {
                        if (!$atleast_item_shipped) {
                            $atleast_item_shipped = true;
                        }
                    }
                }
                $post['aramex_shipment_description'] = $aramex_shipment_description;
                $post['aramex_shipment_info_cod_amount'] = ($order->getPayment()->getMethodInstance()->getCode() != 'ccsave') ? (string) round($order->getData('grand_total'), 2) : '';
                $post['aramex_return_shipment_creation_date'] = "create";
                $post['aramex_shipment_referer'] = 0;
                $replay = $this->postAction($mass = TRUE, $post, $orderItem['method']);
                if ($replay[1] == "DOM") {
                    $method = "Domestic Product Group";
                } else {
                    $method = "International Product Group";
                };

                if ($replay[2] == "error") {
                    $responce .= "<p class='aramex_red'>" . $replay[0] . " - " . $orderItem['order_id'] . ' not created. (' . $method . ')</p>';
                    break;
                } else {
                    $responce .= "<p class='aramex_green'> Aramex Shipment Number: " . $orderItem['order_id'] . ' has been created.(' . $method . ')</p>';
                }
            }
            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($responce));
        } else {
            $errors = "No orders with 'Pending' status selected.";
            return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($errors));
        }
    }

}

?>