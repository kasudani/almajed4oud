<?php

namespace Aramex\Shipment\Controller\Adminhtml\Index;

class Schedulepickup extends \Magento\Backend\App\Action {

    protected $_scopeConfig;
    protected $request;
    protected $shipmentLoader;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Framework\App\Request\Http $request, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader
    ) {
        $this->request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->shipmentLoader = $shipmentLoader;
        parent::__construct($context);
    }

    public function execute() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $account = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_code = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $post = $this->getRequest()->getPost();

        $response = array();
        $clientInfo = $helper->getClientInfo();
        try {
            if (empty($post)) {
                $response['type'] = 'error';
                $response['error'] = $this->__('Invalid form data.');
                print json_encode($response);
                die();
            }
            $post = $post['pickup'];
            $_order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($post['reference']);
            $pickupDate = strtotime($post['date']);
            $readyTimeH = $post['ready_hour'];
            $readyTimeM = $post['ready_minute'];
            $readyTime = mktime(($readyTimeH - 2), $readyTimeM, 0, date("m", $pickupDate), date("d", $pickupDate), date("Y", $pickupDate));

            $closingTimeH = $post['latest_hour'];
            $closingTimeM = $post['latest_minute'];
            $closingTime = mktime(($closingTimeH - 2), $closingTimeM, 0, date("m", $pickupDate), date("d", $pickupDate), date("Y", $pickupDate));
            $params = array(
                'ClientInfo' => $clientInfo,
                'Transaction' => array(
                    'Reference1' => $post['reference']
                ),
                'Pickup' => array(
                    'PickupContact' => array(
                        'PersonName' => html_entity_decode($post['contact']),
                        'CompanyName' => html_entity_decode($post['company']),
                        'PhoneNumber1' => html_entity_decode($post['phone']),
                        'PhoneNumber1Ext' => html_entity_decode($post['ext']),
                        'CellPhone' => html_entity_decode($post['mobile']),
                        'EmailAddress' => html_entity_decode($post['email'])
                    ),
                    'PickupAddress' => array(
                        'Line1' => html_entity_decode($post['address']),
                        'City' => html_entity_decode($post['city']),
                        'StateOrProvinceCode' => html_entity_decode($post['state']),
                        'PostCode' => html_entity_decode($post['zip']),
                        'CountryCode' => $post['country']
                    ),
                    'PickupLocation' => html_entity_decode($post['location']),
                    'PickupDate' => $readyTime,
                    'ReadyTime' => $readyTime,
                    'LastPickupTime' => $closingTime,
                    'ClosingTime' => $closingTime,
                    'Comments' => html_entity_decode($post['comments']),
                    'Reference1' => html_entity_decode($post['reference']),
                    'Reference2' => '',
                    'Vehicle' => $post['vehicle'],
                    'Shipments' => array(
                        'Shipment' => array()
                    ),
                    'PickupItems' => array(
                        'PickupItemDetail' => array(
                            'ProductGroup' => $post['product_group'],
                            'ProductType' => $post['product_type'],
                            'Payment' => $post['payment_type'],
                            'NumberOfShipments' => $post['no_shipments'],
                            'NumberOfPieces' => $post['no_pieces'],
                            'ShipmentWeight' => array('Value' => $post['text_weight'], 'Unit' => $post['weight_unit']),
                        ),
                    ),
                    'Status' => $post['status']
                )
            );
            $baseUrl = $helper->getWsdlPath();
            //SOAP object
            $soapClient = new \Zend\Soap\Client($baseUrl . 'shipping.wsdl');
            $soapClient->setSoapVersion(SOAP_1_1);
            try {
                $results = $soapClient->CreatePickup($params);
                if ($results->HasErrors) {
                    if (count($results->Notifications->Notification) > 1) {
                        $error = "";
                        foreach ($results->Notifications->Notification as $notify_error) {
                            $error.='Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message . "<br>";
                        }
                        $response['error'] = $error;
                    } else {
                        $response['error'] = 'Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message;
                    }
                    $response['type'] = 'error';
                } else {
                    $notify = false;
                    $visible = false;
                    $comment = "Pickup reference number ( <strong>" . $results->ProcessedPickup->ID . "</strong> ).";
                    $history = $_order->addStatusHistoryComment($comment, $_order->getStatus())
                            //->setIsVisibleOnFront($visible)
                            ->setIsCustomerNotified($notify)
                    ;
                    $history->save();


                    $shipmentId = null;
                    $shipment = $objectManager->create('Magento\Sales\Model\Order\Shipment')->getCollection()
                                    ->addFieldToFilter("order_id", $_order->getId())->load();

                    if ($shipment->count() > 0) {
                        foreach ($shipment as $_shipment) {
                            $shipmentId = $_shipment->getId();
                            break;
                        }
                    }
                    if ($shipmentId != null) {
                        $data = [['comment_text' => $comment]];
                        $this->shipmentLoader->setOrderId($post['order_id']); //4
                        $this->shipmentLoader->setShipmentId(null); //null
                        $this->shipmentLoader->setShipment($data);
                        $this->shipmentLoader->setTracking(null); // null
                        $shipment = $this->shipmentLoader->load();
                        /*
                        if (!$shipment) {
                            $this->_forward('noroute');
                            return;
                        }*/
                        
                        if (!empty($data['comment_text'])) {
                            $shipment->addComment(
                                    $data['comment_text'], isset($data['comment_customer_notify']), isset($data['is_visible_on_front'])
                            );

                            $shipment->setCustomerNote($data['comment_text']);
                            $shipment->setCustomerNoteNotify(isset($data['comment_customer_notify']));
                        }
                    }
                    $response['type'] = 'success';
                    $amount = "<p class='amount'>Pickup reference number ( <strong>" . $results->ProcessedPickup->ID . "</strong> ).</p>";
                    $response['html'] = $amount;
                }
            } catch (Exception $e) {
                $response['type'] = 'error';
                $response['error'] = $e->getMessage();
            }
        } catch (Exception $e) {
            $response['type'] = 'error';
            $response['error'] = $e->getMessage();
        }
        print json_encode($response);
        die();
    }

}
