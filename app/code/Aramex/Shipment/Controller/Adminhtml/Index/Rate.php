<?php

namespace Aramex\Shipment\Controller\Adminhtml\Index;

class Rate extends \Magento\Backend\App\Action {

    protected $_scopeConfig;
    protected $request;



    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Framework\App\Request\Http $request, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->request = $request;
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute() {
        //if (!$this->_isAllowedAction('Aramex_Shipment::rate')) {
        //    return false;
        //}

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $account = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_code = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $post = $this->getRequest()->getPost();

        $response = array();
        $clientInfo = $helper->getClientInfo();

        try {
            $country = $objectManager->create('Magento\Directory\Model\Config\Source\Country')->toOptionArray();
            foreach ($country as $item) {
                if ($item['value'] != "") {
                    if ($item['value'] == $country_code) {
                        $countryName = $item['label'];
                    }
                }
            }
            $countryName = ($countryName) ? $countryName : "";
            if (empty($post)) {
                $response['type'] = 'error';
                $response['error'] = 'Invalid form data.';
                print json_encode($response);
                die();
            }
            
            ////// add COD
            if($post['service_type'] == "CDA"){ $aramex_services = "CODS"; }else{ $aramex_services = ""; }
             ///// add COD and

            $params = array(
                'ClientInfo' => $clientInfo,
                'Transaction' => array(
                    'Reference1' => $post['reference']
                ),
                'OriginAddress' => array(
                    'StateOrProvinceCode' => html_entity_decode($post['origin_state']),
                    'City' => html_entity_decode($post['origin_city']),
                    'PostCode' => $post['origin_zipcode'],
                    'CountryCode' => $post['origin_country']
                ),
                'DestinationAddress' => array(
                    'StateOrProvinceCode' => html_entity_decode($post['destination_state']),
                    'City' => html_entity_decode($post['destination_city']),
                    'PostCode' => $post['destination_zipcode'],
                    'CountryCode' => $post['destination_country'],
                ),
                'ShipmentDetails' => array(
                    'PaymentType' => $post['payment_type'],
                    'ProductGroup' => $post['product_group'],
                    'ProductType' => $post['service_type'],
                    'Services'    => $aramex_services,
                    'ActualWeight' => array('Value' => $post['text_weight'], 'Unit' => $post['weight_unit']),
                    'ChargeableWeight' => array('Value' => $post['text_weight'], 'Unit' => $post['weight_unit']),
                    'NumberOfPieces' => $post['total_count']
                )
            );

            $baseUrl = $helper->getWsdlPath();
            //SOAP object
            $soapClient = new \Zend\Soap\Client($baseUrl . 'aramex-rates-calculator-wsdl.wsdl');

            $soapClient->setSoapVersion(SOAP_1_1);

            try {
                
                $results = $soapClient->CalculateRate($params);
                if ($results->HasErrors) {
                    if (count($results->Notifications->Notification) > 1) {
                        $error = "";
                        foreach ($results->Notifications->Notification as $notify_error) {
                            $error.='Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message . "<br>";
                        }
                        $response['error'] = $error;
                    } else {
                        $response['error'] = 'Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message;
                    }
                    $response['type'] = 'error';
                } else {
                    $response['type'] = 'success';
                    $amount = "<p class='amount'>" . $results->TotalAmount->Value . " " . $results->TotalAmount->CurrencyCode . "</p>";
                    $text = "Local taxes - if any - are not included. Rate is based on account number $account in " . $countryName;
                    $response['html'] = $amount . $text;
                }
            } catch (Exception $e) {
                $response['type'] = 'error';
                $response['error'] = $e->getMessage();
            }
        } catch (Exception $e) {
            $response['type'] = 'error';
            $response['error'] = $e->getMessage();
        }
        print json_encode($response);
        die();
    }

}
