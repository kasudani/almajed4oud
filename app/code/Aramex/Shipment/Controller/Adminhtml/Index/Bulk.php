<?php

namespace Aramex\Shipment\Controller\Adminhtml\Index;

use Magento\Framework\Controller\ResultFactory;

class Bulk extends \Magento\Backend\App\Action {

    const XML_PATH_TRANS_IDENTITY_EMAIL = 'trans_email/ident_general/email';
    const XML_PATH_TRANS_IDENTITY_NAME = 'trans_email/ident_general/name';
    const XML_PATH_SHIPMENT_EMAIL_TEMPLATE = 'aramex/template/shipment_template';
    const XML_PATH_SHIPMENT_EMAIL_COPY_TO = 'aramex/template/copy_to';
    const XML_PATH_SHIPMENT_EMAIL_COPY_METHOD = 'aramex/template/copy_method';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $request;
    protected $_scopeConfig;
    protected $shipmentLoader;
    protected $_transportBuilder;
    protected $_storeManager;
    protected $_request;
    protected $resultJsonFactory; 

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\App\Request\Http $request, \Magento\Shipping\Controller\Adminhtml\Order\ShipmentLoader $shipmentLoader, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
             \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->_scopeConfig = $scopeConfig;
        $this->shipmentLoader = $shipmentLoader;
        $this->_transportBuilder = $transportBuilder;
        $this->_storeManager = $storeManager;
        $this->_request = $context->getRequest();
        $this->resultJsonFactory = $resultJsonFactory;

        parent::__construct($context);
    }
    public function execute() {
        $post_out = $this->getRequest()->getPost();
        $post = array();
        $params = array();
        parse_str($post_out['str'], $params);
        $orders = array();
        $post['aramex_shipment_shipper_country'] = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
       
        //check "pending" status
        if (count($post_out["selectedOrders"])) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();      
            foreach ($post_out["selectedOrders"] as $key => $order_id) {
                $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId((int) $order_id);
                if ($order->getStatus() == "pending") {
                    $shipping = $order->getShippingAddress();
                    $shippingCountry = ($shipping) ? $shipping->getData('country_id') : '';
                    if ($shippingCountry == $post['aramex_shipment_shipper_country']) {
                        $orders[$key]['method'] = "DOM";
                    } else {
                        $orders[$key]['method'] = "EXP";
                    }
                    $orders[$key]['order_id'] = $order_id;
                }
            }

            //domestic metods must be first 
            $dom = array();
            $exp = array();
            foreach ($orders as $key => $order_item) {
                if ($order_item['method'] == 'DOM') {
                    $dom[$key]['method'] = "DOM";
                    $dom[$key]['order_id'] = $order_item['order_id'];
                } else {
                    $exp[$key]['method'] = "EXP";
                    $exp[$key]['order_id'] = $order_item['order_id'];
                }
            }
            $orders = array();
            $total = count($dom) + count($exp);
            for ($i = 0; $i < $total; $i++) {
                foreach ($dom as $key => $item) {
                    $orders[$key]['method'] = "DOM";
                    $orders[$key]['order_id'] = $item['order_id'];
                }
                foreach ($exp as $key => $item) {
                    $orders[$key]['method'] = "EXP";
                    $orders[$key]['order_id'] = $item['order_id'];
                }
            }
        }
        
        //domestic metods must be first 
        if (count($orders)) {
            $responce = "";
            foreach ($orders as $key => $orderItem) {
                $post['aramex_shipment_original_reference'] = (int) $orderItem['order_id'];
                $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId((int)$orderItem['order_id']);
                //$customerId = $order->getCustomerId();
                //$customer = Mage::getModel('customer/customer')->load($customerId);
                $isShipped = false;
                $itemsv = $order->getAllVisibleItems();
                $totalWeight = 0;
                foreach ($itemsv as $itemvv) {
                    if ($itemvv->getWeight() != 0) {
                        $weight = $itemvv->getWeight() * $itemvv->getQtyOrdered();
                    } else {
                        $weight = 0.5 * $itemvv->getQtyOrdered();
                    }
                    $totalWeight += $weight;
                    if ($itemvv->getQtyOrdered() == $itemvv->getQtyShipped()) {
                        $isShipped = true;
                    }
                    //quontity  
                    $_qty = abs($itemvv->getQtyOrdered() - $itemvv->getQtyShipped());
                    if ($_qty == 0 and $isShipped) {
                        $_qty = intval($itemvv->getQtyShipped());
                    }

                    $post[$itemvv->getId()] = (string) $_qty;
                }
             
                foreach ($itemsv as $item) {
                       if ($item->getQtyOrdered() > $item->getQtyShipped() or $isShipped) {
                        $_qty = abs($item->getQtyOrdered() - $item->getQtyShipped());
                        if ($_qty == 0 && $isShipped) {
                            $_qty = intval($item->getQtyShipped());
                        }
                        $post['aramex_items'][$item->getId()] = $_qty;
                    }
                }

                $post['order_weight'] = (string) $totalWeight;
                $post['aramex_shipment_shipper_reference'] = $order->getIncrementId();
                $post['aramex_shipment_info_billing_account'] = 1;
                $post['aramex_shipment_shipper_account'] = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_street'] = $this->_scopeConfig->getValue('aramex/shipperdetail/address', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_city'] = $this->_scopeConfig->getValue('aramex/shipperdetail/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_state'] = $this->_scopeConfig->getValue('aramex/shipperdetail/state', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_postal'] = $this->_scopeConfig->getValue('aramex/shipperdetail/postalcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_name'] = $this->_scopeConfig->getValue('aramex/shipperdetail/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_company'] = $this->_scopeConfig->getValue('aramex/shipperdetail/company', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_phone'] = $this->_scopeConfig->getValue('aramex/shipperdetail/phone', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                $post['aramex_shipment_shipper_email'] = $this->_scopeConfig->getValue('aramex/shipperdetail/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

                //shipper parameters
                $post['aramex_shipment_receiver_reference'] = $order->getIncrementId();
                $shipping = $order->getShippingAddress();
                $post['aramex_shipment_receiver_street'] = ($shipping) ? $shipping->getData('street') : '';
                $post['aramex_shipment_receiver_city'] = ($shipping) ? $shipping->getData('city') : '';
                $post['aramex_shipment_receiver_postal'] = ($shipping) ? $shipping->getData('postcode') : '';
                $post['aramex_shipment_receiver_country'] = ($shipping) ? $shipping->getData('country_id') : '';
                $post['aramex_shipment_receiver_name'] = ($shipping) ? $shipping->getName() : '';

                //Contact Info
                $post['aramex_shipment_receiver_name'] = ($shipping) ? $shipping->getName() : '';
                $company_name = isset($billing) ? $billing->getData('company') : '';
                $company_name = ($company_name) ? $company_name : '';
                $company_name = (empty($company_name) and $shipping) ? $shipping->getName() : $company_name;
                $company_name = ($shipping) ? $shipping->getData('company') : '';
                if (empty($company_name)) {
                    $company_name = $post['aramex_shipment_receiver_name'];
                }

                $post['aramex_shipment_receiver_company'] = ($company_name) ? $company_name : '';
                $post['aramex_shipment_receiver_phone'] = ($shipping) ? $shipping->getData('telephone') : '';
                $post['aramex_shipment_receiver_email'] =  $order->getData('customer_email');
                // Other Main Shipment Parameters
                $post['aramex_shipment_info_reference'] = $order->getIncrementId();
                $post['aramex_shipment_info_foreignhawb'] = '';
                $post['aramex_shipment_info_comment'] = '';
                $post['weight_unit'] = 'KG';

                if ($orderItem['method'] == 'DOM') {
                    $post['aramex_shipment_info_product_group'] = $orderItem['method'];
                    $post['aramex_shipment_info_product_type'] = ($params['aramex_shipment_info_product_type_dom']) ? $params['aramex_shipment_info_product_type_dom'] : "";
                    $post['aramex_shipment_info_payment_type'] = ($params['aramex_shipment_info_payment_type_dom']) ? $params['aramex_shipment_info_payment_type_dom'] : "";
                    $post['aramex_shipment_info_payment_option'] = "";
                    $post['aramex_shipment_info_service_type'] = ($params['aramex_shipment_info_service_type_dom']) ? $params['aramex_shipment_info_service_type_dom'] : "";
                    $post['aramex_shipment_currency_code'] = ($params['aramex_shipment_currency_code_dom']) ? $params['aramex_shipment_currency_code_dom'] : "";
                    $post['aramex_shipment_info_custom_amount'] =  "";
                } else {
                    $post['aramex_shipment_info_product_group'] = $orderItem['method'];
                    $post['aramex_shipment_info_product_type'] = ($params['aramex_shipment_info_product_type']) ? $params['aramex_shipment_info_product_type'] : "";
                    $post['aramex_shipment_info_payment_type'] = ($params['aramex_shipment_info_payment_type']) ? $params['aramex_shipment_info_payment_type'] : "";
                    $post['aramex_shipment_info_payment_option'] = ($params['aramex_shipment_info_payment_option']) ? $params['aramex_shipment_info_payment_option'] : "";
                    $post['aramex_shipment_info_service_type'] = ($params['aramex_shipment_info_service_type']) ? $params['aramex_shipment_info_service_type'] : "";
                    $post['aramex_shipment_currency_code'] = ($params['aramex_shipment_currency_code']) ? $params['aramex_shipment_currency_code'] : "";
                    $post['aramex_shipment_info_custom_amount'] = ($params['aramex_shipment_info_custom_amount']) ? $params['aramex_shipment_info_custom_amount'] : "";
                }

                $itemsnamecounter = 1;
                $item_supplier_id = '';
                $aramex_shipment_description = '';
                foreach ($order->getAllVisibleItems() as $itemname) {
                    if ($itemname->getQtyOrdered() > $itemname->getQtyShipped()) {
                        $aramex_shipment_description = $aramex_shipment_description . $itemname->getId() . ' - ' . trim($itemname->getName());
                    }
                    $itemsnamecounter++;
                    $supplier_different = false;
                    $atleast_item_shipped = false;
                    if (!$item_supplier_id) {
                        $item_supplier_id = $itemname->getUdropshipVendor();
                    } else {
                        if ($item_supplier_id != $itemname->getUdropshipVendor()) {
                            if (!$supplier_different) {
                                $supplier_different = true;
                            }
                        }
                    }
                    if ($itemname->getQtyOrdered() == $itemname->getQtyShipped()) {
                        if (!$atleast_item_shipped) {
                            $atleast_item_shipped = true;
                        }
                    }
                }

                $post['aramex_shipment_description'] = $aramex_shipment_description;
                $post['aramex_shipment_info_cod_amount'] = ($order->getPayment()->getMethodInstance()->getCode() != 'ccsave') ? (string) round($order->getData('grand_total'), 2) : '';
                $post['aramex_return_shipment_creation_date'] = "create";
                $post['aramex_shipment_referer'] = 0;

                $replay = $this->postAction($mass = TRUE, $post, $orderItem['method']);
                if ($replay[1] == "DOM") {
                    $method = "Domestic Product Group";
                } else {
                    $method = "International Product Group";
                };
                
                if ($replay[2] == "error") {
                    $responce .= "<p class='aramex_red'>" . $replay[0] . " - " . $orderItem['order_id'] . ' not created. (' . $method . ')</p>';
                    break;
                } else {
                    $responce .= "<p class='aramex_green'> Aramex Shipment Number: " . $orderItem['order_id'] . ' has been created.(' . $method . ')</p>';
                }
            }
            return  $this->resultJsonFactory->create()->setData(['Test-Message' => $responce]);
        } else {
            $errors = "<p class='aramex_red'>No orders with 'Pending' status selected.</p>";
            return  $this->resultJsonFactory->create()->setData(['Test-Message' => $errors]);
        }
    }

    private function postAction($mass = false, $post = Array(), $method) {
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $helperCore = $objectManager->create('\Aramex\Core\Helper\Data');
        $baseUrl = $helper->getWsdlPath();
        //SOAP object
        $soapClient = new \Zend\Soap\Client($baseUrl . 'shipping.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);
        
        $aramex_errors = false;
        $errors = array();
        try {

            /* here's your form processing */
            $order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($post['aramex_shipment_original_reference']);
            $payment = $order->getPayment();
            $totalWeight = 0;
            $totalItems = 0;
            $items = $order->getAllItems();
            $descriptionOfGoods = '';
            foreach ($order->getAllVisibleItems() as $itemname) {
                $descriptionOfGoods .= $itemname->getId() . ' - ' . trim($itemname->getName());
            }
            $aramex_items_counter = 0;
            
            foreach ($post['aramex_items'] as $key => $value) {
                $aramex_items_counter++;
                if ($value != 0) {
                    //itrating order items
                    foreach ($items as $item) {
                        if ($item->getId() == $key) {
                            //get weight
                            if ($item->getWeight() != 0) {
                                $weight = $item->getWeight() * $item->getQtyOrdered();
                            } else {
                                $weight = 0.5 * $item->getQtyOrdered();
                            }

                            // collect items for aramex
                            $aramex_items[] = array(
                                'PackageType' => 'Box',
                                'Quantity' => $post[$item->getId()],
                                'Weight' => array(
                                    'Value' => $weight,
                                    'Unit' => 'Kg'
                                ),
                                'Comments' => $item->getName(), //'',
                                'Reference' => ''
                            );

                            $totalWeight += $weight;
                            $totalItems += $post[$item->getId()];
                        }
                    }
                }
            }

            $totalWeight = $post['order_weight'];
            $params = array();
           //shipper parameters
            $params['Shipper'] = array(
                'Reference1' => $post['aramex_shipment_shipper_reference'], //'ref11111',
                'Reference2' => '',
                'AccountNumber' => ($post['aramex_shipment_info_billing_account'] == 1) ? $post['aramex_shipment_shipper_account'] : $post['aramex_shipment_shipper_account'], //'43871',
                //Party Address
                'PartyAddress' => array(
                    'Line1' => addslashes($post['aramex_shipment_shipper_street']), //'13 Mecca St',
                    'Line2' => '',
                    'Line3' => '',
                    'City' => $post['aramex_shipment_shipper_city'], //'Dubai',
                    'StateOrProvinceCode' => $post['aramex_shipment_shipper_state'], //'',
                    'PostCode' => $post['aramex_shipment_shipper_postal'],
                    'CountryCode' => $post['aramex_shipment_shipper_country'], //'AE'
                ),
                //Contact Info
                'Contact' => array(
                    'Department' => '',
                    'PersonName' => $post['aramex_shipment_shipper_name'], //'Suheir',
                    'Title' => '',
                    'CompanyName' => $post['aramex_shipment_shipper_company'], //'Aramex',
                    'PhoneNumber1' => $post['aramex_shipment_shipper_phone'], //'55555555',
                    'PhoneNumber1Ext' => '',
                    'PhoneNumber2' => '',
                    'PhoneNumber2Ext' => '',
                    'FaxNumber' => '',
                    'CellPhone' => $post['aramex_shipment_shipper_phone'],
                    'EmailAddress' => $post['aramex_shipment_shipper_email'], //'',
                    'Type' => ''
                ),
            );
            //consinee parameters
            $params['Consignee'] = array(
                'Reference1' => $post['aramex_shipment_receiver_reference'], //'',
                'Reference2' => '',
                'AccountNumber' => ($post['aramex_shipment_info_billing_account'] == 2) ? $post['aramex_shipment_shipper_account'] : '',
                //Party Address
                'PartyAddress' => array(
                    'Line1' => $post['aramex_shipment_receiver_street'], //'15 ABC St',
                    'Line2' => '',
                    'Line3' => '',
                    'City' => $post['aramex_shipment_receiver_city'], //'Amman',
                    'StateOrProvinceCode' => '',
                    'PostCode' => $post['aramex_shipment_receiver_postal'],
                    'CountryCode' => $post['aramex_shipment_receiver_country'], //'JO'
                ),
                //Contact Info
                'Contact' => array(
                    'Department' => '',
                    'PersonName' => $post['aramex_shipment_receiver_name'], //'Mazen',
                    'Title' => '',
                    'CompanyName' => $post['aramex_shipment_receiver_company'], //'Aramex',
                    'PhoneNumber1' => $post['aramex_shipment_receiver_phone'], //'6666666',
                    'PhoneNumber1Ext' => '',
                    'PhoneNumber2' => '',
                    'PhoneNumber2Ext' => '',
                    'FaxNumber' => '',
                    'CellPhone' => $post['aramex_shipment_receiver_phone'],
                    'EmailAddress' => $post['aramex_shipment_receiver_email'], //'mazen@aramex.com',
                    'Type' => ''
                )
            );

            // Other Main Shipment Parameters
            $params['Reference1'] = $post['aramex_shipment_info_reference']; //'Shpt0001';
            $params['Reference2'] = '';
            $params['Reference3'] = '';
            $params['ForeignHAWB'] = $post['aramex_shipment_info_foreignhawb'];

            $params['TransportType'] = 0;
            $params['ShippingDateTime'] = time(); //date('m/d/Y g:i:sA');
            $params['DueDate'] = time() + (7 * 24 * 60 * 60); //date('m/d/Y g:i:sA');
            $params['PickupLocation'] = 'Reception';
            $params['PickupGUID'] = '';
            $params['Comments'] = $post['aramex_shipment_info_comment'];
            $params['AccountingInstrcutions'] = '';
            $params['OperationsInstructions'] = '';
            $params['Details'] = array(
                'Dimensions' => array(
                    'Length' => '0',
                    'Width' => '0',
                    'Height' => '0',
                    'Unit' => 'cm'
                ),
                'ActualWeight' => array(
                    'Value' => $totalWeight,
                    'Unit' => $post['weight_unit']
                ),
                'ProductGroup' => $post['aramex_shipment_info_product_group'], //'EXP',
                'ProductType' => $post['aramex_shipment_info_product_type'], //,'PDX'
                'PaymentType' => $post['aramex_shipment_info_payment_type'],
                'PaymentOptions' => $post['aramex_shipment_info_payment_option'], //$post['aramex_shipment_info_payment_option']
                'Services' => $post['aramex_shipment_info_service_type'],
                'NumberOfPieces' => $totalItems,
                'DescriptionOfGoods' => (trim($post['aramex_shipment_description']) == '') ? $descriptionOfGoods : $post['aramex_shipment_description'],
                'GoodsOriginCountry' => $post['aramex_shipment_shipper_country'], //'JO',
                'Items' => $aramex_items,
            );

            $params['Details']['CashOnDeliveryAmount'] = array(
                'Value' => $post['aramex_shipment_info_cod_amount'],
                'CurrencyCode' => $post['aramex_shipment_currency_code']
            );

            $params['Details']['CustomsValueAmount'] = array(
                'Value' => $post['aramex_shipment_info_custom_amount'],
                'CurrencyCode' => $post['aramex_shipment_currency_code']
            );

            $major_par['Shipments'][] = $params;
            $clientInfo = $helper->getClientInfo();
            $major_par['ClientInfo'] = $clientInfo;
            $report_id = (int) $this->_scopeConfig->getValue('aramex/config/report_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            if (!$report_id) {
                $report_id = 9729;
            }
            $major_par['LabelInfo'] = array(
                'ReportID' => $report_id, //'9201',
                'ReportType' => 'URL'
            );


            try {
                //create shipment call
                $auth_call = $soapClient->CreateShipments($major_par);
                if ($auth_call->HasErrors) {
                    if (empty($auth_call->Shipments)) {
                        if (count($auth_call->Notifications->Notification) > 1) {
                            foreach ($auth_call->Notifications->Notification as $notify_error) {
                                    $errors = 'Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message;
                            }
                        } else {
                                $errors = 'Aramex: ' . $auth_call->Notifications->Notification->Code . ' - ' . $auth_call->Notifications->Notification->Message;
                        }
                    } else {
                        if (count($auth_call->Shipments->ProcessedShipment->Notifications->Notification) > 1) {
                            $notification_string = '';
                            foreach ($auth_call->Shipments->ProcessedShipment->Notifications->Notification as $notification_error) {
                                $notification_string .= $notification_error->Code . ' - ' . $notification_error->Message . ' <br />';
                            }
                                $errors = $notification_string;
                           
                        } else {
                                $errors = 'Aramex: ' . $auth_call->Shipments->ProcessedShipment->Notifications->Notification->Code . ' - ' . $auth_call->Shipments->ProcessedShipment->Notifications->Notification->Message;
                        }
                    }
                        return (array($errors, $method, 'error'));
                } else {
 
                    $data = [
                        'items' => $post['aramex_items'],
                        'comment_text' => "AWB No. " . $auth_call->Shipments->ProcessedShipment->ID . " - Order No. " . $auth_call->Shipments->ProcessedShipment->Reference1,
                        'comment_customer_notify' => true,
                        'is_visible_on_front' => true
                    ];                   
                    
                    if ($order->canShip() && $post['aramex_return_shipment_creation_date'] == "create") {
                        $this->shipmentLoader->setOrderId($post['aramex_shipment_original_reference']); //4
                        $this->shipmentLoader->setShipmentId(null); //null
                        $this->shipmentLoader->setShipment($data); //array (size=2) 'items' =>  array (size=1)  4 => string '1' (length=1)  'comment_text' => string '' (length=0)
                        $this->shipmentLoader->setTracking(null); // null
                        $shipment = $this->shipmentLoader->load();
                        if (!$shipment) {
                            $this->_forward('noroute');
                            return;
                        }

                        if (!empty($data['comment_text'])) {
                            $shipment->addComment(
                                    $data['comment_text'], isset($data['comment_customer_notify']), isset($data['is_visible_on_front'])
                            );

                            $shipment->setCustomerNote($data['comment_text']);
                            $shipment->setCustomerNoteNotify(isset($data['comment_customer_notify']));
                        }
///////////// block shipment
                       $shipment->register();
                       $this->_saveShipment($shipment);

                        $ship = true;
                        
                        /* sending mail */
                        if ($ship) {
                            
                                /* send shipment mail */
                                $storeId = $order->getStore()->getId();
                                $copyTo = $helperCore->getEmails(self:: XML_PATH_SHIPMENT_EMAIL_COPY_TO, $storeId);
                                $copyMethod = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_COPY_METHOD, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
                                $templateId = $this->_scopeConfig->getValue(self::XML_PATH_SHIPMENT_EMAIL_TEMPLATE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);

                                if ($order->getCustomerIsGuest()) {
                                    $customerName = $order->getBillingAddress()->getName();
                                } else {
                                    $customerName = $order->getCustomerName();
                                }

                                $shipments_id = $auth_call->Shipments->ProcessedShipment->ID;
                                $templateParams = array(
                                    'order' => $order,
                                    'customerName' => $customerName,
                                    'shipments_id' => $shipments_id
                                );
                                $senderName = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_NAME);
                                $senderEmail = $this->_scopeConfig->getValue(self::XML_PATH_TRANS_IDENTITY_EMAIL);

                                if ($copyTo && $copyMethod == 'bcc') {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(), $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
                                }
                                if ($copyTo && $copyMethod == 'copy') {
                                    $transport = $this->_transportBuilder->setTemplateIdentifier($templateId)
                                            ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $storeId])
                                            ->setTemplateVars($templateParams)
                                            ->setFrom(array('name' => $senderName, 'email' => $senderEmail))
                                            ->addTo($order->getCustomerEmail(), $customerName)
                                            ->addBcc($copyTo)
                                            ->getTransport();
                                }
                            
                            try {
                                $transport->sendMessage();
                            }  catch (Exception $ex) {
                                    $errors = "'Unable to send email.'";
                                    return (array($errors, $method, 'error'));
                            }

                        }
                            return (array(true, $method, 'succes'));

                    }else {
                        $this->messageManager->addError('Cannot do shipment for the order.');
                    }
                }
            } catch (Exception $e) {
                    $errors = $e->getMessage();
                    return array($errors, $method, 'error');
                    $aramex_errors = false;
            }
            if ($aramex_errors) {
                $strip = strstr($post['aramex_shipment_referer'], "aramexpopup", true);
                $url = $strip;
                if (empty($strip)) {
                    $url = $post['aramex_shipment_referer'];
                }
                $resultRedirect->setUrl($url . 'aramexpopup/show');
            } 
        } catch (Exception $e) {
                $errors = $e->getMessage();
                return array($errors, $method, 'error');
        }
    }
    
    protected function _saveShipment($shipment) {
        $shipment->getOrder()->setIsInProcess(true);
        $transaction = $this->_objectManager->create(
                'Magento\Framework\DB\Transaction'
        );
        $transaction->addObject(
                $shipment
        )->addObject(
                $shipment->getOrder()
        )->save();

        return $this;
    }
}

?>