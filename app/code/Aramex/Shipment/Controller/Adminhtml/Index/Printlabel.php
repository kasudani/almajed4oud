<?php

namespace Aramex\Shipment\Controller\Adminhtml\Index;
use Magento\Framework\Controller\ResultFactory;
class Printlabel extends \Magento\Backend\App\Action {

    protected $_scopeConfig;
    protected $_request;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory
     */
    public function __construct(
    \Magento\Backend\App\Action\Context $context, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_request = $context->getRequest();
        $this->_scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute() {

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $account = $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $country_code = $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $_order = $objectManager->create('Magento\Sales\Model\Order')->load($this->_request->getParam('order_id'));
        $formSession = $objectManager->create('\Magento\Backend\Model\Session');
        $previuosUrl=$formSession->getPreviousUrl();
        

        if($this->_request->getParam('order_id')){
            $baseUrl = $helper->getWsdlPath();
            //SOAP object
            $soapClient = new \Zend\Soap\Client($baseUrl . 'shipping.wsdl');
            $soapClient->setSoapVersion(SOAP_1_1);
            $clientInfo = $helper->getClientInfo();
            $awbno = array();

            if(count($_order->getAllStatusHistory())) {
                foreach ($_order->getAllStatusHistory() as $orderComment) {
                    if($orderComment->getComment() && preg_match('/Aramex Shipment Return Order AWB No. ([0-9]+)/',$orderComment->getComment(),$cmatches)){
                        $awbno[1]['comment'] = $cmatches[1];
                        $awbno[1]['created'] = $orderComment->getCreatedAt();
                        break;
                    }else{
                         $awbno[1]['created'] = 0;
                    }
                }
            }else{
                $awbno[1]['created'] = 0;
            }

            if(count($_order->getShipmentsCollection()->count())){

                    foreach ($_order->getShipmentsCollection() as $_shipment) {
                        if ($_shipment->getCommentsCollection()->count()) {
                            foreach ($_shipment->getCommentsCollection() as $_comment) {
                                $awbno[2]['comment'] =trim(strstr($_comment->getComment(),"- Order No",true),"AWB No.");
                                $awbno[2]['created'] = $_comment->getCreatedAt();
                                break;
                            }
                        }
                    }
                if(strtotime($awbno[1]['created']) > strtotime($awbno[2]['created']) ){
                    $shipmentNumber = $awbno[1]['comment'];
                }else{
                    $shipmentNumber = $awbno[2]['comment'];
                }
                $report_id = $this->_scopeConfig->getValue('aramex/config/report_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
                if(!$report_id){
                    $report_id =9729;
                }
                
                $shipmentNumber = $this->_request->getParam('print_label_id');

                $params = array(
                    'ClientInfo'  			=> $clientInfo,
                    'Transaction' 			=> array(
                        'Reference1'			=> $_order->getIncrementId(),
                        'Reference2'			=> '',
                        'Reference3'			=> '',
                        'Reference4'			=> '',
                        'Reference5'			=> '',
                    ),
                    'LabelInfo'				=> array(
                        'ReportID' 				=> $report_id,
                        'ReportType'			=> 'URL',
                    ),
                );
                $params['ShipmentNumber']=$shipmentNumber;
                try {
                    $auth_call = $soapClient->PrintLabel($params);
                    /* bof  PDF demaged Fixes debug */
                    if($auth_call->HasErrors){
                        if(count($auth_call->Notifications->Notification) > 1){
                            foreach($auth_call->Notifications->Notification as $notify_error){
                                $error = "";
                                $error.='Aramex: ' . $notify_error->Code .' - '. $notify_error->Message;
                            }
                            $this->messageManager->addError($error);
                            return $resultRedirect;
                        } else {
                            $this->messageManager->addError('Aramex: ' . $auth_call->Notifications->Notification->Code . ' - '. $auth_call->Notifications->Notification->Message);
                            $resultRedirect->setUrl($previuosUrl);
                            return $resultRedirect;
                        }
                    }

                    /* eof  PDF demaged Fixes */
                    $filepath=$auth_call->ShipmentLabel->LabelURL;
                    $name="{$_order->getIncrementId()}-shipment-label.pdf";
                    ///header('Content-type: application/pdf');
                    ///header('Content-Disposition: attachment; filename="'.$name.'"');
                    //readfile($filepath);
                    header( "HTTP/1.1 301 Moved Permanently" ); 
                    header('Location: ' . $filepath);

                    exit();
                } catch (SoapFault $fault) {
                    $this->messageManager->addError('Error : ' . $fault->faultstring);
                    $resultRedirect->setUrl($previuosUrl);
                    return $resultRedirect;
                }
                catch (Exception $e) {
                    $aramex_errors = true;
                    $this->messageManager->addError($e->getMessage());
                    $resultRedirect->setUrl($previuosUrl);
                    return $resultRedirect;;
                }
            }else{
                $this->messageManager->addError('Shipment is empty or not created yet.');
                $resultRedirect->setUrl($previuosUrl);
                return $resultRedirect;
            }
        }else{
            $this->messageManager->addError('This order no longer exists.');
            $resultRedirect->setUrl($previuosUrl);
            return $resultRedirect;
        }
    }
}
