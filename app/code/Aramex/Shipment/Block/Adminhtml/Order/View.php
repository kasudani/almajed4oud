<?php

/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Aramex\Shipment\Block\Adminhtml\Order;

/**
 * Product View block
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class View extends \Magento\Sales\Block\Adminhtml\Order\View
{

    public $order_id;

    protected function _construct()
    {

        $itemscount = 0;
        $totalWeight = 0;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $_order = $objectManager->create('Magento\Sales\Model\Order')->load($this->getOrder()->getId());
        $this->order_id = $this->getOrder()->getId();
        $itemsv = $_order->getAllVisibleItems();
        foreach ($itemsv as $itemvv) {

            if ((int)$itemvv->getQtyOrdered() > (int)$itemvv->getQtyShipped()) {
                $itemscount += (int)$itemvv->getQtyOrdered() - (int)$itemvv->getQtyShipped();
            }
            if ($itemvv->getWeight() != 0) {
                $weight = $itemvv->getWeight() * $itemvv->getQtyOrdered();
            } else {
                $weight = 0.5 * $itemvv->getQtyOrdered();
            }
            $totalWeight += $weight;
        }

        $history = array();
        if ($_order->getShipmentsCollection()->count()) {
            foreach ($_order->getShipmentsCollection() as $_shipment) {
                if ($_shipment->getCommentsCollection()->count()) {
                    foreach ($_shipment->getCommentsCollection() as $_comment) {
                        $history[] = $_comment->getComment();
                    }
                }
            }
        }

        $aramex_return_button = false;
        if (count($history)) {
            foreach ($history as $_history) {
                $awbno = strstr($_history, "- Order No", true);
                $awbno = trim($awbno, "AWB No.");
                break;
            }
            if (isset($awbno)) {
                if ((int)$awbno)
                    $aramex_return_button = true;
            }
        }
        //barry code
        /*if ($itemscount == 0) {
            $this->buttonList->add('print_aramex_label', array(
                'label' => __('Aramex Print Label'),
                'onclick' => "myObj.printLabel()",
                'class' => 'go'
            ));
        }*/
        //barry code end here
        if ($_order->canShip()) {
            $this->buttonList->add('create_aramex_shipment', array(
                'label' => __('Prepare Aramex Shipment'),
               //'onclick' => 'aramexpop(' . $itemscount . ')',
                'class' => "itemscount_".$itemscount
            ));
        } elseif (!$_order->canShip() && $aramex_return_button) {
            $this->buttonList->add('create_aramex_shipment', array(
                'label' => __('Return Aramex Shipment'),
                //'onclick' => 'aramexreturnpop(' . $itemscount . ')',
                'class' => "itemscount_".$itemscount
            ));
        }



        parent::_construct();
    }


}
