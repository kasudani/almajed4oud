<?php

namespace Aramex\ApiLocationValidator\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeData implements UpgradeDataInterface {

    private $configFactory;

    public function __construct(
    \Magento\Framework\App\State $state, 
    \Magento\Config\Model\Config\Factory $configFactory, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $state->setAreaCode('adminhtml');
        $this->configFactory = $configFactory;
    }

    public function upgrade(
    ModuleDataSetupInterface $setup, ModuleContextInterface $context
    ) {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.1') < 0) {

            // Get aramex_location_country table
            $tableName = $setup->getTable('aramex_location_country');
            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
                $clientInfo = $helper->getStaticClientInfo();
                $params = array(
                    'ClientInfo' => $clientInfo,
                    'Transaction' => array(
                        'Reference1' => '001',
                        'Reference2' => '002',
                        'Reference3' => '003',
                        'Reference4' => '004',
                        'Reference5' => '005'
                    ),
                );

                $baseUrl = $helper->getWsdlPath();
                $soapClient = new \Zend\Soap\Client($baseUrl . 'Location-API-WSDL.wsdl');
                $soapClient->setSoapVersion(SOAP_1_1);
                try {
                    $results = $soapClient->FetchCountries($params);
                    if (is_object($results)) {
                        if (!$results->HasErrors) {
                            /* remove all old entries */
                            $setup->getConnection()->truncateTable($tableName);
                            $countries = $results->Countries->Country;
                            $data = array();
                            foreach ($countries as $key => $country) {
                                if($country->IsoCode != null && $country->InternationalCallingNumber != null ){
                                $data[$key]['name'] = $country->Name;
                                $data[$key]['code'] = $country->Code;
                                $data[$key]['iso_code'] = $country->IsoCode;
                                $data[$key]['state_required'] = $country->StateRequired;
                                $data[$key]['post_code_required'] = $country->PostCodeRequired;
                                // $data[$key]['post_code_regex'] = $country->PostCodeRegex;
                                $data[$key]['international_calling_number'] = $country->InternationalCallingNumber;
                                }
                            }

                            // Insert data to table
                            foreach ($data as $item) {
                                $setup->getConnection()->insert($tableName, $item);
                            }
                            $this->_resetStoreGeneralOptions($data);
                        }
                    }
                } catch (SoapFault $fault) {
                    die($fault);
                }
            }
        }

        $setup->endSetup();
    }

    private function _resetStoreGeneralOptions($data) {
        $requiredStates = array();
        $postalCodeOptional = array();
        foreach ($data as $col) {
            if ($col['state_required'] == 1) {
                if ($col['code']) {
                    $requiredStates[] = $col['code'];
                }
            }
            if ($col['state_required'] == 0) {
                if ($col['code']) {
                    $postalCodeOptional[] = $col['code'];
                }
            }
        }
        $requiredStates = array_unique($requiredStates);
        $postalCodeOptional = array_unique($postalCodeOptional);
        if (count($requiredStates) > 0 && false) {
            $requiredStatesString = implode(',', $requiredStates);
            $configModel = $this->configFactory->create(['general/region/state_required' => $requiredStatesString]);
            $configModel->save();
        }
        if (count($postalCodeOptional) > 0) {
            $postalCodeOptionalString = implode(',', $postalCodeOptional);

            $configData = [
                'section' => 'general',
                'website' => null,
                'store' => null,
                'groups' => [
                    'country' => [
                        'fields' => [
                            'optional_zip_countries' => [
                                'value' => $postalCodeOptionalString,
                            ],
                        ],
                    ],
                ],
            ];

            $configModel = $this->configFactory->create(['data' => $configData]);
            $configModel->save();
        }
    }

}
