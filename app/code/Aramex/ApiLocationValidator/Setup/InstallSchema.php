<?php

namespace Aramex\ApiLocationValidator\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface {

    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();
        
        // Get tutorial_simplenews table
        $tableName = $installer->getTable('aramex_location_country');
        
        // Check if the table already exists
        if ($installer->getConnection()->isTableExists($tableName) != true) {
        //START table setup
        $table = $installer->getConnection()->newTable(
                        $installer->getTable($tableName)
                )->addColumn(
                        'location_id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, array(
                    'identity' => true, 'nullable' => false, 'primary' => true, 'unsigned' => true,
                        ), 'Location ID'
                )->addColumn(
                        'name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, array(
                    'nullable' => false,
                        ), 'Name'
                )->addColumn(
                        'code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 10, array(
                    'nullable' => false,
                        ), 'Code'
                )->addColumn(
                        'iso_code', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 20, array(
                    'nullable' => false,
                        ), 'Iso Code'
                )->addColumn(
                        'state_required', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 4, array(
                    'nullable' => false,
                        ), 'State Required'
                )->addColumn(
                        'post_code_required', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 4, array(
                    'nullable' => false,
                        ), 'Post Code Required'
                )
                ->addColumn(
                        'post_code_regex', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 50, array(
                    'nullable' => false,
                        ), 'Post Code Regex'
                )->addColumn(
                        'international_calling_number', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, 11, array(
                    'nullable' => false,
                        ), 'International Calling Number'
                )->addColumn(
                        'modified_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                        ), 'Modified At'
                )->addColumn(
                        'create_at', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, array(
                        'nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
                        ), 'Created At'
                )->setOption('type', 'InnoDB')->setOption('charset', 'utf8');
                $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }

}
