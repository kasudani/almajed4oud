<?php
 
namespace Aramex\ApiLocationValidator\Model\Resource;
 
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
 
class Country extends AbstractDb
{
    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('aramex_location_country', 'location_id');
    }
}
