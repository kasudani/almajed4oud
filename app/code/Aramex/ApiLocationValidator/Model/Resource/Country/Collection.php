<?php
 
namespace Aramex\ApiLocationValidator\Model\Resource\Country;
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Aramex\ApiLocationValidator\Model\Country',
            'Aramex\ApiLocationValidator\Model\Resource\Country'
        );
    }
}