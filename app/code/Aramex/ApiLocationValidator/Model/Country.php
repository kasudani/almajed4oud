<?php
 
namespace Aramex\ApiLocationValidator\Model;
 
use Magento\Framework\Model\AbstractModel;
 
class Country extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Aramex\ApiLocationValidator\Model\Resource\Country');
    }
}