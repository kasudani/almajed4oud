<?php

namespace Aramex\ApiLocationValidator\Model;

use Magento\Framework\Model\AbstractModel;

class Api extends AbstractModel {

    /**
     * Define resource model
     */
    protected function _construct() {
        $this->_init('Aramex\ApiLocationValidator\Model\Resource\Country');
    }

    public function fetchCities($CountryCode, $NameStartsWith = NULL) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $clientInfo = $helper->getClientInfo();

        $params = array(
            'ClientInfo' => $clientInfo,
            'Transaction' => array(
                'Reference1' => '001',
                'Reference2' => '002',
                'Reference3' => '003',
                'Reference4' => '004',
                'Reference5' => '005'
            ),
            'CountryCode' => $CountryCode,
            'State' => NULL,
            'NameStartsWith' => $NameStartsWith,
        );

        $baseUrl = $helper->getWsdlPath();
        //SOAP object
        $soapClient = new \Zend\Soap\Client($baseUrl . 'Location-API-WSDL.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);

        try {
            $results = $soapClient->FetchCities($params);
            if (is_object($results)) {
                if (!$results->HasErrors) {
                    $cities = isset($results->Cities->string) ? $results->Cities->string : false;
                    return $cities;
                }
            }
        } catch (SoapFault $fault) {
            die('Error : ' . $fault->faultstring);
        }
    }

    public function validateAddress($address) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('\Aramex\Shipment\Helper\Data');
        $clientInfo = $helper->getClientInfo();

        $params = array(
            'ClientInfo' => $clientInfo,
            'Transaction' => array(
                'Reference1' => '001',
                'Reference2' => '002',
                'Reference3' => '003',
                'Reference4' => '004',
                'Reference5' => '005'
            ),
            'Address' => array(
                'Line1' => '001',
                'Line2' => '',
                'Line3' => '',
                'City' => $address['city'],
                'StateOrProvinceCode' => '',
                'PostCode' => $address['post_code'],
                'CountryCode' => $address['country_code']
            )
        );

        $baseUrl = $helper->getWsdlPath();
        //SOAP object
        $soapClient = new \Zend\Soap\Client($baseUrl . 'Location-API-WSDL.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);

        $reponse = array();
        try {
            $results = $soapClient->ValidateAddress($params);
            if (is_object($results)) {
                if ($results->HasErrors) {
                    $suggestedAddresses = (isset($results->SuggestedAddresses->Address)) ? $results->SuggestedAddresses->Address : "";
                    $message = (isset($results->Notifications->Notification->Message))? $results->Notifications->Notification->Message : "";
                    $reponse = array('is_valid' => false, 'suggestedAddresses' => $suggestedAddresses, 'message' => $message);
                } else {
                    $reponse = array('is_valid' => true);
                }
            }
        } catch (SoapFault $fault) {
            die('Error : ' . $fault->faultstring);
        }
        return $reponse;
    }

}
