<?php

namespace Aramex\ApiLocationValidator\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Aramex\ApiLocationValidator\Model\CountryFactory;

class Applyvalidation extends Action
{
    protected $_request;
    protected $_modelCountryFactory;
    /**
     * @param Context $context
     * @param CountryFactory $modelCountryFactory
     */
    public function __construct(
        Context $context, CountryFactory $modelCountryFactory
    )
    {
        parent::__construct($context);
        $this->_request = $context->getRequest();
        $this->_modelCountryFactory = $modelCountryFactory;
    }

    public function execute()
    {
        $post = $this->_request->getPost();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if ($post) {
            $address = array();
            $address['city'] = $this->_request->getParam('city');
            $address['post_code'] = $this->_request->getParam('post_code');
            $address['country_code'] = $this->_request->getParam('country_code');
        }

        $result = $objectManager->create('\Aramex\ApiLocationValidator\Model\Api')->validateAddress($address);
        if (count($result) > 0 && $result != false) {
            echo json_encode($result);
            die();

        } else {
            echo json_encode(array());
        }
    }

}
