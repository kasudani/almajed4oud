<?php

namespace Aramex\ApiLocationValidator\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Aramex\ApiLocationValidator\Model\CountryFactory;

class Searchautocities extends Action {

    protected $_request;

    /**
     * @var \Tutorial\SimpleNews\Model\NewsFactory
     */
    protected $_modelCountryFactory;

    /**
     * @param Context $context
     * @param CountryFactory $modelCountryFactory
     */
    public function __construct(
    Context $context, CountryFactory $modelCountryFactory
    ) {
        parent::__construct($context);
        $this->_request = $context->getRequest();
        $this->_modelCountryFactory = $modelCountryFactory;
    }

    public function execute() {

        $post = $this->_request->getPost();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        if ($post) {
            $countryCode = $this->_request->getParam('country_code');
            $term= $this->_request->getParam('term');
        }

        $cities = $objectManager->create('\Aramex\ApiLocationValidator\Model\Api')->fetchCities($countryCode, $term);
        if (count($cities) > 0 && $cities != false) {
            $cities = array_unique($cities);
            $sortCities = array();
            foreach ($cities as $v) {
                $sortCities[] = ucwords(strtolower($v));
            }
            asort($sortCities, SORT_STRING);

            $to_return = [];
            foreach ($sortCities as $val){
                $to_return[] =  $val ;
            }

             echo json_encode ($to_return);
             die();

        } else {
            
            echo json_encode (array());
        }
    }

}
