define(
    [
        'Aramex_ApiLocationValidator/js/view'
    ],
    function (Component) {
        'use strict';
 
        return Component.extend({
            /**
             * @override
             * use to define amount is display setting
             */
            isDisplayed: function () {
                
                return true;
            }
        });
    }
);