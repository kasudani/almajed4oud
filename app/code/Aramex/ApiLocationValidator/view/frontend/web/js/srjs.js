define([
    'jquery',
    "underscore",
    'Magento_Ui/js/form/form',
    'ko'], function (
    $,
    _,
    Component,
    ko) {
    'use strict';

    return Component.extend({
        defaults: {
          //  template: 'Vendor_Module/checkout/shipping/template'
        },

        initialize: function () {
            var self = this;
            this._super();
            this.setCheckbox();
        },
        setCheckbox: function() {
            var viewModel= {
                selectedAction: ko.observable(false),
                clickedAction: function(){
                    window.alert('checkbox checked!!');
                    return true;
                }
            };

            ko.applyBindings(viewModel);
        }

    });
});