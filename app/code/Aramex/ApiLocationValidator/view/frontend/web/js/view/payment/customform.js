define(
    [
        'jquery',
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/cart/totals-processor/default',
        'Magento_Catalog/js/price-utils',
    ],
    function (
        $, 
        ko, 
        Component, 
        quote, 
        totals, 
        defaultTotal, 
        priceUtils
        ) {
        'use strict';
        return Component.extend({
            
            defaults: {
                template: 'Aramex_ApiLocationValidator/checkout/creditamount' //template file location
            },
            /**
             * apply action
             */
            apply: function(value) {
                alert("apply");
                if (this.validate()) {
                    /* Apply Button action */
                }
            },
            /**
             * Cancel action
             */
            cancel: function() {
                 alert("cancel");
                 
                 /* Cancel Button action */
            },
            /**
             * form validation
             *
             * @returns {boolean}
             */
            validate: function() {
                var form = '#my-form';
                return $(form).validation() && $(form).validation('isValid');
            },
        });
       
    }
);