<?php
namespace Aramex\Shipping\Model;

use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
use  Aramex\Shipment\Helper\Data;

use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Simplexml\Element;
use Magento\Ups\Helper\Config;
use Magento\Framework\Xml\Security;



class Aramex extends AbstractCarrierOnline implements CarrierInterface
{
    /**
     * @var string
     */
    const CODE = 'aramex';
    protected $_code = self::CODE;
    protected $_result = null;
    protected $_defaultGatewayUrl = null;
    protected $_helper;
    protected $logger;
    protected $_scopeConfig;
    protected $pageFactory;
    

    protected $_baseCurrencyRate;
    protected $_xmlAccessRequest;
    protected $_localeFormat;
    protected $_logger;
    protected $configHelper;
    protected $_errors = [];
    protected $_isFixed = true;
    

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     * @param \Aramex\Shipment\Helper\Data $helper
     */
    public function __construct(
        \Aramex\Shipment\Helper\Data $helper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Cms\Model\PageFactory $pageFactory,

        Security $xmlSecurity,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        Config $configHelper,
        array $data = []            
            
    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->logger = $logger;
        $this->_helper = $helper;
        $this->_scopeConfig = $scopeConfig;
        $this->pageFactory = $pageFactory;
        $this->_localeFormat = $localeFormat;
        $this->configHelper = $configHelper;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $data
        );

        $this->_defaultGatewayUrl = $this->_helper->getWsdlPath() . 'Tracking.wsdl';
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {

        return ['aramex' => $this->getConfigData('name')];
    }
        /**
     * Return array of authenticated information
     *
     * @return array
     */
    protected function _getAuthDetails() {
        return array(
            'ClientInfo' => array(
                'AccountCountryCode' => $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountEntity' => $this->_scopeConfig->getValue('aramex/settings/account_entity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountNumber' => $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountPin' => $this->_scopeConfig->getValue('aramex/settings/account_pin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'UserName' => $this->_scopeConfig->getValue('aramex/settings/user_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'Password' => $this->_scopeConfig->getValue('aramex/settings/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'Version' => 'v1.0'
            )
        );
    }

    /**
     * @param RateRequest $request
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        $this->_request = $request;

        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $this->setRequest($request);
        $this->_result = $this->_getQuotes();
        $this->_updateFreeMethodQuote($request);
        return $this->getResult();
    }

    public function getResult()
    {
        return $this->_result;
    }

    public function setRequest(RateRequest $request)
    {
        $this->_request = $request;
        $r = new \Magento\Framework\DataObject();

        if ($request->getLimitMethod()) {
            $r->setService($request->getLimitMethod());
        } else {
            $r->setService('ALL');
        }

        if ($request->getAramexUserid()) {
            $userId = $request->getAramexUserid();
        } else {
            $userId = $this->getConfigData('userid');
        }
        $r->setUserId($userId);

        if ($request->getAramexContainer()) {
            $container = $request->getAramexContainer();
        } else {
            $container = $this->getConfigData('container');
        }
        $r->setContainer($container);

        if ($request->getAramexSize()) {
            $size = $request->getAramexSize();
        } else {
            $size = $this->getConfigData('size');
        }
        $r->setSize($size);

        if ($request->getAramexMachinable()) {
            $machinable = $request->getAramexMachinable();
        } else {
            $machinable = $this->getConfigData('machinable');
        }
        $r->setMachinable($machinable);

        if ($request->getOrigPostcode()) {
            $r->setOrigPostal($request->getOrigPostcode());
        } else {
            $r->setOrigPostal($this->_scopeConfig->getValue('shipping/origin/postcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1);
            // $r->setOrigPostal(Mage::getStoreConfig('shipping/origin/postcode'));
        }

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }
        $r->setDestCountryId($destCountry);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $countryHelper = $objectManager->create('Magento\Directory\Model\Config\Source\Country');
        $countries = $countryHelper->toOptionArray(); //Load an array of countries

        foreach ($countries as $countryKey => $country) {
            if ($country['value'] == $destCountry) {
                $r->setDestCountryName($country['label']);
            }
        }
        if ($request->getDestPostcode()) {
            $r->setDestPostal($request->getDestPostcode());
        }

        $weight = $this->getTotalNumOfBoxes($request->getPackageWeight());
        $r->setWeightPounds(floor($weight));
        $r->setPackageQty($request->getPackageQty());

        $r->setWeightOunces(round(($weight - floor($weight)) * 16, 1));

        if ($request->getFreeMethodWeight() != $request->getPackageWeight()) {
            $r->setFreeMethodWeight($request->getFreeMethodWeight());
        }
        $r->setDestState($request->getDestRegionCode());
        $r->setValue($request->getPackageValue());
        $r->setValueWithDiscount($request->getPackageValueWithDiscount());

//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mylog.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->debug(print_r($r, true));

        $this->_rawRequest = $r;
        return $this;
    }

    protected function _getQuotes()
    {
        return $this->_getAramexQuotes();
        /*return false; */
    }

    public function _getAramexQuotes()
    {
        $r = $this->_rawRequest;
        $pkgWeight = $r->getWeightPounds();
        $pkgQty = $r->getPackageQty();


        $product_group = 'EXP';
        $allowed_methods_key = 'allowed_international_methods';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $allowed_methods = $objectManager->create('Aramex\Shipping\Model\Carrier\Aramex\Source\Internationalmethods')->toKeyArray();


        if ($this->_scopeConfig->getValue('aramex/shipperdetail/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == $r->getDestCountryId()) {
            $product_group = 'DOM';
            $allowed_methods = $objectManager->create('Aramex\Shipping\Model\Carrier\Aramex\Source\Domesticmethods')->toKeyArray();
            $allowed_methods_key = 'allowed_domestic_methods';
        }

        $admin_allowed_methods = explode(',', $this->getConfigData($allowed_methods_key));
        $admin_allowed_methods = array_flip($admin_allowed_methods);
        $allowed_methods = array_intersect_key($allowed_methods, $admin_allowed_methods);

        $baseUrl = $this->_helper->getWsdlPath();


        $clientInfo = $this->_helper->getClientInfo();
        $OriginAddress = array(
            'StateOrProvinceCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/state', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'City' => $this->_scopeConfig->getValue('aramex/shipperdetail/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'PostCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/postalcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'CountryCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
        );
        $DestinationAddress = array(
            'StateOrProvinceCode' => $r->getDestState(),
            'City' => '',
            'PostCode' => self::USA_COUNTRY_ID == $r->getDestCountryId() ? substr($r->getDestPostal(), 0, 5) : $r->getDestPostal(),
            'CountryCode' => $r->getDestCountryId(),
        );
        $ShipmentDetails = array(
            'PaymentType' => 'P',
            'ProductGroup' => $product_group,
            'ProductType' => '',
            'ActualWeight' => array('Value' => $pkgWeight, 'Unit' => 'LB'),
            'ChargeableWeight' => array('Value' => $pkgWeight, 'Unit' => 'LB'),
            'NumberOfPieces' => $pkgQty
        );


        //SOAP object
        $soapClient = new \Zend\Soap\Client($baseUrl . 'aramex-rates-calculator-wsdl.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);

        $params = array('ClientInfo' => $clientInfo, 'OriginAddress' => $OriginAddress, 'DestinationAddress' => $DestinationAddress, 'ShipmentDetails' => $ShipmentDetails);

//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mylog.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->debug(print_r($params, true));
        
        
        
        $priceArr = array();
        foreach ($allowed_methods as $m_value => $m_title) {
            $params['ShipmentDetails']['ProductType'] = $m_value;
            try {
                $results = $soapClient->CalculateRate($params);
                if ($results->HasErrors) {
                    if (count($results->Notifications->Notification) > 1) {
                        $error = "";
                        foreach ($results->Notifications->Notification as $notify_error) {
                            $error .= 'Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message . "  *******  ";
                        }
                        $response['error'] = $error;
                    } else {
                        //$response['error'] = $this->__('Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message);
                        $response['error'] = 'Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message;
                    }
                    $response['type'] = 'error';
                } else {
                    $response['type'] = 'success';
                    $priceArr[$m_value] = array('label' => $m_title, 'amount' => $results->TotalAmount->Value);
                }
            } catch (Exception $e) {
                $response['type'] = 'error';
                $response['error'] = $e->getMessage();
            }

        }


        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        if (empty($priceArr)) {
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($response);
            $result->append($error);
            return $error;
        } else {
            foreach ($priceArr as $method => $values) {
                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                $rate = $this->_rateMethodFactory->create();
                $rate->setCarrier($this->_code);
                $rate->setCarrierTitle($this->getConfigData('title'));
                $rate->setMethod($method);
                $rate->setMethodTitle($values['label']);
                /*you can fetch shipping price from different sources over some APIs, we used price from config.xml - xml node price*/
                $rate->setPrice($values['amount']);
                $rate->setCost($values['amount']);
                $result->append($rate);
            }
        }
        return $result;
    }
    
    /**
     * Get tracking
     *
     * @param string|string[] $trackings
     * @return Result
     */
    public function getTracking($trackings) {

        $this->setTrackingReqeust();

        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }

        $this->_getXmlTracking($trackings);

        return $this->_result;
    }
    
    /**
     * Set tracking request
     *
     * @return void
     */
    protected function setTrackingReqeust()
    {
        $r = new \Magento\Framework\DataObject();

        $userId = $this->getConfigData('userid');
        $r->setUserId($userId);

        $this->_rawTrackRequest = $r;
    }
    

   /**
     * Send request for tracking
     *
     * @param string[] $trackings
     * @return void
     */
    protected function _getXmlTracking($trackings)
    {
        $r = $this->_rawTrackRequest;

        foreach ($trackings as $tracking) {
            $this->_parseXmlTrackingResponse($tracking);
        }
    }
    /**
     * Parse xml tracking response
     *
     * @param string $trackingvalue
     * @param string $response
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    protected function _parseXmlTrackingResponse($trackingvalue)
    {
        $resultArr = [];
        
        if (!$this->_result) {
            $this->_result = $this->_trackFactory->create();
        }
        $defaults = $this->getDefaults();
        $url = $this->_defaultGatewayUrl;
 
        //SOAP object
        $clientAramex = new \Zend\Soap\Client($url);
        $clientAramex->setSoapVersion(SOAP_1_1);
        
        $aramexParams = $this->_getAuthDetails();

	$aramexParams['Transaction'] 	= array('Reference1' => '001' );
	$aramexParams['Shipments'] 		= array($trackingvalue);
       
        $_resAramex = $clientAramex->TrackShipments($aramexParams);
        
        if(is_object($_resAramex) && !$_resAramex->HasErrors){
            
            $tracking = $this->_trackStatusFactory->create();
            $tracking->setCarrier('aramex');
            $tracking->setCarrierTitle($this->getConfigData('title'));
            $tracking->setTracking($trackingvalue);

            if(!empty($_resAramex->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult)){
		$tracking->setTrackSummary($this->getTrackingInfoTable($_resAramex->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult));
            } else {
		$tracking->setTrackSummary('Unable to retrieve quotes, please check if the Tracking Number is valid or contact your administrator.');
            }
            $this->_result->append($tracking);
	} else {
            $errorMessage = '';
            foreach($_resAramex->Notifications as $notification){
		$errorMessage .= '<b>' . $notification->Code . '</b>' . $notification->Message;
            }
            $error = $this->_trackErrorFactory->create();
            $error->setCarrier('aramex');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setTracking($trackingvalue);
            $error->setErrorMessage($errorMessage);
            $this->_result->append($error);
            }

    }
    
    /**
     * Get tracking response
     *
     * @return string
     */
    public function getResponse()
    {
        $statuses = '';
        if ($this->_result instanceof \Magento\Shipping\Model\Tracking\Result) {
            if ($trackings = $this->_result->getAllTrackings()) {
                foreach ($trackings as $tracking) {
                    if ($data = $tracking->getAllData()) {
                        if (!empty($data['track_summary'])) {
                            $statuses .= __($data['track_summary']);
                        } else {
                            $statuses .= __('Empty response');
                        }
                    }
                }
            }
        }
        if (empty($statuses)) {
            $statuses = __('Empty response');
        }

        return $statuses;
    }   
    
    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getTrackingInfoTable($HAWBHistory) {

        $_resultTable = '<table summary="Item Tracking"  class="data-table">';
        $_resultTable .= '<col width="1">
                          <col width="1">
                          <col width="1">
                          <col width="1">
                          <thead>
                          <tr class="first last">
                          <th>Location</th>
                          <th>Action Date/Time</th>
                          <th class="a-right">Tracking Description</th>
                          <th class="a-center">Comments</th>
                          </tr>
                          </thead><tbody>';

        foreach ($HAWBHistory as $HAWBUpdate) {

            $_resultTable .= '<tr>
                <td>' . $HAWBUpdate->UpdateLocation . '</td>
                <td>' . $HAWBUpdate->UpdateDateTime . '</td>
                <td>' . $HAWBUpdate->UpdateDescription . '</td>
                <td>' . $HAWBUpdate->Comments . '</td>
                </tr>';
        }
        $_resultTable .= '</tbody></table>';

        return $_resultTable;
    }
      protected function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
    }
    

}
