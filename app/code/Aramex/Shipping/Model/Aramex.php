<?php

namespace Aramex\Shipping\Model;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Carrier\AbstractCarrierOnline;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Ups\Helper\Config;
use Magento\Framework\Xml\Security;

class Aramex extends AbstractCarrierOnline implements CarrierInterface {

    const CODE = 'aramex';
    protected $_code = self::CODE;
    protected $_request;
    protected $_result;
    protected $_baseCurrencyRate;
    protected $_xmlAccessRequest;
    protected $_localeFormat;
    protected $_logger;
    protected $configHelper;
    protected $_errors = [];
    protected $_isFixed = true;
    protected $_defaultGatewayUrl = null;
    protected $_helper;
    protected $_storeManager;

    public function __construct(
    \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory, 
    \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory, 
    \Aramex\Shipment\Helper\Data $helper, 
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
    \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory, 
    \Psr\Log\LoggerInterface $logger, Security $xmlSecurity, 
    \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory, 
    \Magento\Shipping\Model\Rate\ResultFactory $rateFactory, 

    \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory, 
    \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory, 
    \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory, 
    \Magento\Directory\Model\RegionFactory $regionFactory, 
    \Magento\Directory\Model\CountryFactory $countryFactory, 
    \Magento\Directory\Model\CurrencyFactory $currencyFactory, 
    \Magento\Directory\Helper\Data $directoryData, 
    \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry, 
    \Magento\Framework\Locale\FormatInterface $localeFormat, 
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    Config $configHelper, 
            
    array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_helper = $helper;
        $this->_localeFormat = $localeFormat;
        $this->configHelper = $configHelper;
        $this->_storeManager = $storeManager;
        parent::__construct(
                $scopeConfig, 
                $rateErrorFactory, 
                $logger, 
                $xmlSecurity, 
                $xmlElFactory, 
                $rateFactory, 
                $rateMethodFactory, 
                $trackFactory, 
                $trackErrorFactory, 
                $trackStatusFactory, 
                $regionFactory, 
                $countryFactory, 
                $currencyFactory, 
                $directoryData, 
                $stockRegistry, 
                
                $data
        );
        $this->_defaultGatewayUrl = $this->_helper->getWsdlPath() . 'Tracking.wsdl';
    }

    protected function _doShipmentRequest(\Magento\Framework\DataObject $request) {
        
    }

    public function getAllowedMethods() {
        
    }

    public function collectRates(RateRequest $request) {
        $this->_request = $request;
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $this->setRequest($request);
        return $this->_result = $this->_getQuotes();
    }

    public function setRequest(RateRequest $request) {
        $this->_request = $request;
        $r = new \Magento\Framework\DataObject();

        if ($request->getLimitMethod()) {
            $r->setService($request->getLimitMethod());
        } else {
            $r->setService('ALL');
        }

        if ($request->getAramexUserid()) {
            $userId = $request->getAramexUserid();
        } else {
            $userId = $this->getConfigData('userid');
        }
        $r->setUserId($userId);

        if ($request->getAramexContainer()) {
            $container = $request->getAramexContainer();
        } else {
            $container = $this->getConfigData('container');
        }
        $r->setContainer($container);

        if ($request->getAramexSize()) {
            $size = $request->getAramexSize();
        } else {
            $size = $this->getConfigData('size');
        }
        $r->setSize($size);

        if ($request->getAramexMachinable()) {
            $machinable = $request->getAramexMachinable();
        } else {
            $machinable = $this->getConfigData('machinable');
        }
        $r->setMachinable($machinable);

        if ($request->getOrigPostcode()) {
            $r->setOrigPostal($request->getOrigPostcode());
        } else {
            $r->setOrigPostal($this->_scopeConfig->getValue('shipping/origin/postcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == 1);
            // $r->setOrigPostal(Mage::getStoreConfig('shipping/origin/postcode'));
        }

        if ($request->getDestCountryId()) {
            $destCountry = $request->getDestCountryId();
        } else {
            $destCountry = self::USA_COUNTRY_ID;
        }
        $r->setDestCountryId($destCountry);

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $countryHelper = $objectManager->create('Magento\Directory\Model\Config\Source\Country');
        $countries = $countryHelper->toOptionArray(); //Load an array of countries

        foreach ($countries as $countryKey => $country) {
            if ($country['value'] == $destCountry) {
                $r->setDestCountryName($country['label']);
            }
        }
        if ($request->getDestPostcode()) {
            $r->setDestPostal($request->getDestPostcode());
        }

        $weight = $this->getTotalNumOfBoxes($request->getPackageWeight());

        $r->setWeightPounds($weight);
        $r->setPackageQty($request->getPackageQty());

        $r->setWeightOunces(round(($weight - floor($weight)) * 16, 1));

        if ($request->getFreeMethodWeight() != $request->getPackageWeight()) {
            $r->setFreeMethodWeight($request->getFreeMethodWeight());
        }
        $r->setDestState($request->getDestRegionCode());
        $r->setValue($request->getPackageValue());
        $r->setValueWithDiscount($request->getPackageValueWithDiscount());

//$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/mylog.log');
//$logger = new \Zend\Log\Logger();
//$logger->addWriter($writer);
//$logger->debug(print_r($r, true));
        $r->setDestCity($request->getDestCity());
        $this->_rawRequest = $r;
        return $this;
    }

    protected function _getQuotes() {

        return $this->_getAramexQuotes();
    }

    public function _getAramexQuotes() {
        $r = $this->_rawRequest;
        $pkgWeight = $r->getWeightPounds();
        $pkgQty = $r->getPackageQty();
        $product_group = 'EXP';
        $allowed_methods_key = 'allowed_international_methods';
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $allowed_methods = $objectManager->create('Aramex\Shipping\Model\Carrier\Aramex\Source\Internationalmethods')->toKeyArray();

        if ($this->_scopeConfig->getValue('aramex/shipperdetail/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) == $r->getDestCountryId()) {
            $product_group = 'DOM';
            $allowed_methods = $objectManager->create('Aramex\Shipping\Model\Carrier\Aramex\Source\Domesticmethods')->toKeyArray();
            $allowed_methods_key = 'allowed_domestic_methods';
        }

        $admin_allowed_methods = explode(',', $this->getConfigData($allowed_methods_key));
        $admin_allowed_methods = array_flip($admin_allowed_methods);
        $allowed_methods = array_intersect_key($allowed_methods, $admin_allowed_methods);

        $baseUrl = $this->_helper->getWsdlPath();
        $clientInfo = $this->_helper->getClientInfo();
        $OriginAddress = array(
            'StateOrProvinceCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/state', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'City' => $this->_scopeConfig->getValue('aramex/shipperdetail/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'PostCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/postalcode', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'CountryCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/country', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
        );
        $DestinationAddress = array(
            /*'StateOrProvinceCode' => $r->getDestState(),
            'City' => $r->getDestCity(),*/
            //'StateOrProvinceCode' => $r->getDestCity(),
            //'City' => $r->getDestState(),
            'StateOrProvinceCode' => $this->_scopeConfig->getValue('aramex/shipperdetail/city', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'City' => $this->_scopeConfig->getValue('aramex/shipperdetail/state', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
            'PostCode' => self::USA_COUNTRY_ID == $r->getDestCountryId() ? substr($r->getDestPostal(), 0, 5) : $r->getDestPostal(),
            'CountryCode' => $r->getDestCountryId(),
        );
        $ShipmentDetails = array(
            'PaymentType' => 'P',
            'ProductGroup' => $product_group,
            'ProductType' => '',
            'ActualWeight' => array('Value' => $pkgWeight, 'Unit' => 'KG'),
            'ChargeableWeight' => array('Value' => $pkgWeight, 'Unit' => 'KG'),
            'NumberOfPieces' => $pkgQty
        );

        //SOAP object
       
        $soapClient = new \Zend\Soap\Client($baseUrl . 'aramex-rates-calculator-wsdl.wsdl');
        $soapClient->setSoapVersion(SOAP_1_1);
        $baseCurrencyCode = $this->_storeManager->getStore()->getBaseCurrency()->getCode();
        $params = array('ClientInfo' => $clientInfo, 'OriginAddress' => $OriginAddress, 'DestinationAddress' => $DestinationAddress, 'ShipmentDetails' => $ShipmentDetails, 'PreferredCurrencyCode' => $baseCurrencyCode);
        $priceArr = array();

        foreach ($allowed_methods as $m_value => $m_title) {
            $params['ShipmentDetails']['ProductType'] = $m_value;
            
            if($m_value == "CDA"){  
                $params['ShipmentDetails']['Services'] = "CODS"; 
            }else{
                $params['ShipmentDetails']['Services'] = "";
            }
            
            try {
                $results = $soapClient->CalculateRate($params);
                
                if ($results->HasErrors) {
                    if (count($results->Notifications->Notification) > 1) {
                        $error = "";
                        foreach ($results->Notifications->Notification as $notify_error) {
                            $error .= 'Aramex: ' . $notify_error->Code . ' - ' . $notify_error->Message . "  *******  ";
                        }
                        $response['error'] = $error;
                    } else {
                        //$response['error'] = $this->__('Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message);
                        $response['error'] = 'Aramex: ' . $results->Notifications->Notification->Code . ' - ' . $results->Notifications->Notification->Message;
                    }
                    $response['type'] = 'error';
                } else {
                    $response['type'] = 'success';
                    $priceArr[$m_value] = array('label' => $m_title, 'amount' => $results->TotalAmount->Value, 'currency' => $results->TotalAmount->CurrencyCode);
                }
            } catch (Exception $e) {
                $response['type'] = 'error';
                $response['error'] = $e->getMessage();
            }

        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();

        if (empty($priceArr)) {
            if(isset($response)) {
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier($this->_code);
                $error->setCarrierTitle($this->getConfigData('title'));
                $error->setErrorMessage($response);
                $result->append($error);
                return $error;
            }
        } else {
            foreach ($priceArr as $method => $values) {
                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                $rate = $this->_rateMethodFactory->create();
                $rate->setCarrier($this->_code);
                $rate->setCarrierTitle($this->getConfigData('title'));
                $rate->setMethod($method);
                $rate->setMethodTitle($values['label']);
                $rate->setPrice($values['amount']);
                $rate->setCost($values['amount']);
                $result->append($rate);
            }
        }
        return $result;
    }

    public function proccessAdditionalValidation(\Magento\Framework\DataObject $request) {
        return true;
    }

    public function getTracking($trackings) {

        $this->setTrackingReqeust();
        if (!is_array($trackings)) {
            $trackings = [$trackings];
        }

        $this->_getXmlTracking($trackings);
        return $this->_result;
    }

    protected function _getXmlTracking($trackings) {
        $r = $this->_rawTrackRequest;

        foreach ($trackings as $tracking) {
            $this->_parseXmlTrackingResponse($tracking);
        }
    }

    protected function setTrackingReqeust() {

        $r = new \Magento\Framework\DataObject();
        $userId = $this->getConfigData('userid');
        $r->setUserId($userId);
        $this->_rawTrackRequest = $r;
    }

    protected function _parseXmlTrackingResponse($trackingvalue) {
        $resultArr = [];
        if (!$this->_result) {
            $this->_result = $this->_trackFactory->create();
        }
        $defaults = $this->getDefaults();
        $url = $this->_defaultGatewayUrl;

        //SOAP object
        $clientAramex = new \Zend\Soap\Client($url);
        $clientAramex->setSoapVersion(SOAP_1_1);

        $aramexParams = $this->_getAuthDetails();
        $aramexParams['Transaction'] = array('Reference1' => '001');
        $aramexParams['Shipments'] = array($trackingvalue);

        $_resAramex = $clientAramex->TrackShipments($aramexParams);

        if (is_object($_resAramex) && !$_resAramex->HasErrors) {

            $tracking = $this->_trackStatusFactory->create();
            $tracking->setCarrier('aramex');
            $tracking->setCarrierTitle($this->getConfigData('title'));
            $tracking->setTracking($trackingvalue);

            if (!empty($_resAramex->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult)) {
                $tracking->setTrackSummary($this->getTrackingInfoTable($_resAramex->TrackingResults->KeyValueOfstringArrayOfTrackingResultmFAkxlpY->Value->TrackingResult));
            } else {
                $tracking->setTrackSummary('Unable to retrieve quotes, please check if the Tracking Number is valid or contact your administrator.');
            }
            $this->_result->append($tracking);
        } else {
            $errorMessage = '';
            foreach ($_resAramex->Notifications as $notification) {
                $errorMessage .= '<b>' . $notification->Code . '</b>' . $notification->Message;
            }
            $error = $this->_trackErrorFactory->create();
            $error->setCarrier('aramex');
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setTracking($trackingvalue);
            $error->setErrorMessage($errorMessage);
            $this->_result->append($error);
        }
    }

    public function getTrackingInfoTable($HAWBHistory) {
        $checkArray = is_array($HAWBHistory);

        $_resultTable = '<table summary="Item Tracking"  class="data-table">';
        $_resultTable .= '<col width="1">
                          <col width="1">
                          <col width="1">
                          <col width="1">
                          <thead>
                          <tr class="first last">
                          <th>Location</th>
                          <th>Action Date/Time</th>
                          <th class="a-right">Tracking Description</th>
                          <th class="a-center">Comments</th>
                          </tr>
                          </thead><tbody>';
        if($checkArray){
            foreach ($HAWBHistory as $HAWBUpdate) {
                $_resultTable .= '<tr>
                    <td>' . $HAWBUpdate->UpdateLocation . '</td>
                    <td>' . $HAWBUpdate->UpdateDateTime . '</td>
                    <td>' . $HAWBUpdate->UpdateDescription . '</td>
                    <td>' . $HAWBUpdate->Comments . '</td>
                    </tr>';
            }
        }else{
                    $_resultTable .= '<tr>
                    <td>' . $HAWBHistory->UpdateLocation . '</td>
                    <td>' . $HAWBHistory->UpdateDateTime . '</td>
                    <td>' . $HAWBHistory->UpdateDescription . '</td>
                    <td>' . $HAWBHistory->Comments . '</td>
                    </tr>';
        }


        $_resultTable .= '</tbody></table>';
        return  $_resultTable;
    }

    protected function _getAuthDetails() {
        return array(
            'ClientInfo' => array(
                'AccountCountryCode' => $this->_scopeConfig->getValue('aramex/settings/account_country_code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountEntity' => $this->_scopeConfig->getValue('aramex/settings/account_entity', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountNumber' => $this->_scopeConfig->getValue('aramex/settings/account_number', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'AccountPin' => $this->_scopeConfig->getValue('aramex/settings/account_pin', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'UserName' => $this->_scopeConfig->getValue('aramex/settings/user_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'Password' => $this->_scopeConfig->getValue('aramex/settings/password', \Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                'Version' => 'v1.0'
            )
        );
    }

    public function checkAvailableShipCountries(\Magento\Framework\DataObject $request){

        $allowedCity = explode(',', $this->_scopeConfig->getValue('carriers/mpjakplusshipping/specificregion',\Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        if(in_array($request->getDestRegionId(), $allowedCity)) {
           return false;     
        } else {
           return $this;
        }
    }

}
