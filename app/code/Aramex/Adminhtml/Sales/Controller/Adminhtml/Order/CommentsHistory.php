<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aramex\Adminhtml\Sales\Controller\Adminhtml\Order;

/**
 * Class CommentsHistory
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CommentsHistory extends \Magento\Sales\Controller\Adminhtml\Order\CommentsHistory
{
    public function execute()
    {
        $this->_initOrder();
        $layout = $this->layoutFactory->create();

        $html = $layout->createBlock('Magento\Sales\Block\Adminhtml\Order\View\Tab\History')
            ->setTemplate('Aramex_Shipment::aramex/sales/order/view/tab/history.phtml')
            ->toHtml();
        $this->_translateInline->processResponseBody($html);
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setContents($html);
        return $resultRaw;
    }
}
