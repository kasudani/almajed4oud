<?php

namespace Aramex\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper {

    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $om
     * 
     */
    public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->_scopeConfig = $scopeConfig;
    }

    public function getEmails($configPath, $storeId) {
        $data = $this->_scopeConfig->getValue($configPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
        if (!empty($data)) {
            return explode(',', $data);
        }
        return false;
    }

}
