define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'mage/storage'
], function ($, wrapper,quote, storage) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction, messageContainer) {
            if (messageContainer.custom_attributes != undefined) {
                $.each(messageContainer.custom_attributes , function( key, value ) {
                    if(!value){ 
                        value = $('input[name="custom_attributes[latlong]"]').val();
                    }
                    if($.isPlainObject(value)){ 
                        value = value['value'];
                    }
                    var dataToPass = {};
                    dataToPass['latlong'] = value;
                    dataToPass['address_id'] = "new-customer-address"; 
                    storage.post(
                        'saddress/index/address',
                        JSON.stringify(dataToPass),
                        true
                    ).done(
 
                    );

                });
            }

           /* console.log(messageContainer.custom_attributes);
            console.log(messageContainer);
            if (messageContainer.custom_attributes != undefined) {
                $.each(messageContainer.custom_attributes , function( key, value ) {
                    if(!value){
                        value = $('input[name="custom_attributes[latlong]"]').val();
                    }
                    if($.isPlainObject(value)){ 
                        value = value['value'];
                    }

                    messageContainer['custom_attributes'][key] = {'attribute_code':key,'value':value};
                    /*if(messageContainer['custom_attributes'][key])
                        messageContainer['custom_attributes'][key]['value'] = value;
                    else{ 
                        messageContainer['custom_attributes'][key] = {'attribute_code':key,'value':value};
                    }
                });
            }*/

            return originalAction(messageContainer);
        });
    };
});