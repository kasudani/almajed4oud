<?php
namespace Custom\Succex\Observer;
 
class MyObserver implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
       $displayText = $observer->getData('order_ids');

       if(isset($displayText[0]) && $displayText[0] && isset($_SESSION['latlong']) && $_SESSION['latlong'] && $_SESSION['latlong'] != ''){

       		$orderID = $displayText[0];
       		$latlong = $_SESSION['latlong'];
	       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	       $orderObj = $objectManager->create('Magento\Sales\Model\Order')->load($orderID);

	       $shippingAddressObj = $orderObj->getShippingAddress();
	       $shippingAddressObj->setLatlong($latlong);
	        $shippingAddressArray = $shippingAddressObj->getData();
	       //print_r($shippingAddressArray); 
	       $shippingAddressObj->save();


			/*$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
			$connection = $resource->getConnection();
	        $themeTable = $resource->getTableName('customer_address_entity_varchar');
	        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);

	        $sql = "Select * FROM  customer_address_entity_varchar where attribute_id='183' AND entity_id = '".$shippingAddressArray['customer_address_id']."'";// limit 0,4
	        $result = $connection->fetchAll($sql);
	        print_r($result);*/


	       $address = $objectManager->create('Magento\Customer\Model\Address')->load($shippingAddressArray['customer_address_id']);
	       $AddressArray = $address->getData();
	       //print_r($AddressArray);
	       //print_r($address->getCity());
			//$address->setCustomerId($customer->getId())
			$address->setLatlong($latlong);
			try{

				$address->save();
			} catch(Exception $e) {

			}

			$_SESSION['latlong'] = "";

       }

       if(isset($displayText[0]) && $displayText[0]){
       		$orderID = $displayText[0];
       		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	        $orderObj = $objectManager->create('Magento\Sales\Model\Order')->load($orderID);

	        $shippingAddressObj = $orderObj->getShippingAddress();
	        $shippingAddressArray = $shippingAddressObj->getData();

	        $payment = $orderObj->getPayment();
	        $method = $payment->getMethodInstance();
	        $methodTitle = $method->getTitle(); 
	        if($payment->getMethod()=='cashondelivery'){


	          $customerSession = $objectManager->create('Magento\Customer\Model\SessionFactory')->create();
	          $model = $objectManager->create('Custom\Confirm\Model\Token');
	          $data   = array('token'=>$this->createRandomCode(), 'user_id'=>$customerSession->getCustomer()->getId(), 'order_id'=> $orderID);
	          $model->setData($data);
	          $model->save();
	          $this->sendSMS($shippingAddressArray['telephone'], $data);
	        }


       }

       
       /*
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $themeTable = $resource->getTableName('ordermedicine_data');
        $currenttime=gmstrftime('%Y-%m-%d %H:%M:%S',time()+19800);
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customerId = $customerSession->getCustomerId();
      //  print_r($customerSession->getData());
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);


       print_r($displayText);*/
       return $this;
    }



    function createRandomCode() { 

        $chars = "abcdefghijkmnopqrstuvwxyz023456789"; 
        srand((double)microtime()*1000000); 
        $i = 0; 
        $pass = '' ; 

        while ($i <= 7) { 
            $num = rand() % 33; 
            $tmp = substr($chars, $num, 1); 
            $pass = $pass . $tmp; 
            $i++; 
        } 

        return $pass; 

    } 



    public function sendSMS($telephone, $data){
      $user = "ecommerce@almajed4oud.com";
      $pass = "G@majed4oud";
      //$numbers = "+919694050706";
      $numbers = $telephone;
      $sender = "Al Majed-AD";
      $token = $data['token'];
      $msg = "Please click on this url to confirm order http://59ee1ae106.nxcli.net/confirm/order/index/token/$token";
      $unicode = "UTF8";

      $url = "http://api.unifonic.com/wrapper/sendSMS.php?";
      $stringToPost = "userid=".$user."&password=".$pass."&to=".$numbers."&sender=".$sender."&msg=".$msg."&encoding=".$unicode;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_TIMEOUT, 5);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $stringToPost);
      $result = curl_exec($ch);

      $this->__saveLog($msg,$data['user_id'], $data['order_id'], 3, $result, $telephone);
      return $result;
    }

    private function __saveLog($message_text,$customerId=null,$orderId=null,$message_type,$buffer,$target){
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
       $smsCollection = $objectManager->create('\Emipro\Smsnotification\Model\Smslog');
       $smsCollection->setorderId($orderId);
       $smsCollection->setCustomerId($customerId);
       $smsCollection->setSmsContent($message_text);
       $smsCollection->setMessageType($message_type);
       $smsCollection->setApiResult($buffer);
       $smsCollection->setContactNumber($target);
       $smsCollection->setUpdatedAt(date('Y-m-d h:m:i'));
       $smsCollection->save();
    }

}