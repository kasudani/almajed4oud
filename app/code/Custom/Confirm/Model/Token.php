<?php
namespace Custom\Confirm\Model;

class Token extends \Magento\Framework\Model\AbstractModel 
{

	const CACHE_TAG = 'confirm_tokens';
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Custom\Confirm\Model\ResourceModel\Token');
    }

}