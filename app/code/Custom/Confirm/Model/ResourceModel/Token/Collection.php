<?php
namespace  Custom\Confirm\Model\ResourceModel\Token;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Custom\Confirm\Model\Token','Custom\Confirm\Model\ResourceModel\Token');
    }
}