<?php
namespace Custom\Confirm\Model\ResourceModel;
class Token extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('confirm_tokens','id');
    }
}