<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Custom\Confirm\Controller\Order;

use Magento\Sales\Model\Order;
use Magento\Backend\App\Action\Context;
use \Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
   
    protected $_messageManager;

    public function __construct(
        Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        parent::__construct($context);
        $this->_messageManager = $messageManager;
    }

    public function execute()
    {
      $token = $this->getRequest()->getParam("token"); 

      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

      $collection = $objectManager->create('Custom\Confirm\Model\ResourceModel\Token\Collection')->addFieldToFilter('token', $token);

      $order_id = '';
      $user_id = '';
      $token_id = '';
      //print_r(get_class_methods(get_class($collection)));
      if(count($collection)){
        foreach ($collection as $key => $value) {
          $value = $value->getData();
          $token_id = $value['id'];
          $order_id = $value['order_id'];
          $user_id = $value['user_id'];
        }
      }
      if($order_id!=''){
        $orderObj = $objectManager->create('Magento\Sales\Model\Order')->load($order_id);

        $orderState = Order::STATE_PROCESSING;
        //$orderObj->setState($orderState)->setStatus('confirmed101');
        $orderObj->setStatus('confirmed101');
        $orderObj->save();


        /*Order status history comment*/
        $history = $orderObj->addStatusHistoryComment('Order status updated by User confirmation using SMS link', $orderObj->getStatus());
        $history->setIsVisibleOnFront(0);
        $history->setIsCustomerNotified(1);
        $history->save();
        
        /*Notify Customer by Email*/
        $orderCommentSender = $objectManager
        ->create('Magento\Sales\Model\Order\Email\Sender\OrderCommentSender');
        $orderCommentSender->send($orderObj, true);


        $model = $objectManager->create('Custom\Confirm\Model\Token');
        $model->load($token_id);
        $model->delete();


        $this->_messageManager->addSuccessMessage('Order confirmed successfully');
      } else {
        $this->_messageManager->addErrorMessage('Token Expired or Invalid');
      }
      
      $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
      $resultRedirect->setUrl('/');

      return $resultRedirect;

    }


     


}
