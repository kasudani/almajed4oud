var inp = '';
function addressPageCall(){
    var region_id = jQuery('#region_id');
    if (typeof(region_id) != 'undefined' && region_id != null) {
        inp = document.getElementById('#city');
        var value = jQuery('#region_id').val();
        if (value != '' && typeof(value) != 'undefined') {
            getRegionCitiesAddress(value,'edit');
        }
        jQuery('#region_id').change(function(event) {
            var value = jQuery('#region_id').val();
            if (value != '') {
                getRegionCitiesAddress(value,'edit');
            }
        });
        jQuery('#country_id').change(function(event) {
            var value = jQuery('#region_id').val();
            if (value != '') {
                getRegionCitiesAddress(value,'edit');
            } else {
                jQuery('#city').html(inp);
                jQuery('.billing_notinlist').remove();
            }
        });
    }
}
function shippingmainCityCart(){
    if(jQuery('#shipping-zip-form').length==0){
        setTimeout(function(){ shippingmainCityCart();}, 2000);
    }
    var region_id = jQuery('#shipping-zip-form [name="region_id"]');
    if (typeof(region_id) != 'undefined' && region_id != null
        && jQuery('#shipping-zip-form [name="city"]') != 'undefined'
        && jQuery('#shipping-zip-form [name="city"]') !=null) {
        var city_id =  jQuery('#shipping-zip-form [name="city"]').attr('id');
        inp = document.getElementById(city_id);
        var value = jQuery('#shipping-zip-form [name="region_id"]').val();
        if (value != '' && typeof(value) != 'undefined') {
            getRegionCities(value,'shipping-zip-form');
        }
        jQuery('#shipping-zip-form [name="region_id"]').change(function(event) {
            var value = jQuery('#shipping-zip-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'shipping-zip-form');
            }
        });
        jQuery('#shipping-zip-form [name="country_id"]').change(function(event) {
            var value = jQuery('#shipping-zip-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'shipping-zip-form');
            } else {
                jQuery('#shipping-zip-form [name="city"]').html(inp);
                jQuery('#shipping-zip-form .billing_notinlist').remove();
            }
        });
    }
}
function bilingmainCityCall(){
    if(jQuery('#billing-new-address-form [name="region_id"]').length == 0){
        setTimeout(function(){ bilingmainCityCall();}, 1000);
    }
    var region_id = jQuery('#billing-new-address-form [name="region_id"]');
    if (typeof(region_id) != 'undefined' && region_id != null) {
        var city_id =  jQuery('#billing-new-address-form [name="city"]').attr('id');
        inp = document.getElementById(city_id);
        var value = jQuery('#billing-new-address-form [name="region_id"]').val();
        if (value != '' && typeof(value) != 'undefined') {
            getRegionCities(value,'billing-new-address-form');
        }
        jQuery('#billing-new-address-form [name="region_id"]').change(function(event) {
            var value = jQuery('#billing-new-address-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'billing-new-address-form');
            }
        });
        jQuery('#billing-new-address-form [name="country_id"]').change(function(event) {
            var value = jQuery('#billing-new-address-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'billing-new-address-form');
            } else {
                jQuery('#billing-new-address-form [name="city"]').html(inp);
                jQuery('#billing-new-address-form .billing_notinlist').remove();
            }
        });
    }
}

function shippingmainCityCall(){
    if(jQuery('#co-shipping-form [name="region_id"]').length == 0){
        setTimeout(function(){ shippingmainCityCall();}, 1000);
    }else if(jQuery('#shipping').css('display') == 'none' || jQuery('#co-shipping-form').css('display')== 'none'){
        setTimeout(function(){ bilingmainCityCall();}, 1000);
    }
    var region_id = jQuery('#co-shipping-form [name="region_id"]');
    if (typeof(region_id) != 'undefined' && region_id != null) {
        var city_id =  jQuery('#co-shipping-form [name="city"]').attr('id');
        inp = document.getElementById(city_id);
        var value = jQuery('#co-shipping-form [name="region_id"]').val();
        if (value != '' && typeof(value) != 'undefined') {
            getRegionCities(value,'co-shipping-form');
        }
        jQuery('#co-shipping-form [name="region_id"]').change(function(event) {
            var value = jQuery('#co-shipping-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'co-shipping-form');
            }
        });
        jQuery('#co-shipping-form [name="country_id"]').change(function(event) {
            var value = jQuery('#co-shipping-form [name="region_id"]').val();
            if (value != '') {
                getRegionCities(value,'co-shipping-form');
            } else {
                jQuery('#co-shipping-form [name="city"]').html(inp);
                jQuery('#co-shipping-form .billing_notinlist').remove();
            }
        });
    }
}
/* This is for checkout Step */
var ajaxLoading = false;
function getRegionCities(value,main_id) {
    if(!ajaxLoading) {
        ajaxLoading = true;
        var city_id =  jQuery('#'+main_id+' [name="city"]').attr('id');
        var url = window.data_url;
        var loader = '<div data-role="loader" class="loading-mask city_loading_mask" style="position: relative;text-align:right;"><div class="loader"><img src="'+window.loading_url+'" alt="Loading..." style="position: absolute;text-align:center;"></div>Please wait loading cities...</div>';
        if(jQuery('#'+main_id+' .city_loading_mask').length==0){
            jQuery('#'+main_id+' [name="city"]').after(loader);
        }
        emptyInput('',main_id);
        jQuery('#error-'+city_id).hide();
        jQuery('.mage-error').hide();
        jQuery('#'+main_id+' [name="city"]').hide();
        jQuery('#'+city_id+'-select').remove();
        jQuery('#'+main_id+' .billing_notinlist').remove();
		jQuery('#'+main_id+' .br_billing_notinlist').remove();
		jQuery('#'+main_id+' .postcode_billing_notinlist').remove();
		jQuery('#'+main_id+' .postcode_br_billing_notinlist').remove();
		jQuery('#'+main_id+' [name="zip-select"]').remove();
		jQuery('#'+main_id+' [name="postcode"]').show();
        jQuery.ajax({
            url : url,
            type: "get",
            data:"state="+value,
            dataType: 'json',
        }).done(function (transport) {
            ajaxLoading = false;
            jQuery('#error-'+city_id).show();
            jQuery('.mage-error').show();
            jQuery('#'+main_id+' .city_loading_mask').remove();
            jQuery('#'+main_id+' [name="city"]').show();
            var response = transport;

            var options = '<select onchange="getCityState(this.value,\''+main_id+'\'),getZipcodes(this.value,\''+main_id+'\')" id="'+city_id+'-select" class="select" title="City" name="city-select" ><option value="">Please select city</option>';
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i] + '">' + response[i] + '</option>';
                }
                options += "</select>";
                if(window.data_city_link!=""){
                    var title = window.data_city_title;
                    options+= "<br class='br_billing_notinlist' /><a onclick='notInList(\"billing\",\""+main_id+"\")' class='billing_notinlist' href='javascript:void(0)' class='notinlist'>"+title+"</a>";
                }
                jQuery('#'+main_id+' [name="city"]').hide();
                if(jQuery('#'+city_id+'-select').length==0){
                    jQuery('#'+main_id+' [name="city"]').after(options);
                }
            } else {
                jQuery('#'+main_id+' [name="city"]').html(inp);
                jQuery('#'+main_id+' .billing_notinlist').remove();
            }
        }).fail( function ( error )
        {
            ajaxLoading = false;
            jQuery('#error-'+city_id).show();
            jQuery('#'+main_id+' .city_loading_mask').remove();
            jQuery('#'+main_id+' [name="city"]').show();
            console.log(error);
        });
    }
}
function getRegionCitiesAddress(value,main_id) {
    var main_id = 'edit';
    if(!ajaxLoading) {
        ajaxLoading = true;
        var city_id =  "city";
        var url = window.data_url;
        var loader = '<div data-role="loader" class="loading-mask city_loading_mask" style="position: relative;text-align:right;"><div class="loader"><img src="'+window.loading_url+'" alt="Loading..." style="position: absolute;text-align:center;"></div>Please wait loading cities...</div>';
        if(jQuery('.city_loading_mask').length==0){
            jQuery('#city').after(loader);
        }
        emptyInput('',main_id);
        jQuery('#error-'+city_id).hide();
        jQuery('#city-select-error').remove();
        jQuery('.mage-error').hide();
        jQuery('#city').hide();
        jQuery('#'+city_id+'-select').remove();
        jQuery('.billing_notinlist').remove();
		jQuery('.br_billing_notinlist').remove();
		jQuery('.postcode_billing_notinlist').remove();
		jQuery('.postcode_br_billing_notinlist').remove();
		jQuery('#zip-select,#zip-select-error,#zip-error').remove();
		jQuery('#zip').removeClass('mage-error');
		jQuery('#zip').show();
        jQuery.ajax({
            url : url,
            type: "get",
            data:"state="+value,
            dataType: 'json',
        }).done(function (transport) {
            ajaxLoading = false;
            jQuery('#error-'+city_id).show();
            jQuery('.mage-error').show();
            jQuery('.city_loading_mask').remove();
            jQuery('#city').show();
            var response = transport;

            var options = '<select onchange="getCityState(this.value,\''+main_id+'\'),getZipcodes(this.value,\''+main_id+'\')" id="'+city_id+'-select" class="validate-select select" title="City" name="city-select" ><option value="">Please select city</option>';
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i] + '">' + response[i] + '</option>';
                }
                options += "</select>";
                if(window.data_city_link!=""){
                    var title = window.data_city_title;
                    options+= "<br class='br_billing_notinlist' /><a onclick='notInList(\"billing\",\""+main_id+"\")' class='billing_notinlist' href='javascript:void(0)' class='notinlist'>"+title+"</a>";
                }
                jQuery('#city').hide();
                if(jQuery('#'+city_id+'-select').length==0){
                    jQuery('#city').after(options);
                }
            } else {
                jQuery('#city').html(inp);
                jQuery('.billing_notinlist').remove();
            }
        }).fail( function ( error )
        {
            ajaxLoading = false;
            jQuery('#error-'+city_id).show();
            jQuery('.city_loading_mask').remove();
            jQuery('#city').show();
            console.log(error);
        });
    }
}
/* City not in list */
function notInList(type,main_id){
    if(main_id=='edit'){
        var city_id =  "city";
        jQuery('#'+city_id+'-select').remove();
        jQuery('.billing_notinlist').remove();
        jQuery('.br_billing_notinlist').remove();
        jQuery('#city').show();
    }else{
        var city_id =  jQuery('#'+main_id+' [name="city"]').attr('id');
        jQuery('#'+city_id+'-select').remove();
        jQuery('#'+main_id+' .billing_notinlist').remove();
        jQuery('#'+main_id+' .br_billing_notinlist').remove();
        jQuery('#'+main_id+' [name="city"]').show();
    }

}
function getCityState(val,main_id){
    emptyInput(val,main_id);

}
function getZipState(val,main_id){
    emptyInputZip(val,main_id);
}
function emptyInput(val,main_id){
    if(main_id=='edit'){
        jQuery('#city').focus();
        jQuery('#city').val(val);
		if(val!=""){
			jQuery('#zip-error').remove();
			jQuery('#city-select-error').remove();
			jQuery('#city-select').removeClass('mage-error');
		}
        e = jQuery.Event('keyup');
        e.keyCode= 13; // enter
        jQuery('#city').trigger(e);
	}else{
        jQuery('#'+main_id+' [name="city"]').focus();
        jQuery('#'+main_id+' [name="city"]').val(val);
		if(val!=""){
			jQuery('#city-select-error').remove();
			jQuery('#'+main_id+' [name="city"]').removeClass('mage-error');
		}
        e = jQuery.Event('keyup');
        e.keyCode= 13; // enter
        jQuery('#'+main_id+' [name="city"]').trigger(e);
    }
}
function emptyInputZip(val,main_id){
    if(main_id=='edit'){
        jQuery('#postcode').focus();
        jQuery('#postcode').val(val);
		if(val!=""){
			jQuery('#zip-select-error').remove();
			jQuery('#zip-error').remove();
			jQuery('#zip-select').removeClass('mage-error');
		}
        e = jQuery.Event('keyup');
        e.keyCode= 13; // enter
        jQuery('#postcode').trigger(e);
    }else{
        jQuery('#'+main_id+' [name="postcode"]').focus();
        jQuery('#'+main_id+' [name="postcode"]').val(val);
		if(val!=""){
			jQuery('#zip-select-error').remove();
			jQuery('#'+main_id+' [name="postcode"]').removeClass('mage-error');
			jQuery('#zip-select').removeClass('mage-error');
		}
        e = jQuery.Event('keyup');
        e.keyCode= 13; // enter
        jQuery('#'+main_id+' [name="postcode"]').trigger(e);
    }
}
function getZipcodes(value,type){
	
	if(type!='edit'){
		if (value != '' && jQuery('#'+type+' [name="city-select"]').length > 0 &&  jQuery('#'+type+' [name="city-select"]').is('select')) {
				getPostcodes(value,type);
        }
	}else{
		if (value != '' && jQuery('#city-select').length > 0 &&  jQuery('#city-select').is('select')) {
			getPostcodesForAddress(value,type);
        }
	}
   
}
/* This is for Zip codes */

function getPostcodes(value,main_id) {
	   var postcode_id =  jQuery('#'+main_id+' [name="postcode"]').attr('id');
        var url = window.data_zip_url;
        var loader = '<div data-role="loader" class="loading-mask postcode_loading_mask" style="position: relative;text-align:right;"><div class="loader"><img src="'+window.loading_url+'" alt="Loading..." style="position: absolute;text-align:center;"></div>Please wait loading zip codes...</div>';
        if(jQuery('#'+main_id+' .postcode_loading_mask').length==0){
            jQuery('#'+main_id+' [name="postcode"]').after(loader);
        }
        emptyInputZip('',main_id);
        jQuery('#error-'+postcode_id).hide();
        jQuery('.mage-error').hide();
        jQuery('#'+main_id+' [name="postcode"]').hide();
        jQuery('#'+postcode_id+'-select').remove();
        jQuery('#'+main_id+' .postcode_billing_notinlist').remove();
		jQuery('#'+main_id+' .postcode_br_billing_notinlist').remove();
        jQuery.ajax({
            url : url,
            type: "get",
            data:"city="+value,
            dataType: 'json',
        }).done(function (transport) {
            jQuery('#error-'+postcode_id).show();
            jQuery('.mage-error').show();
            jQuery('#'+main_id+' .postcode_loading_mask').remove();
            jQuery('#'+main_id+' [name="postcode"]').show();
            var response = transport;

            var options = '<select onchange="getZipState(this.value,\''+main_id+'\')" id="'+postcode_id+'-select" class="validate-select select" title="Postcode" name="zip-select" ><option value="">Please select zip code</option>';
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i] + '">' + response[i] + '</option>';
                }
                options += "</select>";
                if(window.data_zip_link!=""){
                    var title = window.data_zip_title;
                    options+= "<br class='postcode_br_billing_notinlist' /><a onclick='notInListZip(\"billing\",\""+main_id+"\")' class='postcode_billing_notinlist' href='javascript:void(0)' class='postcode_notinlist'>"+title+"</a>";
                }
                jQuery('#'+main_id+' [name="postcode"]').hide();
                if(jQuery('#'+postcode_id+'-select').length==0){
                    jQuery('#'+main_id+' [name="postcode"]').after(options);
                }
            } else {
                jQuery('#'+main_id+' [name="postcode"]').html(inp);
                jQuery('#'+main_id+' .postcode_billing_notinlist').remove();
            }
        }).fail( function ( error )
        {
            jQuery('#error-'+postcode_id).show();
            jQuery('#'+main_id+' .postcode_loading_mask').remove();
            jQuery('#'+main_id+' [name="postcode"]').show();
            console.log(error);
        });
}
function getPostcodesForAddress(value,main_id) {
	   var postcode_id =  jQuery('#zip').attr('id');
        var url = window.data_zip_url;
        var loader = '<div data-role="loader" class="loading-mask postcode_loading_mask" style="position: relative;text-align:right;"><div class="loader"><img src="'+window.loading_url+'" alt="Loading..." style="position: absolute;text-align:center;"></div>Please wait loading zip codes...</div>';
        if(jQuery('.postcode_loading_mask').length==0){
            jQuery('#zip').after(loader);
        }
        emptyInputZip('',main_id);
        jQuery('#error-'+postcode_id).hide();
        jQuery('.mage-error').hide();
        jQuery('#zip').hide();
        jQuery('#zip-select-error').remove();
		jQuery('#zip-select').remove();
        jQuery('.postcode_billing_notinlist').remove();
		jQuery('.postcode_br_billing_notinlist').remove();
        jQuery.ajax({
            url : url,
            type: "get",
            data:"city="+value,
            dataType: 'json',
        }).done(function (transport) {
            jQuery('#error-'+postcode_id).show();
            jQuery('.mage-error').show();
            jQuery('.postcode_loading_mask').remove();
            jQuery('#zip').show();
            var response = transport;

            var options = '<select onchange="getZipState(this.value,\''+main_id+'\')" id="'+postcode_id+'-select" class="validate-select select" title="Postcode" name="zip-select" ><option value="">Please select zip code</option>';
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    options += '<option value="' + response[i] + '">' + response[i] + '</option>';
                }
                options += "</select>";
                if(window.data_zip_link!=""){
                    var title = window.data_zip_title;
                    options+= "<br class='postcode_br_billing_notinlist' /><a onclick='notInListZip(\"billing\",\""+main_id+"\")' class='postcode_billing_notinlist' href='javascript:void(0)' class='postcode_notinlist'>"+title+"</a>";
                }
                jQuery('#zip').hide();
                if(jQuery('#zip-select').length==0){
                    jQuery('#zip').after(options);
                }
            } else {
                jQuery('#zip').html(inp);
                jQuery('.postcode_billing_notinlist').remove();
            }
        }).fail( function ( error )
        {
            jQuery('#error-'+postcode_id).show();
            jQuery('.postcode_loading_mask').remove();
            jQuery('#zip').show();
            console.log(error);
        });
}
/* Zip not in list */
function notInListZip(type,main_id){
	if(main_id=='edit'){
        var postcode_id =  "postcode";
        jQuery('#'+postcode_id+'-select').remove();
        jQuery('.postcode_billing_notinlist').remove();
        jQuery('.postcode_br_billing_notinlist').remove();
        jQuery('#postcode').show();
    }else{
        var postcode_id =  jQuery('#'+main_id+' [name="postcode"]').attr('id');
        jQuery('#'+postcode_id+'-select').remove();
        jQuery('#'+main_id+' .postcode_billing_notinlist').remove();
        jQuery('#'+main_id+' .postcode_br_billing_notinlist').remove();
        jQuery('#'+main_id+' [name="postcode"]').show();
    }
	
}