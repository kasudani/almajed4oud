<?php
 
namespace Custom\City\Controller\Index;
 
use Custom\City\Controller\Zip;
 
class Zips extends Zip
{
    public function execute()
    {
        $city = $this->getRequest()->getParam('city');
		$zip_codes_options = array();
		if($city!=""){
			$city_id = $this->_cityFactory->create()->getCollection()->addFieldToFilter('city',$city)->getFirstItem()->getId();
			$zip_codes = $this->_zipFactory->create()->getCollection()->addFieldToFilter('city_id',$city_id)
			->addFieldToFilter('main_table.status',1);
			$zip_codes->getSelect()
			 ->order('id DESC');
			if($zip_codes->count() > 0){
				foreach($zip_codes as $zip){
					$zip_codes_options[] = ucfirst($zip->getZipName());
				}
			}
		}
		echo json_encode($zip_codes_options);
		
    }
}