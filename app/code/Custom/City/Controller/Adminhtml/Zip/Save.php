<?php
 
namespace Custom\City\Controller\Adminhtml\Zip;
 
use Custom\City\Controller\Adminhtml\Zip;
 
class Save extends Zip
{
   /**
     * @return void
     */
   public function execute()
   {
      $isPost = $this->getRequest()->getPost();
 
      if ($isPost) {
         $zipModel = $this->_zipFactory->create();
         $zipId = $this->getRequest()->getParam('id');
 
         if ($zipId) {
            $zipModel->load($zipId);
         }else{
			 $formData['created_at'] = date('Y-m-d');
		 }
         $formData = $this->getRequest()->getParam('zip');
		 $zipModel->setData($formData);
         
         try {
            // Save zip
            $zipModel->save();
 
            // Display success message
            $this->messageManager->addSuccess(__('The zip code has been saved.'));
 
            // Check if 'Save and Continue'
            if ($this->getRequest()->getParam('back')) {
               $this->_redirect('*/*/edit', ['id' => $zipModel->getId(), '_current' => true]);
               return;
            }
 
            // Go to grid page
            $this->_redirect('*/*/');
            return;
         } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
         }
 
         $this->_getSession()->setFormData($formData);
         $this->_redirect('*/*/edit', ['id' => $zipId]);
      }
   }
}