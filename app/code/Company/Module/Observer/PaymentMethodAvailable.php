<?php

namespace Company\Module\Observer;

use Magento\Framework\Event\ObserverInterface;


class PaymentMethodAvailable implements ObserverInterface
{
    /**
     * payment_method_is_active event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */

    /**
       * @var \Magento\Checkout\Model\Session
       */
       protected $_checkoutSession;

      /**
       * Constructor
       *
       * @param \Magento\Checkout\Model\Session $checkoutSession
       */
        public function __construct
        (
            \Psr\Log\LoggerInterface $logger,
            \Magento\Checkout\Model\Session $checkoutSession
         ) {
            $this->logger = $logger;
            $this->_checkoutSession = $checkoutSession;
            return;
        }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // you can replace "checkmo" with your required payment method code
       // $logger->debug(print_r($observer->getEvent()->getMethodInstance()->getCode(), true));

        $shippingMethod = $this->_checkoutSession->getQuote()->getShippingAddress()->getShippingMethod();

        if ($shippingMethod == 'smsashipping_smsashipping' || $shippingMethod == 'aramex_ONP') {
            
            if($observer->getEvent()->getMethodInstance()->getCode()=="cashondelivery"){
                $checkResult = $observer->getEvent()->getResult();
                $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
            }

        } else if($shippingMethod == 'flatrate_flatrate' || $shippingMethod == 'mpbarqshipping_mpbarqshipping') {

            if($observer->getEvent()->getMethodInstance()->getCode()=="paytabsexpress"){
                $checkResult = $observer->getEvent()->getResult();
                $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
            }
        }
        
    }
}