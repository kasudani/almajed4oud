<?php
namespace Jsp\TrackAramexShipment\Controller\Account;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Element\Messages;
use Magento\Framework\View\Result\PageFactory;

class TrackAramexShipmentSearch extends \Magento\Customer\Controller\AbstractAccount
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;


    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_registry = $registry;
        parent::__construct($context);
    }

    /**
     * Default customer account page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {   
        $number = $this->getRequest()->getParam('track_number');

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        /** @var Messages $messageBlock */
        $messageBlock = $resultPage->getLayout()->createBlock(
            'Magento\Framework\View\Element\Messages',
            'answer'
        );

        if(empty($number)){
            $this->messageManager->addError(__('You didn\'t enter a order/shipment number!'));
            return $this->resultRedirectFactory->create()->setPath('customer/account/trackaramexshipment/');
        } else {

            if (!is_numeric($number)) {
               $this->messageManager->addError(__('Please enter a valid order/shipment number!'));
               return $this->resultRedirectFactory->create()->setPath('customer/account/trackaramexshipment/'); 
            }
        }
        
        $resultPage->getConfig()->getTitle()->set(__('Track Aramex Shipment Search'));
        return $resultPage;
    }
}
