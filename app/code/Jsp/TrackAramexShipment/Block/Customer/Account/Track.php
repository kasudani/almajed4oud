<?php
namespace Jsp\TrackAramexShipment\Block\Customer\Account;

class Track extends \Magento\Customer\Block\Form\Edit
{
    
    /**
     * Get Post Action Url
     * 
     * @return string
     */
    public function getPostUrl(){
        return $this->getUrl('customer/account/track');
    }

    public function getAccountNumber()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/account_number',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getUserName()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/user_name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getPassword()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/password',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getAccountPin()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/account_pin',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getAccountEntity()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/account_entity',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}

	public function getAccountCountryCode()
	{
	    return $this->_scopeConfig->getValue('aramex/settings/account_country_code',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
	}
}
