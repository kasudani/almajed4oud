<?php
/**
 *
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Jsp\Placeholder\Plugin\Checkout\Block\Checkout\AttributeMerger;

class Plugin
{
  public function afterMerge(\Magento\Checkout\Block\Checkout\AttributeMerger $subject, $result)
  {
    if (array_key_exists('street', $result)) {
      $result['street']['children'][0]['placeholder'] = __('Street address – اسم الشارع');
      $result['street']['children'][1]['placeholder'] = __('Building number or nearest - رقم السكن  او اقرب معلم');
    }

    if (array_key_exists('telephone', $result)) {
      $result['telephone']['placeholder'] = __('Phone Number (Include country code ex. +966-0507221516)');
    }

    if (array_key_exists('region_id', $result)) {
      $result['region_id']['label'] = __('City');
    }

    if (array_key_exists('city', $result)) {
      $result['city']['label'] = __('State/Province');
    }

    return $result;
  }
}