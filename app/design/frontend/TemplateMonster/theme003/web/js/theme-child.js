define([
    'jquery',
    'jquery/ui',
    'theme'
], function ($) {
    'use strict';

    $.widget('TemplateMonster.themeChild', $.TemplateMonster.theme, {

        options: {},
        home_carusel_new: function () {
            $(".block-new-products .product-items").owlCarousel({
                items: 4,
                autoPlay: 1400000,
                stopOnHover: true,
                pagination: false,
                transitionStyle: false,
                navigation: true
            });

        },
        title_wrap: function () {
            var class_title = $('.block-title strong, .page-title .base');
            $(class_title).each(function () {
                var str = $(this).html();
                var pos = str.lastIndexOf(' ');
                if(pos > 0){
                    str = str.substr(0, pos) + ' <b>' + str.substr(pos + 1) + '</b>';
                    $(this).html(str);
                }
            });
        },


        cursor_search: function () {
            $('.rd-navbar-search-toggle').on("click", function () {
                setTimeout(function() {
                    $('.rd-navbar-search #search').focus();
                }, 200);
            });
        },

        _create: function () {
            this._super();
            this.home_carusel_new();
            this.title_wrap();
            this.cursor_search();
        }
    });

    return $.TemplateMonster.themeChild;

});