/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-address/form-popup-state',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/model/shipping-rate-registry',
        'Magento_Checkout/js/action/set-shipping-information',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Ui/js/modal/modal',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/checkout-data',
        'uiRegistry',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rate-service'
    ],
    function (
        $,
        _,
        Component,
        ko,
        customer,
        addressList,
        addressConverter,
        quote,
        createShippingAddress,
        selectShippingAddress,
        shippingRatesValidator,
        formPopUpState,
        shippingService,
        selectShippingMethodAction,
        rateRegistry,
        setShippingInformationAction,
        stepNavigator,
        modal,
        checkoutDataResolver,
        checkoutData,
        registry,
        $t
    ) {
        'use strict';

        var popUp = null;

        return Component.extend({
            defaults: {
                template: 'Magento_Checkout/shipping'
            },
            visible: ko.observable(!quote.isVirtual()),
            errorValidationMessage: ko.observable(false),
            isCustomerLoggedIn: customer.isLoggedIn,
            isFormPopUpVisible: formPopUpState.isVisible,
            isFormInline: addressList().length == 0,
            isNewAddressAdded: ko.observable(false),
            saveInAddressBook: 1,
            quoteIsVirtual: quote.isVirtual(),
            map: null,
            marker: null,
            autocomplete: null,

            /**
             * @return {exports}
             */
            initialize: function () {
                var self = this,
                    hasNewAddress,
                    fieldsetName = 'checkout.steps.shipping-step.shippingAddress.shipping-address-fieldset';

                this._super();
                shippingRatesValidator.initFields(fieldsetName);

                if (!quote.isVirtual()) {
                    stepNavigator.registerStep(
                        'shipping',
                        '',
                        $t('Shipping'),
                        this.visible, _.bind(this.navigate, this),
                        10
                    );
                }
                checkoutDataResolver.resolveShippingAddress();

                hasNewAddress = addressList.some(function (address) {
                    return address.getType() == 'new-customer-address';
                });

                this.isNewAddressAdded(hasNewAddress);

                this.isFormPopUpVisible.subscribe(function (value) {
                    if (value) {
                        self.getPopUp().openModal();
                    }
                });

                quote.shippingMethod.subscribe(function () {
                    self.errorValidationMessage(false);
                });

                registry.async('checkoutProvider')(function (checkoutProvider) {
                    var shippingAddressData = checkoutData.getShippingAddressFromData();

                    if (shippingAddressData) {
                        checkoutProvider.set(
                            'shippingAddress',
                            $.extend({}, checkoutProvider.get('shippingAddress'), shippingAddressData)
                        );
                    }
                    checkoutProvider.on('shippingAddress', function (shippingAddressData) {
                        checkoutData.setShippingAddressFromData(shippingAddressData);
                    });
                });

                

                if (this.isFormInline) { 

                    console.log("inline");
                    var lat = 44.88623409320778,
                    lng = -87.86480712897173,
                    latlng = new google.maps.LatLng(lat, lng),
                    image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
                    setTimeout(function(){
                        var mapOptions = {
                                        center: new google.maps.LatLng(lat, lng),
                                        zoom: 13,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                                        panControl: true,
                                        panControlOptions: {
                                            position: google.maps.ControlPosition.TOP_RIGHT
                                        },
                                        zoomControl: true,
                                        zoomControlOptions: {
                                            style: google.maps.ZoomControlStyle.LARGE,
                                            position: google.maps.ControlPosition.TOP_left
                                        }
                                    };

                                    self.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                                    self.marker = new google.maps.Marker({
                                        position: latlng,
                                        /*draggable:true,*/
                                        animation: google.maps.Animation.DROP,
                                        map: self.map,
                                        icon: image
                                    });


                        $('.location-box-view').show();
                        self.map.controls[google.maps.ControlPosition.LEFT_TOP].push($('.location-box-view')[0]);


                        $('#widget-mylocation-button').on('click',function(){
                            console.log("up");
                            $('input[name="custom_attributes[latlong]"]').val('');
                            self.currentPosition();
                        }); 

                        self.autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});
                        google.maps.event.addListener(self.autocomplete, 'place_changed', function() {
                            var place = autocomplete.getPlace();
                            self.fillAddress(place.address_components);
                            $('input[name="custom_attributes[latlong]"]').val(place.geometry.location.lat()+','+place.geometry.location.lng());
                            var pos = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };
                            self.setCenterPosition(pos);
                        });

                        google.maps.event.addListener(self.marker, 'dragend', function(evt) 
                        {
                            var pos = { lat: evt.latLng.lat().toFixed(4), lng: evt.latLng.lng().toFixed(4)};
                            self.geocodePosition(pos);
                        });
                        self.map.addListener('dragend', function() {
                            //console.log("got here ");
                              self.map.setCenter(self.map.getCenter());
                               self.marker.setPosition(self.map.getCenter());
                            var pos = { lat: self.map.getCenter().lat().toFixed(4), lng: self.map.getCenter().lng().toFixed(4)};
                            self.geocodePosition(pos);
                          });

                        self.currentPosition();
                    }, 2000);
                }

                return this;
            },

            /**
             * Load data from server for shipping step
             */
            navigate: function () {
                //load data from server for shipping step
            },

            /**
             * @return {*}
             */
            getPopUp: function () {
                var self = this,
                    buttons;

                if (!popUp) {
                    buttons = this.popUpForm.options.buttons;
                    this.popUpForm.options.buttons = [
                        {
                            text: buttons.save.text ? buttons.save.text : $t('Save Address'),
                            class: buttons.save.class ? buttons.save.class : 'action primary action-save-address',
                            click: self.saveNewAddress.bind(self)
                        },
                        {
                            text: buttons.cancel.text ? buttons.cancel.text : $t('Cancel'),
                            class: buttons.cancel.class ? buttons.cancel.class : 'action secondary action-hide-popup',
                            click: function () {
                                this.closeModal();
                            }
                        }
                    ];
                    this.popUpForm.options.closed = function () {
                        self.isFormPopUpVisible(false);
                    };
                    popUp = modal(this.popUpForm.options, $(this.popUpForm.element));
                }


                var lat = 44.88623409320778,
                lng = -87.86480712897173,
                latlng = new google.maps.LatLng(lat, lng),
                image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
                setTimeout(function(){
                    var mapOptions = {
                                    center: new google.maps.LatLng(lat, lng),
                                    zoom: 13,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    panControl: true,
                                    panControlOptions: {
                                        position: google.maps.ControlPosition.TOP_RIGHT
                                    },
                                    zoomControl: true,
                                    zoomControlOptions: {
                                        style: google.maps.ZoomControlStyle.LARGE,
                                        position: google.maps.ControlPosition.TOP_left
                                    }
                                };

                                self.map = new google.maps.Map(document.getElementById('map'), mapOptions);
                                self.marker = new google.maps.Marker({
                                    position: latlng,
                                    /*draggable:true,*/
                                    animation: google.maps.Animation.DROP,
                                    map: self.map,
                                    icon: image
                                });

                     $('.location-box-view').show();
                        self.map.controls[google.maps.ControlPosition.LEFT_TOP].push($('.location-box-view')[0]);

                    $('#widget-mylocation-button').on('click',function(){
                        console.log("up");
                        $('input[name="custom_attributes[latlong]"]').val('');
                        self.currentPosition();
                    }); 

                    self.autocomplete = new google.maps.places.Autocomplete($("#address")[0], {});
                    google.maps.event.addListener(self.autocomplete, 'place_changed', function() {
                        var place = self.autocomplete.getPlace();
                        self.fillAddress(place.address_components);
                        $('input[name="custom_attributes[latlong]"]').val(place.geometry.location.lat()+','+place.geometry.location.lng());
                        var pos = { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() };
                        self.setCenterPosition(pos); 
                    });
 

                    google.maps.event.addListener(self.marker, 'dragend', function(evt) 
                    {
                        var pos = { lat: evt.latLng.lat().toFixed(4), lng: evt.latLng.lng().toFixed(4)};
                        self.geocodePosition(pos);
                    });

                    self.map.addListener('dragend', function() {
                            //console.log("got here ");
                            self.map.setCenter(self.map.getCenter());
                            self.marker.setPosition(self.map.getCenter());
                            var pos = { lat: self.map.getCenter().lat().toFixed(4), lng: self.map.getCenter().lng().toFixed(4)};
                            self.geocodePosition(pos);
                          });

                    self.currentPosition();
                }, 500);

                return popUp;
            },
            currentPosition: function() {
                var self = this;
                if(navigator.geolocation){
                    navigator.geolocation.getCurrentPosition(
                         function( position ){
                            var pos = { lat: position.coords.latitude, lng: position.coords.longitude };
                            if($('input[name="custom_attributes[latlong]"]').val()==""){
                                self.geocodePosition(pos);
                            }else{
                                var latlngs = $('input[name="custom_attributes[latlong]"]').val();
                                latlngs = latlngs.split(",");
                                pos = { lat: latlngs[0], lng: latlngs[1] };
                                self.setCenterPosition(pos);
                            }
                    }
                    );
                }
            },

            /**
             * Show address form popup
             */
            showFormPopUp: function () {
                this.isFormPopUpVisible(true);
                
            },

            /**
             * Show address form popup
             */
            setCenterPosition: function (pos) {
                var latlng = new google.maps.LatLng(pos.lat, pos.lng);
                this.marker.setPosition(latlng);
                this.map.setCenter(latlng);
            },
            geocodePosition: function (pos) {
                
                 // success cb
                    var latlng = new google.maps.LatLng(pos.lat, pos.lng);
                
                    var self = this;
                    this.setCenterPosition(pos);
                    console.log(pos.lat+','+pos.lng);
                    $('input[name="custom_attributes[latlong]"]').val(pos.lat+','+pos.lng);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                       
                        self.fillAddress(results[0].address_components);

                    } 
                });
                    
                            
            },
            fillAddress: function (address_components) {
                 var postcode='', city = '', street0 = '', street1 = '', street2 = '', region_id = '';
                        address_components.forEach(function(value,key){
                                console.log(value);
                            
                            if(value.types[0] == "postal_code"){
                                postcode = value.long_name;
                                
                            }
                            if(value.types[0] == "administrative_area_level_1"){
                                city =  value.long_name;
                            }
                            if(value.types[0] == "premise"){
                                street0 = value.long_name;
                            }
                            if(value.types[0] == "street_number"){
                                if(street0 == '')
                                    street0 = value.long_name;
                                else
                                    street0 = street0+' '+value.long_name;
                            }
                            if(value.types[0] == "route"){
                                if(street0 == '')
                                    street0 = value.long_name;
                                else
                                    street0 = street0+' '+value.long_name;
                            }
                            if(value.types[0] == "neighborhood"){
                                street1 = value.long_name;
                            }
                            if(value.types[0] == "sublocality_level_2" || value.types[1] == "sublocality_level_2" || value.types[2] == "sublocality_level_2"){
                                if(street1 == '')
                                    street1 = value.long_name;
                                else
                                    street1 = street1+' '+value.long_name;
                            }

                            if(value.types[0] == "sublocality_level_3" || value.types[1] == "sublocality_level_3" || value.types[2] == "sublocality_level_3"){
                                if(street1 == '')
                                    street1 = value.long_name;
                                else
                                    street1 = street1+' '+value.long_name;
                            }
                            if(value.types[0] == "sublocality" || value.types[1] == "sublocality"){
                                street2 = value.long_name;
                            }
                            if(value.types[0] == "locality"){
                                var val = value.long_name;
                                if ($('select[name="region_id"] option:contains('+ val +')').length) {
                                    region_id = $('select[name="region_id"] option:contains('+ val +')').attr('value');
                                }
                            }
                            if(value.types[0] == "administrative_area_level_2"  ){
                                 
                                var val = value.long_name;
                                if ($('select[name="region_id"] option:contains('+ val +')').length) {
                                    region_id = $('select[name="region_id"] option:contains('+ val +')').attr('value');
                                } 
                            }
                        });


                        $('input[name="postcode"]').val(postcode);
                        $('input[name="postcode"]').trigger('change');

                        $('input[name="city"]').val(city);
                        $('input[name="city"]').trigger('change');

                        $('input[name="street[0]"]').val(street0);
                        $('input[name="street[0]"]').trigger('change');

                        $('input[name="street[1]"]').val(street1);
                        $('input[name="street[1]"]').trigger('change');

                        $('input[name="street[2]"]').val(street2);
                        $('input[name="street[2]"]').trigger('change');

                        $('select[name="region_id"]').val(region_id);
                        $('select[name="region_id"]').trigger('change');                            
            },

            /**
             * Save new shipping address
             */
            saveNewAddress: function () {
                var addressData,
                    newShippingAddress;

                this.source.set('params.invalid', false);
                this.source.trigger('shippingAddress.data.validate');

                if (!this.source.get('params.invalid')) {
                    addressData = this.source.get('shippingAddress');
                    // if user clicked the checkbox, its value is true or false. Need to convert.
                    addressData.save_in_address_book = this.saveInAddressBook ? 1 : 0;

                    // New address must be selected as a shipping address
                    newShippingAddress = createShippingAddress(addressData);
                    selectShippingAddress(newShippingAddress);
                    checkoutData.setSelectedShippingAddress(newShippingAddress.getKey());
                    checkoutData.setNewCustomerShippingAddress(addressData);
                    this.getPopUp().closeModal();
                    this.isNewAddressAdded(true);
                }
            },

            /**
             * Shipping Method View
             */
            rates: shippingService.getShippingRates(),
            isLoading: shippingService.isLoading,
            isSelected: ko.computed(function () {
                    return quote.shippingMethod() ?
                        quote.shippingMethod().carrier_code + '_' + quote.shippingMethod().method_code
                        : null;
                }
            ),

            /**
             * @param {Object} shippingMethod
             * @return {Boolean}
             */
            selectShippingMethod: function (shippingMethod) {
                selectShippingMethodAction(shippingMethod);
                checkoutData.setSelectedShippingRate(shippingMethod.carrier_code + '_' + shippingMethod.method_code);

                return true;
            },

            /**
             * Set shipping information handler
             */
            setShippingInformation: function () {
                console.log("setShippingInformation");
                if (this.validateShippingInformation()) {
                    setShippingInformationAction().done(
                        function () {
                            stepNavigator.next();
                        }
                    );
                }
            },

            /**
             * @return {Boolean}
             */
            validateShippingInformation: function () {
                console.log("validateShippingInformation");
                var shippingAddress,
                    addressData,
                    loginFormSelector = 'form[data-role=email-with-possible-login]',
                    emailValidationResult = customer.isLoggedIn();

                if (!quote.shippingMethod()) {
                    this.errorValidationMessage('Please specify a shipping method.');

                    return false;
                }

                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (this.isFormInline) {
                    this.source.set('params.invalid', false);
                    this.source.trigger('shippingAddress.data.validate');

                    if (this.source.get('shippingAddress.custom_attributes')) {
                        this.source.trigger('shippingAddress.custom_attributes.data.validate');
                    }

                    if (this.source.get('params.invalid') ||
                        !quote.shippingMethod().method_code ||
                        !quote.shippingMethod().carrier_code ||
                        !emailValidationResult
                    ) {
                        return false;
                    }

                    shippingAddress = quote.shippingAddress();
                    addressData = addressConverter.formAddressDataToQuoteAddress(
                        this.source.get('shippingAddress')
                    );

                    //Copy form data to quote shipping address object
                    for (var field in addressData) {

                        if (addressData.hasOwnProperty(field) &&
                            shippingAddress.hasOwnProperty(field) &&
                            typeof addressData[field] != 'function' &&
                            _.isEqual(shippingAddress[field], addressData[field])
                        ) {
                            shippingAddress[field] = addressData[field];
                        } else if (typeof addressData[field] != 'function' &&
                            !_.isEqual(shippingAddress[field], addressData[field])) {
                            shippingAddress = addressData;
                            break;
                        }
                    }

                    if (customer.isLoggedIn()) {
                        shippingAddress.save_in_address_book = 1;
                    }
                    selectShippingAddress(shippingAddress);
                }

                if (!emailValidationResult) {
                    $(loginFormSelector + ' input[name=username]').focus();

                    return false;
                }

                return true;
            }
        });
    }
);
