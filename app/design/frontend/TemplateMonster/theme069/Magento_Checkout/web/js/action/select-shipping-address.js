/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'Magento_Checkout/js/model/quote',
        'mage/storage'
    ],
    function(quote, storage) {
        'use strict';
        return function(shippingAddress) {
        	console.log(shippingAddress);
        	var dataToPass = {};
        		if(shippingAddress.customerAddressId){
                    dataToPass['address_id'] = shippingAddress.customerAddressId;
                }else{
                	dataToPass['address_id'] = "new-customer-address";
                }

                    storage.post(
                        'saddress/index/address',
                        JSON.stringify(dataToPass),
                        true
                    ).done(
 
                    );
                 
            quote.shippingAddress(shippingAddress);
        };
    }
);
